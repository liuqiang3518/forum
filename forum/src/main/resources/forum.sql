/*
Navicat MySQL Data Transfer

Source Server         : Lcalhost
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : forum

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2016-03-18 18:12:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `academy`
-- ----------------------------
DROP TABLE IF EXISTS `academy`;
CREATE TABLE `academy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `academyId` bigint(20) NOT NULL,
  `academyName` varchar(255) NOT NULL COMMENT '学院名称',
  `deanID` bigint(20) DEFAULT '0' COMMENT '院长ID',
  `deanName` varchar(100) DEFAULT NULL COMMENT '院长姓名',
  `introduction` text COMMENT '学院介绍',
  PRIMARY KEY (`id`),
  KEY `academyId` (`academyId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of academy
-- ----------------------------
INSERT INTO `academy` VALUES ('1', '100', '车辆工程学院', '0', null, null);
INSERT INTO `academy` VALUES ('2', '123', '计算机科学与工程学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('3', '101', '材料科学与工程学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('4', '102', '化学化工学院', '0', null, null);
INSERT INTO `academy` VALUES ('5', '103', '光电信息学院', '0', null, null);
INSERT INTO `academy` VALUES ('6', '104', '管理学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('7', '105', '思想政治教育学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('8', '106', '语言学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('9', '107', '应用技术学院', '0', null, null);
INSERT INTO `academy` VALUES ('10', '108', '机械工程学院', '0', null, null);
INSERT INTO `academy` VALUES ('11', '109', ' 	电子信息与自动化学院', '0', null, null);
INSERT INTO `academy` VALUES ('12', '110', ' 	药学与生物工程学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('13', '111', ' 	数学与统计学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('14', '112', '会计学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('15', '113', '经济与贸易学院 ', '0', null, null);
INSERT INTO `academy` VALUES ('16', '114', '重庆知识产权学院', '0', null, null);
INSERT INTO `academy` VALUES ('17', '115', ' 	商贸信息学院', '0', null, null);
INSERT INTO `academy` VALUES ('18', '116', ' 	MBA教育中心', '0', null, null);

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userName` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `createDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'liu', '123456', '1', null);
INSERT INTO `admin` VALUES ('3', 'qq', '123456', '2', null);
INSERT INTO `admin` VALUES ('12', 'aa', '123456', '2', '2015-05-19');
INSERT INTO `admin` VALUES ('13', 'dd', '123456', '2', '2015-06-06');
INSERT INTO `admin` VALUES ('14', 'fsdfs', '123456', '2', '2015-06-06');
INSERT INTO `admin` VALUES ('15', 'gsd', '123456', '2', '2015-06-06');
INSERT INTO `admin` VALUES ('16', 'gsgs', '123456', '2', '2015-06-06');

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `postId` bigint(20) NOT NULL,
  `targetId` bigint(20) NOT NULL DEFAULT '0' COMMENT 'targetID为0表示评论该帖子\r\n其他值表示回复评论的对应的id',
  `tuserId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `content` text NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_postId` (`postId`),
  KEY `comment_userId` (`userId`),
  CONSTRAINT `comment_postId` FOREIGN KEY (`postId`) REFERENCES `post` (`id`),
  CONSTRAINT `comment_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('1', '1', '0', null, '2', 'qqqqqqqqqqqqqqq', '2015-04-22 00:00:01');
INSERT INTO `comment` VALUES ('2', '1', '0', null, '2', 'sefasga', '2015-04-18 00:00:02');
INSERT INTO `comment` VALUES ('3', '1', '0', null, '2', 'dsgasdg', '2015-04-19 00:00:03');
INSERT INTO `comment` VALUES ('4', '1', '0', null, '2', 'sgsg', '2015-04-21 00:00:04');
INSERT INTO `comment` VALUES ('5', '1', '0', null, '2', 'vfgsgssd', '2015-04-26 00:00:05');
INSERT INTO `comment` VALUES ('6', '1', '0', null, '2', '<p>违法施工围墙</p>', '2015-05-05 14:49:49');
INSERT INTO `comment` VALUES ('7', '1', '0', null, '2', '<p>sdfadfsdadEQ</p>', '2015-05-05 15:05:07');
INSERT INTO `comment` VALUES ('8', '1', '0', null, '2', '<p><img src=\"http://img.baidu.com/hi/jx2/j_0016.gif\"/></p>', '2015-05-05 15:09:23');
INSERT INTO `comment` VALUES ('9', '1', '0', null, '2', '<p><img src=\"/forum/file/img/20150505/1430809797639000880.jpg\" title=\"1430809797639000880.jpg\" alt=\"qq.jpg\" width=\"343\" height=\"149\" style=\"width: 343px; height: 149px;\"/></p>', '2015-05-05 15:10:07');
INSERT INTO `comment` VALUES ('10', '1', '0', null, '2', '<p><span style=\"background-color: rgb(255, 0, 0);\"><em><strong>gaga啊得噶</strong></em></span></p>', '2015-05-05 15:13:19');
INSERT INTO `comment` VALUES ('11', '1', '0', null, '2', '<p><img width=\"307\" height=\"175\" src=\"http://api.map.baidu.com/staticimage?center=116.4017,39.915&zoom=10&width=530&height=340&markers=116.404,39.915\" style=\"width: 307px; height: 175px;\"/></p>', '2015-05-05 15:13:39');
INSERT INTO `comment` VALUES ('12', '1', '0', null, '2', '<table><tbody><tr class=\"firstRow\"><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">sss</td><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">ss</td><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">qq</td></tr><tr><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">qq</td><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">qq</td><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">qqqq</td></tr></tbody></table><p><br/></p>', '2015-05-05 15:40:41');
INSERT INTO `comment` VALUES ('13', '1', '0', null, '2', '<p><img width=\"417\" height=\"237\" src=\"http://api.map.baidu.com/staticimage?center=106.561015,29.55095&zoom=12&width=530&height=340&markers=116.404,39.915\" style=\"width: 417px; height: 237px;\"/></p>', '2015-05-05 16:15:11');
INSERT INTO `comment` VALUES ('14', '28', '0', null, '2', '<table><tbody><tr class=\"firstRow\"><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">11</td><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">11</td><td width=\"353\" valign=\"top\" style=\"word-break: break-all;\">111</td></tr></tbody></table><p><br/></p>', '2015-05-06 10:47:26');
INSERT INTO `comment` VALUES ('15', '28', '0', null, '2', '<p><img width=\"412\" height=\"258\" src=\"http://api.map.baidu.com/staticimage?center=106.550523,29.46256&zoom=14&width=530&height=340&markers=116.404,39.915\" style=\"float: left; width: 412px; height: 258px;\"/>我在这里！！</p>', '2015-05-06 10:53:03');
INSERT INTO `comment` VALUES ('16', '28', '0', null, '2', '<p>qqqq</p>', '2015-05-06 13:33:47');
INSERT INTO `comment` VALUES ('17', '22', '0', null, '2', '<p>sfadfa</p>', '2015-05-06 13:57:29');
INSERT INTO `comment` VALUES ('18', '28', '16', '2', '2', 'qqqq1', '2015-05-06 15:09:25');
INSERT INTO `comment` VALUES ('19', '28', '14', '2', '2', 'qqqq2', '2015-05-06 16:34:26');
INSERT INTO `comment` VALUES ('20', '28', '16', '2', '2', '不是这样的！！', '2015-05-06 20:13:25');
INSERT INTO `comment` VALUES ('21', '1', '8', '2', '2', 'qqqq', '2015-05-07 00:09:53');
INSERT INTO `comment` VALUES ('22', '28', '16', '2', '2', 'qafas', '2015-05-07 00:10:26');
INSERT INTO `comment` VALUES ('23', '28', '0', null, '2', '<p><img src=\"/forum/file/img/20150507/1430928648848070568.jpg\" title=\"1430928648848070568.jpg\" width=\"347\" height=\"231\" style=\"width: 347px; height: 231px;\"/></p>', '2015-05-07 00:10:54');
INSERT INTO `comment` VALUES ('24', '28', '23', '2', '2', 'gwasgsd', '2015-05-07 00:11:15');
INSERT INTO `comment` VALUES ('25', '28', '14', '2', '2', '滑石粉', '2015-05-09 17:15:40');
INSERT INTO `comment` VALUES ('26', '28', '23', '2', '2', '啊话说', '2015-05-09 17:17:23');
INSERT INTO `comment` VALUES ('27', '1', '0', null, '2', '<p><img src=\"/forum/file/img/20150509/1431169776711030047.jpg\" title=\"1431169776711030047.jpg\" width=\"362\" height=\"183\" style=\"width: 362px; height: 183px; float: left;\"/>画的不错的！！</p>', '2015-05-09 19:09:51');
INSERT INTO `comment` VALUES ('28', '1', '10', '2', '2', '什么东西？？', '2015-05-12 00:41:12');
INSERT INTO `comment` VALUES ('29', '26', '0', null, '2', '<p>ssdgsdfhfsd大航海时代好</p>', '2015-05-12 20:40:02');
INSERT INTO `comment` VALUES ('30', '30', '0', null, '2', '<p><img src=\"/forum/ueditor/jsp/upload/image/20150516/1431742696648014675.jpg\" title=\"1431742696648014675.jpg\" width=\"326\" height=\"237\" style=\"width: 326px; height: 237px;\"/></p>', '2015-05-16 10:18:20');
INSERT INTO `comment` VALUES ('31', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:34');
INSERT INTO `comment` VALUES ('32', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:40');
INSERT INTO `comment` VALUES ('33', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:46');
INSERT INTO `comment` VALUES ('34', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:48');
INSERT INTO `comment` VALUES ('35', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:48');
INSERT INTO `comment` VALUES ('36', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:49');
INSERT INTO `comment` VALUES ('37', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:51');
INSERT INTO `comment` VALUES ('38', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:52');
INSERT INTO `comment` VALUES ('39', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:16:52');
INSERT INTO `comment` VALUES ('40', '32', '0', null, '6', '<p>ggsadgasgcghreahdfhjeahasdgwag杀手工会瓦良格发</p>', '2015-05-16 17:17:40');
INSERT INTO `comment` VALUES ('41', '32', '0', null, '6', '<p>三国杀<br/></p>', '2015-05-16 17:18:39');
INSERT INTO `comment` VALUES ('42', '32', '0', null, '6', '<p>还不如我是</p>', '2015-05-16 17:19:30');
INSERT INTO `comment` VALUES ('43', '32', '0', null, '6', '<p>达曙凤蝶</p>', '2015-05-16 17:20:28');
INSERT INTO `comment` VALUES ('44', '32', '0', null, '6', '<p>dda</p>', '2015-05-16 17:21:40');
INSERT INTO `comment` VALUES ('45', '32', '0', null, '6', '<p>dda</p>', '2015-05-16 17:22:14');
INSERT INTO `comment` VALUES ('46', '32', '0', null, '6', '<p>bbb</p>', '2015-05-16 17:24:45');
INSERT INTO `comment` VALUES ('47', '32', '0', null, '6', '<p>bbb</p>', '2015-05-16 17:28:49');
INSERT INTO `comment` VALUES ('48', '32', '0', null, '6', '<p>ACASFWSA</p>', '2015-05-16 17:33:38');
INSERT INTO `comment` VALUES ('49', '32', '0', null, '6', '<p>xcvbsvv</p>', '2015-05-16 17:37:54');
INSERT INTO `comment` VALUES ('50', '32', '0', null, '6', '<p>xcvbsvv</p>', '2015-05-16 17:39:02');
INSERT INTO `comment` VALUES ('51', '32', '0', null, '6', '<p>xcvbsvv</p>', '2015-05-16 17:39:08');
INSERT INTO `comment` VALUES ('52', '32', '0', null, '6', '<p>vxczvbszxbs</p>', '2015-05-16 17:43:49');
INSERT INTO `comment` VALUES ('53', '22', '17', '2', '6', 'ssss ', '2015-05-16 20:03:30');
INSERT INTO `comment` VALUES ('54', '27', '0', null, '6', '<p>adfafa</p>', '2015-05-17 00:13:58');
INSERT INTO `comment` VALUES ('55', '28', '16', '2', '6', 'dsddfsfsa', '2015-05-17 00:15:02');
INSERT INTO `comment` VALUES ('56', '22', '0', null, '2', '<p><img src=\"http://img.baidu.com/hi/jx2/j_0001.gif\"/></p>', '2015-05-20 11:35:42');
INSERT INTO `comment` VALUES ('62', '30', '0', null, '2', '<p>gdfsgsf</p>', '2015-06-06 15:55:46');
INSERT INTO `comment` VALUES ('63', '30', '30', '2', '2', 'dfsgsfg', '2015-06-06 15:55:53');
INSERT INTO `comment` VALUES ('64', '30', '0', null, '2', '<p>sdfgsfgdsfhsdf<br/></p>', '2015-06-06 15:56:03');
INSERT INTO `comment` VALUES ('65', '29', '0', null, '2', '<p>dsfhdfherh</p>', '2015-06-06 15:56:29');
INSERT INTO `comment` VALUES ('66', '35', '0', null, '2', '<p>不好学，真的不好学！！！</p>', '2015-06-06 23:30:15');
INSERT INTO `comment` VALUES ('67', '35', '0', null, '6', '<p>学好。好学。好学！！！！！</p>', '2015-06-06 23:31:24');
INSERT INTO `comment` VALUES ('68', '35', '66', '2', '6', '嗡嗡嗡嗡嗡嗡', '2015-06-06 23:31:41');
INSERT INTO `comment` VALUES ('69', '35', '0', null, '6', '<p>士大夫士大夫敢死队说过的</p>', '2015-06-06 23:31:49');
INSERT INTO `comment` VALUES ('70', '22', '17', '2', '6', '了个啊个挖个好', '2015-06-06 23:32:46');
INSERT INTO `comment` VALUES ('71', '22', '56', '2', '6', '哇好高哇各位了一个', '2015-06-06 23:32:53');
INSERT INTO `comment` VALUES ('72', '22', '0', null, '6', '<p>苏丹皇宫沙发伽师瓜打鳄鱼和的后果发生了个</p>', '2015-06-06 23:33:12');
INSERT INTO `comment` VALUES ('73', '29', '65', '2', '6', '很多所谓啊好了', '2015-06-06 23:33:52');
INSERT INTO `comment` VALUES ('74', '29', '0', null, '6', '<p>说如果发额私聊发了哈哈设施</p>', '2015-06-06 23:33:59');
INSERT INTO `comment` VALUES ('75', '38', '0', null, '6', '<p>是的，没错！！！</p>', '2015-06-09 17:20:45');

-- ----------------------------
-- Table structure for `filestore`
-- ----------------------------
DROP TABLE IF EXISTS `filestore`;
CREATE TABLE `filestore` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filemd5` varchar(255) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `uploadDate` datetime NOT NULL,
  `isfreeze` int(11) NOT NULL DEFAULT '0' COMMENT '不冻结 0   冻结  1',
  `filesize` varchar(255) DEFAULT '0',
  `filetype` int(11) DEFAULT NULL COMMENT '1为文档文件，2为音频文件，3为视频文件，4为图片文件，0为其他',
  PRIMARY KEY (`id`),
  KEY `fileStore_userId` (`userId`),
  CONSTRAINT `fileStore_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of filestore
-- ----------------------------
INSERT INTO `filestore` VALUES ('1', 'c598e5b016a5acef8890627888414cb', 'Wallpaper01.jpg', 'file/img/48973109-fe75-4832-ab63-dad85df7bb0d.jpg', '2', '2015-05-14 16:10:18', '0', '443KB', '4');
INSERT INTO `filestore` VALUES ('2', 'bf1246be2dbaa8b28b752738e4eea272', '1334845207145.jpg', 'file/img/a1a94911-5f53-4477-8edd-5d987c92f415.jpg', '2', '2015-05-14 16:13:11', '0', '260KB', '4');
INSERT INTO `filestore` VALUES ('3', '277ba83f12fc0d2d17e5ac2c6f781f1f', '74932B86883E.jpg', 'file/img/1fc4c0f0-1865-4ffc-bee7-cdb1e96e28d2.jpg', '2', '2015-05-14 16:19:18', '0', '258KB', '4');
INSERT INTO `filestore` VALUES ('4', '43d1a2ea3ce6d915b8f54cceabf47a6f', 'liu110.jpg', 'file/img/28714b2c-61e7-4c17-bb32-81c97868b47c.jpg', '2', '2015-05-14 18:31:04', '0', '1M', '4');
INSERT INTO `filestore` VALUES ('6', '7decf6080458058e83d55a2efdded22f', 'Wallpaper02.jpg', 'file/img/6164c627-c923-48a6-8a1f-42a410cfce91.jpg', '2', '2015-05-15 10:11:58', '0', '626KB', '4');
INSERT INTO `filestore` VALUES ('7', '6a3d6067b166a3f99f278d7ac3225dd4', '安卓壁纸_风景a976.jpg', 'file/img/13bbc38d-f7ee-4a86-a000-229c1eaf244f.jpg', '2', '2015-05-15 10:42:43', '0', '225KB', '4');
INSERT INTO `filestore` VALUES ('8', '41cac72e90e2bd1ab6333d4ded4be618', 'a5863815_s.jpg', 'file/img/381f1e4c-fb8d-47fd-aa29-4e7ae7796da0.jpg', '2', '2015-05-15 11:05:37', '0', '53KB', '4');
INSERT INTO `filestore` VALUES ('9', '60556a6d90843d5355a8df746f3db928', 'qq.jpg', 'file/img/e7c41b0a-3882-4581-907c-e95216a7b8c0.jpg', '2', '2015-05-15 11:05:38', '0', '392KB', '4');
INSERT INTO `filestore` VALUES ('10', 'ea3c1efd1195ccc11de784e4eead2fa8', '安卓壁纸_风景696a.jpg', 'file/img/b3b61ae9-4a3b-4198-b653-496949217c52.jpg', '2', '2015-05-15 11:09:51', '0', '84KB', '4');
INSERT INTO `filestore` VALUES ('11', 'ad9e83ad1376aba14b0b5c924780f6b', '安卓壁纸_艺术d24d.jpg', 'file/img/40024276-c179-4e20-955c-39858accb021.jpg', '2', '2015-05-15 11:12:20', '0', '221KB', '4');
INSERT INTO `filestore` VALUES ('12', 'fa8f678233c402695b9082d7066091d2', '安卓壁纸_风景fd09.jpg', 'file/img/20345420-737b-4d01-a198-194d75b8d752.jpg', '2', '2015-05-15 11:13:31', '0', '74KB', '4');
INSERT INTO `filestore` VALUES ('13', 'd08271f33c0c94c2550f764efbdd5483', '7B56B0696365.jpg', 'file/img/04707762-acdb-44a2-820c-4cc233349b68.jpg', '2', '2015-05-15 11:13:58', '0', '95KB', '4');
INSERT INTO `filestore` VALUES ('14', '90dbbacd2644253c474364af0173ac21', '773D1E407FA4.jpg', 'file/img/6eedb1a9-9035-44ef-b2fa-4f3093de77e7.jpg', '2', '2015-05-15 11:18:55', '0', '432KB', '4');
INSERT INTO `filestore` VALUES ('15', 'c27de82e773be032dea4b099580b076', 'Annie Lennox - I Put A Spell On You.mp3', 'file/music/3d465e7e-d9f2-4a9f-8a45-abe9bdaa086d.mp3', '2', '2015-05-15 13:41:58', '0', '8M', '2');
INSERT INTO `filestore` VALUES ('16', 'e58029d1af673328fc2d7e20e8894518', '7A2D218B7252.jpg', 'file/img/fab57e62-77bc-4fd1-a1d3-e0a467e7d923.jpg', '2', '2015-05-15 15:28:51', '0', '29KB', '4');
INSERT INTO `filestore` VALUES ('17', 'f13dda3b8131e07fc6668bc2395216da', '7C0FD2BDD77A.jpg', 'file/img/111eeddf-a754-4927-8ebd-274bc06bc335.jpg', '2', '2015-05-15 15:39:07', '0', '115KB', '4');
INSERT INTO `filestore` VALUES ('18', 'd96bda9aa85f3c6ef1881b1609147a51', 'Java框架Bootstrap、HTML5、jQuery、Spring MVC、Hibernate、Spring Security、jQuery jqGrid、网格、Ajax分页、表格排序、增加、修改、查看、删除、查询、导出Excel.docx', 'file/document/4a631bd0-fc05-4d5c-a9e9-63b5ee8f99ac.docx', '2', '2015-05-16 08:56:43', '0', '2M', '1');
INSERT INTO `filestore` VALUES ('19', 'a75299993755fd7fe2429f072a156105', '08143934_学位英语名单.xls', 'file/document/e6b94b5c-b0c9-44ac-9824-ce4336f8730e.xls', '2', '2015-05-16 08:56:55', '0', '100KB', '1');
INSERT INTO `filestore` VALUES ('20', '6fbacbabce19bf222e5430eda43371f4', 'jdbc封装(实现对实体的增删改查[分页]).zip', 'file/other/77ac68a9-9cc5-4645-898c-112ab88b4cbb.zip', '2', '2015-05-16 08:57:07', '0', '3M', '0');
INSERT INTO `filestore` VALUES ('21', '8d994fabbadcb47fb413ebe12b6cedc8', 'jQuery-File-Upload-master.zip', 'file/other/40e5ab05-2e64-40d8-8e33-50cce8439916.zip', '2', '2015-05-16 08:57:07', '0', '116KB', '0');
INSERT INTO `filestore` VALUES ('22', '224b53aa50a7f985e91806d9e19c975c', '[Android开发从零开始].1.Android开发环境搭建.mp4', 'file/vidoe/722fa434-8627-4706-93aa-c715d65a3567.mp4', '2', '2015-05-16 10:26:15', '0', '30M', '3');
INSERT INTO `filestore` VALUES ('23', '9dcc37854e806b1609b069617a88e059', 'ajaxfileupload.js', 'file/other/aa5453ae-93ad-467a-b59d-019aacd8bc94.js', '2', '2015-05-16 17:14:36', '0', '7KB', '0');
INSERT INTO `filestore` VALUES ('24', 'cd2a5b13aadae8a665fc83a385af6ace', 'B.O.B、jessie J - Price Tag.ape', 'file/music/374dfc30-34d4-45ed-89e7-c41dad596c7c.ape', '2', '2015-05-23 14:03:14', '0', '27M', '2');

-- ----------------------------
-- Table structure for `hotpost`
-- ----------------------------
DROP TABLE IF EXISTS `hotpost`;
CREATE TABLE `hotpost` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `postId` bigint(20) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '0表示左边的热帖，1表示右边的',
  `imagePath` varchar(255) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  CONSTRAINT `postId` FOREIGN KEY (`postId`) REFERENCES `post` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hotpost
-- ----------------------------
INSERT INTO `hotpost` VALUES ('1', '1', '1', null, '2015-04-15');
INSERT INTO `hotpost` VALUES ('2', '2', '1', null, '2015-04-11');
INSERT INTO `hotpost` VALUES ('3', '3', '1', null, '2015-04-16');
INSERT INTO `hotpost` VALUES ('4', '4', '1', null, '2015-04-18');
INSERT INTO `hotpost` VALUES ('5', '5', '1', null, '2015-04-11');
INSERT INTO `hotpost` VALUES ('6', '6', '1', null, '2015-04-26');
INSERT INTO `hotpost` VALUES ('7', '7', '1', null, '2015-04-20');
INSERT INTO `hotpost` VALUES ('8', '8', '1', null, '2015-04-09');
INSERT INTO `hotpost` VALUES ('9', '9', '1', null, '2015-04-23');
INSERT INTO `hotpost` VALUES ('10', '10', '1', null, '2015-04-09');
INSERT INTO `hotpost` VALUES ('11', '7', '0', 'images/slide-1.jpg', '2015-04-22');
INSERT INTO `hotpost` VALUES ('12', '3', '0', 'images/slide-2.jpg', '2015-04-11');
INSERT INTO `hotpost` VALUES ('13', '6', '0', 'images/slide-3.jpg', '2015-04-19');
INSERT INTO `hotpost` VALUES ('14', '16', '0', 'images/slide-4.jpg', '2015-04-13');
INSERT INTO `hotpost` VALUES ('15', '15', '0', 'images/slide-1.jpg', '2015-04-07');
INSERT INTO `hotpost` VALUES ('16', '10', '0', 'images/slide-6.jpg', '2015-04-23');
INSERT INTO `hotpost` VALUES ('17', '32', '0', 'images/28d0492e-c535-4a6a-a295-e0e28b2ce005.jpg', '2015-05-19');
INSERT INTO `hotpost` VALUES ('18', '32', '0', 'images/5170c88a-9e56-4f0c-970e-495031c566f7.jpg', '2015-05-19');

-- ----------------------------
-- Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `targetId` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `isRead` int(11) NOT NULL DEFAULT '0' COMMENT '0表示未读1表示已读',
  `createDate` datetime NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0为帖子消息1为系统消息',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', '2', '32', null, '干啥干啥啊好你已更新', '1', '2015-05-16 17:43:49', '0');
INSERT INTO `message` VALUES ('2', '2', '22', null, '点点滴滴已更新', '1', '2015-05-16 20:03:30', '0');
INSERT INTO `message` VALUES ('3', '2', null, '标题1', 'sfgashgaghrweg', '1', '2015-05-13 21:02:44', '1');
INSERT INTO `message` VALUES ('4', '2', null, '标题2', 'fnfgjfdgj', '1', '2015-05-02 21:03:00', '1');
INSERT INTO `message` VALUES ('5', '2', '27', null, 'dds&nbsp;已更新！', '0', '2015-05-17 00:14:05', '0');
INSERT INTO `message` VALUES ('6', '2', '28', null, 'qq&nbsp;有回复！', '1', '2015-05-17 00:15:03', '0');
INSERT INTO `message` VALUES ('9', '6', null, '你好，你好', '你好，你好你好，你好', '0', '2015-05-20 08:59:36', '1');
INSERT INTO `message` VALUES ('10', '2', null, '你好，你好', '你好，你好你好，你好', '0', '2015-05-20 08:59:36', '1');
INSERT INTO `message` VALUES ('11', '2', null, 'sdgasdg', 'gasgsadgsdgasge', '0', '2015-06-06 09:16:15', '1');
INSERT INTO `message` VALUES ('12', '2', '35', null, '学日语&nbsp;已更新！', '0', '2015-06-06 23:31:25', '0');
INSERT INTO `message` VALUES ('13', '2', '35', null, '学日语&nbsp;有回复！', '0', '2015-06-06 23:31:41', '0');
INSERT INTO `message` VALUES ('14', '2', '35', null, '学日语&nbsp;已更新！', '0', '2015-06-06 23:31:50', '0');
INSERT INTO `message` VALUES ('15', '2', '22', null, '点点滴滴&nbsp;有回复！', '0', '2015-06-06 23:32:46', '0');
INSERT INTO `message` VALUES ('16', '2', '22', null, '点点滴滴&nbsp;有回复！', '0', '2015-06-06 23:32:53', '0');
INSERT INTO `message` VALUES ('17', '2', '22', null, '点点滴滴&nbsp;已更新！', '0', '2015-06-06 23:33:12', '0');
INSERT INTO `message` VALUES ('18', '2', '29', null, '信息安全是一门很重要的课程&nbsp;有回复！', '0', '2015-06-06 23:33:52', '0');
INSERT INTO `message` VALUES ('19', '2', '29', null, '信息安全是一门很重要的课程&nbsp;已更新！', '0', '2015-06-06 23:33:59', '0');
INSERT INTO `message` VALUES ('20', '2', '38', null, '网络是一门很有趣的学科&nbsp;已更新！', '0', '2015-06-09 17:20:45', '0');

-- ----------------------------
-- Table structure for `plate`
-- ----------------------------
DROP TABLE IF EXISTS `plate`;
CREATE TABLE `plate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plateName` varchar(100) NOT NULL,
  `themeId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL COMMENT '版主Id',
  `introduction` text,
  `postNumber` bigint(20) DEFAULT '0',
  `color` varchar(10) DEFAULT 'blue',
  `icon` varchar(30) DEFAULT 'icon-signal',
  PRIMARY KEY (`id`),
  KEY `plate_themeId` (`themeId`) USING BTREE,
  KEY `plate_userId` (`userId`),
  CONSTRAINT `plate_themeId` FOREIGN KEY (`themeId`) REFERENCES `theme` (`id`),
  CONSTRAINT `plate_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plate
-- ----------------------------
INSERT INTO `plate` VALUES ('2', '软件', '1', '2', '软件开发', '22', 'orange', 'icon-bell');
INSERT INTO `plate` VALUES ('3', '信息安全', '1', '2', '信息安全技术', '3', 'red', 'icon-star');
INSERT INTO `plate` VALUES ('4', '网络', '1', '2', '网络是一门很有趣的学科', '1', 'orange', 'icon-print\r\n');
INSERT INTO `plate` VALUES ('5', '操作系统', '1', '2', '操作系统非常有趣', '1', 'green', 'icon-tasks');
INSERT INTO `plate` VALUES ('6', 'C语言', '1', '2', 'C语言是一种不太好学的计算机语言', '1', 'brown', 'icon-time');
INSERT INTO `plate` VALUES ('7', 'Java Web', '1', '2', 'Java Web是非常流行的', '0', 'lime', 'icon-tint');
INSERT INTO `plate` VALUES ('8', 'windows', '1', '2', 'windows操作系统', '0', 'blue', 'icon-trophy');
INSERT INTO `plate` VALUES ('9', '其他', '1', '2', '其他的帖子', '0', 'teal', 'icon-picture');
INSERT INTO `plate` VALUES ('11', '英语四级', '2', '2', '英语四级很容易的', '1', 'pink', 'icon-pencil');
INSERT INTO `plate` VALUES ('12', '英语六级', '2', '2', '英语六级难吗', '0', 'grey', 'icon-flag');
INSERT INTO `plate` VALUES ('13', '雅思', '2', '2', '考雅思出国去', '1', 'darkblue', 'icon-fire');
INSERT INTO `plate` VALUES ('14', '托福', '2', '2', '托福也不太好考', '1', 'lightred', 'icon-calendar');
INSERT INTO `plate` VALUES ('15', '日语', '2', '2', '日语很好学的', '1', 'red', 'icon-check');
INSERT INTO `plate` VALUES ('16', '出国留学', '2', '2', '出国留学很不错的选择', '0', 'orange', 'icon-cloud');
INSERT INTO `plate` VALUES ('17', '其他', '2', '2', '', '0', 'lime', 'icon-cog');
INSERT INTO `plate` VALUES ('23', '阿萨', '2', null, '在vava等等', '0', 'red', 'icon-barcode');

-- ----------------------------
-- Table structure for `post`
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plateId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL COMMENT '帖子名字',
  `content` text NOT NULL,
  `postDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `pageView` bigint(20) DEFAULT '0',
  `ReplyNumber` bigint(20) DEFAULT '0',
  `isfrozen` int(11) NOT NULL DEFAULT '0' COMMENT '0表示不冻结1表示冻结',
  PRIMARY KEY (`id`),
  KEY `post_plateId` (`plateId`),
  KEY `post_userId` (`userId`),
  CONSTRAINT `post_plateId` FOREIGN KEY (`plateId`) REFERENCES `plate` (`id`),
  CONSTRAINT `post_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES ('1', '2', '2', 'liu', '软件开发技术', '软件开发是一系列按照特定顺序组织的计算机数据和指令的集合', '2015-04-20 00:00:00', '2015-04-21 00:00:00', '20', '3', '0');
INSERT INTO `post` VALUES ('2', '2', '2', 'liu', '系统软件', '一般来讲软件被划分为系统软件、应用软件和介于这两者之间的中间件', '2015-04-21 00:00:00', '2015-04-21 00:00:00', '4', '0', '0');
INSERT INTO `post` VALUES ('3', '2', '2', 'liu', '软件并不只是包括可以在计算机', '软件并不只是包括可以在计算机（这里的计算机是指广义的计算机）上运行的电脑程序，与这些电脑程序相关的文档一般也被认为是软件的一部分', '2015-04-15 00:00:00', '2015-04-16 00:00:00', '11', '0', '0');
INSERT INTO `post` VALUES ('4', '2', '2', 'liu', '单的说软件就是程序加文档的集合', '简单的说软件就是程序加文档的集合体。另也泛指社会结构中的管理系统、思想意识形态、思想政治觉悟、法律法规等等。', '2015-04-10 00:00:00', '2015-04-11 00:00:00', '2', '0', '0');
INSERT INTO `post` VALUES ('5', '2', '2', 'liu', '国标中对软件的定义', '软件，拼音为Ruǎnjiàn，国标中对软件的定义为：与计算机系统操作有关的计算机程序、规程、规则，以及可能有的文件、文档及数据。', '2015-04-06 00:00:00', '2015-04-07 00:00:00', '2', '0', '0');
INSERT INTO `post` VALUES ('6', '2', '2', 'liu', '够提供所要求功能和', '运行时，能够提供所要求功能和性能的指令或计算机程序集合。', '2015-04-10 00:00:00', '2015-04-11 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('7', '2', '2', 'liu', '数据结构', '程序能够满意地处理信息的数据结构。', '2015-04-09 00:00:00', '2015-04-17 00:00:00', '11', '0', '0');
INSERT INTO `post` VALUES ('8', '2', '2', 'liu', '述程序功能需求', '描述程序功能需求以及程序如何操作和使用所要求的文档。', '2015-04-07 00:00:00', '2015-04-11 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('9', '2', '2', 'liu', '没有物理形态', '无形的，没有物理形态，只能通过运行状况来了解功能、特性、和质量', '2015-04-07 00:00:00', '2015-04-04 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('10', '2', '2', 'liu', '软件渗透了大量的脑力劳动', '软件渗透了大量的脑力劳动，人的逻辑思维、智能活动和技术水平是软件产品的关键', '2015-04-10 00:00:00', '2015-04-13 00:00:00', '1', '0', '0');
INSERT INTO `post` VALUES ('11', '2', '2', 'liu', '软件不会像硬件一样老化磨损', '软件不会像硬件一样老化磨损，但存在缺陷维护和技术更新', '2015-04-08 00:00:00', '2015-04-10 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('12', '2', '2', 'liu', '对于硬件有依赖性', '软件的开发和运行必须依赖于特定的计算机系统环境，对于硬件有依赖性，为了减少依赖，开发中提出了软件的可移植性', '2015-04-01 00:00:00', '2015-04-09 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('13', '2', '2', 'liu', '软件具有可复用性', '软件具有可复用性，软件开发出来很容易被复制，从而形成多个副本', '2015-04-05 00:00:00', '2015-04-13 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('14', '2', '2', 'liu', '系统软件为计算机', '系统软件为计算机使用提供最基本的功能，可分为操作系统和系统软件，其中操作系统是最基本的软件。', '2015-04-10 00:00:00', '2015-04-25 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('15', '2', '2', 'liu', '操作系统是一管理计算机硬件与软件资源的程序', '操作系统是一管理计算机硬件与软件资源的程序，同时也是计算机系统的内核与基石', '2015-04-17 00:00:00', '2015-04-19 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('16', '2', '2', 'liu', '支撑软件是支撑各种软件的开发与维护的软件', '支撑软件是支撑各种软件的开发与维护的软件，又称为软件开发环境（SDE）。它主要包括环境数据库、各种接口软件和工具组。', '2015-04-23 00:00:00', '2015-04-25 00:00:00', '1', '0', '0');
INSERT INTO `post` VALUES ('22', '2', '2', 'lqq', '点点滴滴', '<p>威威威威威威威威威威</p>', '2015-05-05 00:00:00', '2015-05-05 00:00:00', '26', '6', '0');
INSERT INTO `post` VALUES ('23', '2', '2', 'lqq', '企鹅速度嘎啊是', '<p>阿桑的歌发生的岗位的gvwe<br/></p>', '2015-05-05 00:00:00', '2015-05-05 00:00:00', '2', '0', '0');
INSERT INTO `post` VALUES ('25', '2', '2', 'lqq', '啊说话的话', '<p>大锅饭vasdgvasd</p>', '2015-05-05 00:00:00', '2015-05-05 00:00:00', '0', '0', '0');
INSERT INTO `post` VALUES ('26', '2', '2', 'lqq', '为他所噶山豆根', '<p>是的gvasdgRBasfgsA诗歌大赛夺冠</p>', '2015-05-05 00:00:00', '2015-05-05 00:00:00', '5', '1', '0');
INSERT INTO `post` VALUES ('27', '2', '2', 'lqq', 'dds', '<p>哈哈哈哈哈哈<br/></p>', '2015-05-05 00:00:00', '2015-05-05 00:00:00', '5', '1', '0');
INSERT INTO `post` VALUES ('28', '2', '2', 'lqq', 'qq', '<p>qqqqqq</p>', '2015-05-06 10:46:56', '2015-05-06 10:46:56', '165', '10', '0');
INSERT INTO `post` VALUES ('29', '3', '2', 'lqq', '信息安全是一门很重要的课程', '<p>信息安全是一门很重要的课程信息安全是一门很重要的课程信息安全是一门很重要的课程</p>', '2015-05-13 01:11:18', '2015-05-13 01:11:18', '7', '3', '0');
INSERT INTO `post` VALUES ('30', '3', '2', 'lqq', '信息安全啊嘎是个挖个葫芦娃', '<p>信息安全啊嘎是个挖个葫芦娃</p><p>信息安全啊嘎是个挖个葫芦娃</p>', '2015-05-13 01:18:47', '2015-05-13 01:18:47', '6', '4', '0');
INSERT INTO `post` VALUES ('31', '5', '2', 'lqq', '就算是贾大师', '<p><span style=\"color: rgb(39, 42, 48); font-family: &#39;Microsoft Yahei&#39;; line-height: 30px; text-align: justify; text-indent: 32px; background-color: rgb(255, 255, 255);\">特别想说“翻译老师，你个老伙计”。就算是贾大师，可能至少会译为“卧了个大槽/屌爆了”之类的。现在的翻译完全和之前“说脏话”的梗没有关联了</span></p>', '2015-05-13 01:22:17', '2015-05-13 01:22:17', '2', '0', '0');
INSERT INTO `post` VALUES ('32', '3', '2', 'lqq', '干啥干啥啊好你', '<p>干啥干啥啊好你干啥干啥啊好你干啥干啥啊好你</p>', '2015-05-15 09:23:50', '2015-05-15 09:23:50', '16', '22', '0');
INSERT INTO `post` VALUES ('33', '6', '2', 'lqq', 'dfadfa', '<p>dsdgfsadgasdgsdg</p>', '2015-06-06 16:08:10', '2015-06-06 16:08:10', '2', '0', '0');
INSERT INTO `post` VALUES ('34', '13', '2', 'lqq', '雅思是vgsdgdsb', '<p>傻不傻的公司不会喜欢白色 &nbsp;</p>', '2015-06-06 16:09:00', '2015-06-06 16:09:00', '0', '0', '0');
INSERT INTO `post` VALUES ('35', '15', '2', 'lqq', '学日语', '<p>学日语学日语学日语学日语学日语学日语学日语学日语学日语</p>', '2015-06-06 23:29:38', '2015-06-06 23:29:38', '7', '4', '0');
INSERT INTO `post` VALUES ('36', '11', '6', 'liu', '更合适的感受了好多个', '<p>是不会受到广泛士大夫发射点发射点呵呵听说或多或少的方法</p>', '2015-06-06 23:34:32', '2015-06-06 23:34:32', '0', '0', '0');
INSERT INTO `post` VALUES ('37', '14', '6', 'liu', 'fasdgasdg', '<p>&nbsp;电话嘎谁还敢发我拉黑了个好</p>', '2015-06-06 23:34:51', '2015-06-06 23:34:51', '0', '0', '0');
INSERT INTO `post` VALUES ('38', '4', '2', 'lqq', '网络是一门很有趣的学科', '<p>网络是一门很有趣的学科</p><p>网络是一门很有趣的学科</p><p>网络是一门很有趣的学科</p><p>网络是一门很有趣的学科</p>', '2015-06-09 17:19:50', '2015-06-09 17:19:50', '4', '1', '0');

-- ----------------------------
-- Table structure for `test`
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES ('1', 'q', 'q');

-- ----------------------------
-- Table structure for `theme`
-- ----------------------------
DROP TABLE IF EXISTS `theme`;
CREATE TABLE `theme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `themeName` varchar(255) NOT NULL,
  `academyId` bigint(20) DEFAULT NULL,
  `introduuction` text,
  `postNumber` bigint(20) DEFAULT '0',
  `color` varchar(10) DEFAULT 'blue',
  `icon` varchar(30) DEFAULT 'icon-ok',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of theme
-- ----------------------------
INSERT INTO `theme` VALUES ('1', '计算机', '123', '计算机硬件软件开发', '34', 'green', 'icon-signal');
INSERT INTO `theme` VALUES ('2', '外国语学院', '106', '英语，日语，俄语，韩语', '4', 'red', 'icon-bar-chart');
INSERT INTO `theme` VALUES ('3', '车辆工程学院', '100', '汽车，摩托车', '0', 'orange', 'icon-beaker');
INSERT INTO `theme` VALUES ('4', '材料科学与工程学院', '101', '材料化学，有机物', '0', 'lime', 'icon-bell');
INSERT INTO `theme` VALUES ('5', '思想政治教育学院 ', '105', '马克思，列宁毛泽东主义', '0', 'teal', 'icon-bolt');
INSERT INTO `theme` VALUES ('6', '机械工程学院', '108', '机械学院，机械', '0', 'blue', 'icon-book');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userName` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `academyId` bigint(20) NOT NULL COMMENT '学院编号',
  `role` int(11) NOT NULL DEFAULT '0' COMMENT '0为学生 1为老师',
  `sex` int(11) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(100) DEFAULT NULL,
  `nativePlace` varchar(255) DEFAULT NULL COMMENT '籍贯',
  `abode` varchar(255) DEFAULT NULL COMMENT '现居地',
  `plateId` bigint(20) DEFAULT NULL COMMENT '主题id  如果为0表示普通用户，不为0表示该主题的版主',
  `createDate` date DEFAULT NULL,
  `imagePath` varchar(255) NOT NULL DEFAULT 'images/tou.jpg',
  `isfrozen` int(11) NOT NULL DEFAULT '0' COMMENT '0表示不冻结1表示冻结',
  PRIMARY KEY (`id`),
  KEY `user_academyId` (`academyId`),
  CONSTRAINT `user_academyId` FOREIGN KEY (`academyId`) REFERENCES `academy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', 'lqq', '123456', '11103080316', '1161416766@qq.com', '2', '0', '0', '2011-02-18', '13545673657235', '江西', '重庆', null, '2015-04-15', 'images/c2611b8c-f005-4cb2-baad-abab3e5143b1.jpg', '0');
INSERT INTO `user` VALUES ('6', 'liu', '123456', '1161416766', 'liuqiang3518@126.com', '2', '0', null, null, null, '江西', null, null, '2015-04-26', 'images/tou.jpg', '0');
INSERT INTO `user` VALUES ('7', 'qq', 'faf', '45452', '23235', '3', '0', null, null, null, null, null, null, null, 'images/tou.jpg', '0');

-- ----------------------------
-- Table structure for `userfile`
-- ----------------------------
DROP TABLE IF EXISTS `userfile`;
CREATE TABLE `userfile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(255) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `isFile` int(11) DEFAULT NULL COMMENT '是否是标准文件 1为是 0为不是',
  `MD5` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT '0' COMMENT 'level为0的文件的parentlevel为-1',
  `parentId` bigint(11) DEFAULT '-1',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userfile_userId` (`userId`),
  CONSTRAINT `userfile_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userfile
-- ----------------------------
INSERT INTO `userfile` VALUES ('4', 'Wallpaper02.jpg', '2', '1', '7decf6080458058e83d55a2efdded22f', '0', '-1', '2015-05-15 10:11:58');
INSERT INTO `userfile` VALUES ('6', '安卓壁纸_风景a976.jpg', '2', '1', '6a3d6067b166a3f99f278d7ac3225dd4', '1', '3', '2015-05-15 10:42:43');
INSERT INTO `userfile` VALUES ('7', 'a5863815_s.jpg', '2', '1', '41cac72e90e2bd1ab6333d4ded4be618', '0', '-1', '2015-05-15 11:05:37');
INSERT INTO `userfile` VALUES ('9', 'Wallpaper.jpg', '2', '1', 'c598e5b016a5acef8890627888414cb', '0', '-1', '2015-05-15 11:05:38');
INSERT INTO `userfile` VALUES ('10', 'Wallpaper02.jpg', '2', '1', '7decf6080458058e83d55a2efdded22f', '0', '-1', '2015-05-15 11:05:38');
INSERT INTO `userfile` VALUES ('11', '风景696a.jpg', '2', '1', 'ea3c1efd1195ccc11de784e4eead2fa8', '0', '-1', '2015-05-15 11:09:51');
INSERT INTO `userfile` VALUES ('12', '74932B86883E.jpg', '2', '1', '277ba83f12fc0d2d17e5ac2c6f781f1f', '0', '-1', '2015-05-15 11:10:03');
INSERT INTO `userfile` VALUES ('13', '1334845207145.jpg', '2', '1', 'bf1246be2dbaa8b28b752738e4eea272', '0', '-1', '2015-05-15 11:12:05');
INSERT INTO `userfile` VALUES ('14', '安卓壁纸_艺术d24.jpg', '2', '1', 'ad9e83ad1376aba14b0b5c924780f6b', '0', '53', '2015-05-15 11:12:20');
INSERT INTO `userfile` VALUES ('42', '文档', '2', '0', null, '1', '-1', '2015-05-15 13:30:41');
INSERT INTO `userfile` VALUES ('43', 'Annie Lennox - I Put A Spell On You.mp3', '2', '1', 'c27de82e773be032dea4b099580b076', '0', '45', '2015-05-15 13:42:57');
INSERT INTO `userfile` VALUES ('45', '音乐', '2', '0', null, '1', '-1', '2015-05-15 13:43:50');
INSERT INTO `userfile` VALUES ('46', 'music', '2', '0', null, '3', '45', '2015-05-15 13:44:02');
INSERT INTO `userfile` VALUES ('47', '视频', '2', '0', null, '1', '-1', '2015-05-15 14:28:20');
INSERT INTO `userfile` VALUES ('48', 'java', '2', '0', null, '1', '-1', '2015-05-15 14:30:36');
INSERT INTO `userfile` VALUES ('51', '图片', '2', '0', null, '1', '-1', '2015-05-15 15:39:00');
INSERT INTO `userfile` VALUES ('52', '7C0FD2BDD77A.jpg', '2', '1', 'f13dda3b8131e07fc6668bc2395216da', '2', '51', '2015-05-15 15:39:07');
INSERT INTO `userfile` VALUES ('53', 'dd', '2', '0', null, '3', '51', '2015-05-15 17:56:32');
INSERT INTO `userfile` VALUES ('54', '7C0FD2BDD77A-副本-.jpg', '2', '1', 'f13dda3b8131e07fc6668bc2395216da', '4', '53', '2015-05-15 18:06:44');
INSERT INTO `userfile` VALUES ('56', '08143934_学位英语名单.xls', '2', '1', 'a75299993755fd7fe2429f072a156105', '0', '42', '2015-05-16 08:56:55');
INSERT INTO `userfile` VALUES ('57', 'jdbc封装(实现对实体的增删改查[分页]).zip', '2', '1', '6fbacbabce19bf222e5430eda43371f4', '0', '42', '2015-05-16 08:57:07');
INSERT INTO `userfile` VALUES ('58', 'jQuery-File-Upload-master.zip', '2', '1', '8d994fabbadcb47fb413ebe12b6cedc8', '0', '42', '2015-05-16 08:57:07');
INSERT INTO `userfile` VALUES ('59', '[Android开发从零开始].1.Android开发环境搭建.mp4', '2', '1', '224b53aa50a7f985e91806d9e19c975c', '0', '47', '2015-05-16 10:26:16');
INSERT INTO `userfile` VALUES ('62', '文档', '7', '0', null, '0', '-1', '2015-05-18 16:55:26');
INSERT INTO `userfile` VALUES ('63', '图片', '7', '0', null, '0', '-1', '2015-05-18 16:55:26');
INSERT INTO `userfile` VALUES ('64', '音乐', '7', '0', null, '0', '-1', '2015-05-18 16:55:26');
INSERT INTO `userfile` VALUES ('65', 'B.O.B、jessie J - Price Tag.ape', '2', '1', 'cd2a5b13aadae8a665fc83a385af6ace', '0', '-1', '2015-05-23 14:03:16');
INSERT INTO `userfile` VALUES ('66', 'ajaxfileupload.js', '2', '1', '9dcc37854e806b1609b069617a88e059', '0', '-1', '2015-06-08 17:55:47');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', '刘强强', '20', '2015-03-16', '杭州', '888888');
INSERT INTO `userinfo` VALUES ('2', 'liuqiangq', '22', '2015-03-16', '杭州', '888888');
INSERT INTO `userinfo` VALUES ('5', '芊芊', '31', '2015-03-31', '江西', '888888');
INSERT INTO `userinfo` VALUES ('6', 'qq', '18', '2015-04-21', 'qqq', '888888');
DROP TRIGGER IF EXISTS `commentNumUpdate`;
DELIMITER ;;
CREATE TRIGGER `commentNumUpdate` AFTER INSERT ON `comment` FOR EACH ROW begin  
        update post  
        set ReplyNumber= ReplyNumber+1 where id=NEW.postId ;  
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `postNumUpdate`;
DELIMITER ;;
CREATE TRIGGER `postNumUpdate` AFTER INSERT ON `post` FOR EACH ROW begin  
        update plate  
        set postNumber= postNumber+1 where id=NEW.plateId ;  
        update theme 
        set postNumber= postNumber+1 where id=(select p.themeId from plate p where p.id=NEW.plateId) ;  
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `adduserfilrforuser`;
DELIMITER ;;
CREATE TRIGGER `adduserfilrforuser` AFTER INSERT ON `user` FOR EACH ROW begin  
    insert  into userfile (fileName, userId, isFile, level, parentId, createDate)  values  ('文档',NEW.id ,0 , 0, -1, NOW());
    insert  into userfile (fileName, userId, isFile, level, parentId, createDate)  values  ('图片',NEW.id ,0 , 0, -1, NOW());
    insert  into userfile (fileName, userId, isFile, level, parentId, createDate)  values  ('音乐',NEW.id ,0 , 0, -1, NOW());
end
;;
DELIMITER ;
