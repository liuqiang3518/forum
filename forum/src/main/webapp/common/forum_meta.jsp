<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page  isELIgnored="false"%>  
<%
   //设置无缓存
   response.setHeader("progma","no-cache");   
   response.setHeader("Cache-Control","no-cache");   
%> 
    <c:set var="ctx" value="<%=request.getContextPath() %>"/>
    <script type="text/javascript" src="${ctx }/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap.min.js"></script>
    
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css"  href="${ctx }/css/blue.css">
	<link rel="stylesheet" type="text/css"  href="${ctx }/css/style.css">
	<link rel="stylesheet" type="text/css"  href="${ctx }/css/themes.css">
	<link rel="stylesheet" type="text/css"  href="${ctx }/font-awesome/css/font-awesome.css">
