//登录弹出框
function login(url){
	var mass="<form>";
	mass+="<div class='form-group'>";
	mass+="<input type='email' class='form-control' name='email'  placeholder='email'>";
	mass+="<div/>";
	mass+="<br>";
	mass+="<div class='form-group'>";
	mass+="<input type='password' class='form-control' name='password'  placeholder='password'>";
	mass+="<div/>";
	mass+="<form/>";
    BootstrapDialog.show({
    	title: '登录',
        message: mass,
        buttons: [{
            label: '登录',
            cssClass: 'btn-primary',
            action: function(dialogRef) {
            	var emailValue = dialogRef.getModalBody().find("input[name='email']").val();
            	var passwordValue=dialogRef.getModalBody().find("input[name='password']").val();
          	  userRemoting.loginJson(encode64(emailValue),encode64(passwordValue),function(data){
        		  if(data=="1"){
//        			  alert($("#loginform").attr("action"));
        			  if(null!=url&&url!=""){
        				  window.location.href=url;
        			  }else{
        				  window.location.href=$("#loginform").attr("action");
        			  }
                     }else if(data=="0"){
                         BootstrapDialog.alert({
                             title: '提醒',
                             message: '账号或密码错误!',
                             type: BootstrapDialog.TYPE_WARNING, 
                             closable: true, 
                             draggable: true,
                             buttonLabel: '确定', 
                         });
                     }else if(data=="2"){
                    	 BootstrapDialog.alert({
                             title: '提醒',
                             message: '该账号已冻结!',
                             type: BootstrapDialog.TYPE_WARNING, 
                             closable: true, 
                             draggable: true,
                             buttonLabel: '确定', 
                         });
                     }
        	  });
                return false;
            }
        }]
    });	
    return false;
}
