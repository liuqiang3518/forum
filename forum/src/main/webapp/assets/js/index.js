
  	$(document).ready(function() {
		var slider = new bootslider('#bootslider', {
			animationIn: "fadeInUp",
			animationOut: "flipOutX",
			timeout: 5000,
			autoplay: true,
			preload: true,
			pauseOnHover: false,
			thumbnails: true,
			pagination: true,
			mousewheel: false,
			keyboard: true,
			touchscreen: true
		});
		slider.init();
	});
  	
  //搜索
  	function search(){
  		var target=$("#search").val();
  		window.location.href="luceneSeach.do?target="+target;
  	}
