var bool;
  $(function(){

    $("#loginform").validation();
    //登录
    $("#loginbtn").on('click',function(event){
      // 2.最后要调用 valid()方法。
      if ($("#loginform").valid('填写信息不完整。')==false){
        return false;
      }else{
    	  //登录
    	  var email=encode64($('#email').val());
    	  var password=encode64($('#password').val());
    	  userRemoting.loginJson(email,password,function(data){
    		  if(data=="1"){
//    			  alert($("#loginform").attr("action"));
    			  var url=$("#url").val();
    			  if(null!=url&&url!=""){
    				  window.location.href=url;
    			  }else{
    				  window.location.href=$("#loginform").attr("action");
    			  }
                 }else if(data=="0"){
                     BootstrapDialog.alert({
                         title: '提醒',
                         message: '账号或密码错误!',
                         type: BootstrapDialog.TYPE_WARNING, 
                         closable: true, 
                         draggable: true,
                         buttonLabel: '确定', 
                     });
                 }else if(data=="2"){
                	 BootstrapDialog.alert({
                         title: '提醒',
                         message: '该账号已冻结!',
                         type: BootstrapDialog.TYPE_WARNING, 
                         closable: true, 
                         draggable: true,
                         buttonLabel: '确定', 
                     });
                 }
    	  });
	  }
      return false;
    });
    $("#regbtn").on('click',function(event){
    	window.location.href=$('#ctx').val()+"/regist.do"
    	return false;
    });
  });

