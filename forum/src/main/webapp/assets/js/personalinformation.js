  $(function(){

          $.ajax({
              url: "getUserJson.do",
              datatype: 'json',
              type: "Post",
              data: {},
              success: function (data) {
              	if(data!=null){
              		var user=eval("(" + data + ")").user;
              		if(null!=user){
              			$("#d_img").attr("src", user.imagePath);
              			$("#d_username").html(user.userName);
              			$("#d_email").html(user.email);
              			if(user.role=="0"){
              				$("#d_role").html("学生");
              				$("#dt_userid").html("学号：");
              			}else if(user.role=="1"){
              				$("#d_role").html("教师");
              				$("#dt_userid").html("教师号");
              			}
              			$("#d_userid").html(user.userId);
              			$("#d_academy").html(user.academyName);
              			if(user.sex=='0'){
              				$("#d_sex").html("男");
              			}else if(user.sex=='1'){
              				$("#d_sex").html("女");
              			}
              			$("#d_birthday").html(user.birthday);
              			$("#d_phoneNumber").html(user.phoneNumber);
              			$("#d_nativePlace").html(user.nativePlace);
              			$("#d_abode").html(user.abode);
              		}else{
              	           BootstrapDialog.alert({
              	               title: '提醒',
              	               message: '数据获取出错!',
              	               type: BootstrapDialog.TYPE_WARNING, 
              	               closable: true, 
              	               draggable: true,
              	               buttonLabel: '确定', 
              	           });	        			
              		}
              	}
              },
          });  
	  
	  return false;
  });
 