	
  	//如果用户已登录，获取用户名字
 	 $(function () {
 		var arrstr = document.cookie.split("; ");
 		for(var i = 0;i < arrstr.length;i ++){
 		  var temp = arrstr[i].split("=");
 		  if(temp[0] == 'forum_user'){
 			userRemoting.getUserName(function(data){
 				$("#userName").html(data);
 			});
 			return false;
 		  }
 		}
 		return false;
 	 });
 	//用户消息展示
 	$(function () {
 		$.ajax({
            url: "getUserMessageNum.do",
            datatype: 'json',
            type: "Post",
            data: {
            },
            success: function (data) {
            	if(data!=null){
            		var total=eval("(" + data + ")").total;
            		if(0<total){
            			$("#messageShow").html("<i class='icon-envelope-alt'></i><span class='label label-lightred'>"+total+"</span>");
            		}
            	}
            },
        }); 
 		return false;
 	 });
 	//用户注销
function logout(){
	userRemoting.logout(function(data){		
		if(data==1){
			window.location.reload();
		}else{
           BootstrapDialog.alert({
               title: '提醒',
               message: '注销出现错误!',
               type: BootstrapDialog.TYPE_WARNING, 
               closable: true, 
               draggable: true,
               buttonLabel: '确定', 
           });			
		}
	});
}
function personcenter(){
	window.location.href="personCenter.do?active=1";
}
function netdisk(){
	window.location.href="getnetdisk.do";
}