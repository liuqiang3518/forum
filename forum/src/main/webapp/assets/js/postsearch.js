    $(function () {
        $.ajax({
            url: "luceneSeachJson.do",
            datatype: 'json',
            type: "Post",
            data: {
            	"target":$("#target").val(),
            	"page":1
            },
            success: function (data) {
                if (data != null) {
                	var htm="";
                    $.each(eval("(" + data + ")").list, function (index, item) { //遍历返回的json
                        htm+=("<tr role='row'>");
                        htm+=("<td class='col-sm-6'><a href='postdetail.do?id="+item.id+"'>" + item.name+ "<a></td>");
                        htm+=("<td class='col-sm-2'>" + item.userName +"&nbsp;/"+item.postDateStr+ "</td>");
                        htm+=("<td class='col-sm-1'>"+item.pageView+"&nbsp;/"+item.replyNumber+"</td>");
                        htm+=("<td class='col-sm-2'>"+item.updateDateStr+"</td>");
                        htm+=('</tr>');
                    });
                    $("#list").html(htm);
                    var pageCount = eval("(" + data + ")").pageCount; //取到pageCount的值(把返回数据转成object类型)
                    var currentPage = eval("(" + data + ")").CurrentPage; //得到urrentPage
                    var options = {
                    	bootstrapMajorVersion: 3, //版本
                    	numberOfPages:5,
                        currentPage: currentPage, //当前页数
                        totalPages: pageCount, //总页数
                        itemTexts: function (type, page, current) {
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },//点击事件，用于通过Ajax来刷新整个list列表
                        onPageClicked: function (event, originalEvent, type, page) {
//                        	alert(page);
                        	$.ajax({
                                url: "luceneSeachJson.do",
                                type: "Post",
                                data:  {
                                	"target":$("#target").val(),
                                	"page":page
                                },
                                success: function (data2) {
                                    if (data2 != null) {
                                    	var htm2="";
                                        $.each(eval("(" + data2 + ")").list, function (index, item) { //遍历返回的json
                                            htm2+=("<tr role='row'>");
                                            htm2+=("<td class='col-sm-6'><a href='postdetail.do?id="+item.id+"'>" + item.name+ "<a></td>");
                                            htm2+=("<td class='col-sm-2'>" + item.userName +"&nbsp;/"+item.postDateStr+ "</td>");
                                            htm2+=("<td class='col-sm-1'>"+item.pageView+"&nbsp;/"+item.replyNumber+"</td>");
                                            htm2+=("<td class='col-sm-2'>"+item.updateDateStr+"</td>");
                                            htm2+=('</tr>');
                                        });
                                        $("#list").html(htm2);
                                    }
                                }
                            });
                        }
                    }
                    $('#pageshow').bootstrapPaginator(options);
                }
            }
        });
    });
    
  	$(document).ready(function() {
		var slider = new bootslider('#bootslider', {
			animationIn: "fadeInUp",
			animationOut: "flipOutX",
			timeout: 5000,
			autoplay: true,
			preload: true,
			pauseOnHover: false,
			thumbnails: true,
			pagination: true,
			mousewheel: false,
			keyboard: true,
			touchscreen: true
		});
		slider.init();
	});
  	
  //搜索
  	function search(){
  		var target=$("#search").val();
  		window.location.href="luceneSeach.do?target="+target;
  	}
