    $(function () {
    	loadPageData();
    });
//加载图片上传控件
  $(function(){	
	  var uploader = new plupload.Uploader({
			runtimes : 'html5,html4',
			browse_button : 'uploadsubmit', // you can pass in id...
			url : 'file/netdiskUpload.do',
			filters : {
				max_file_size : '500mb',
			},
			multipart_params: { 'level': $("#level").val(),'parentId': $("#parentId").val() },
			init: {
				PostInit: function() {
				},
				FilesAdded: function(up, files) {
					uploader.start();
					return false;
				},
				Error: function(up, err) {
   	           BootstrapDialog.alert({
  	               title: '提醒',
  	               message: '提交错误!',
  	               type: BootstrapDialog.TYPE_WARNING, 
  	               closable: true, 
  	               draggable: true,
  	               buttonLabel: '确定', 
  	           });	
				}
			}
		});
	  uploader.bind('UploadComplete',function(up,files){ 
//		  window.location.reload();
		  $("#upprogress").html("<span >成功上传"+up.total.uploaded+"个文件</span>");
		  loadPageData();
	  });
	  uploader.bind('UploadProgress',function(up,file){ 
		  var upprogress=$("#upprogress");
		  upprogress.css("width",file.percent+"%");
		  upprogress.html("<span >已上传进度："+up.total.uploaded+"/"+up.files.length+"</span>");
	  });
	    uploader.init();
  });
  //新建文件夹
  function newFile(){
	  var parentId=$("#parentId").val();
	  var parentLevel=$("#level").val();
		var mass="<form>";
		mass+="<div class='form-group'>";
		mass+="<input type='text' class='form-control' name='fileName'  placeholder='请输入新建文件名'>";
		mass+="<div/>";
		mass+="<form/>";
	    BootstrapDialog.show({
	    	title: '新建文件夹',
	        message: mass,
	        buttons: [{
	            label: '确定',
	            cssClass: 'btn-primary',
	            action: function(dialogRef) {
	            	var fileName = dialogRef.getModalBody().find("input[name='fileName']").val();
	            	$.ajax({
	                    url: "newFile.do",
	                    datatype: 'json',
	                    type: "Post",
	                    data: {
	                    	"parentLevel":parentLevel,
	                    	"parentId":parentId,
	                    	"fileName":fileName
	                    },
	                    success: function (data) {
	                    	if(data!=null){
	                    		var result=eval("(" + data + ")").result;
	                    		if(result==1){
	                    			loadPageData();
	                    			dialogRef.close();
	                    		}else{
	                    	           BootstrapDialog.alert({
	                    	               title: '提醒',
	                    	               message: '请求错误!',
	                    	               type: BootstrapDialog.TYPE_WARNING, 
	                    	               closable: true, 
	                    	               draggable: true,
	                    	               buttonLabel: '确定', 
	                    	           });	        			
	                    		}
	                    	}
	                    },
	                }); 
	                return false;
	            }
	        }]
	    });	
	    return false;
  }
  //加载表格数据
  function loadPageData(){
      $.ajax({
          url: "getnetdiskJson.do",
          datatype: 'json',
          type: "Post",
          data: {
          	"parentId":$("#parentId").val(),
          	"fileType":$("#fileType").val(),
          	"page":1
          },
          success: function (data) {
              if (data != null) {
              	
              	var htm="";
                  $.each(eval("(" + data + ")").list, function (index, item) { //遍历返回的json
                      htm+=("<tr role='row'>");
                      htm+="<td class='col-sm-8'>";
                      htm+="<div class='btn-group pull-right'>";  
                      htm+="<button type='button' class='btn btn-default dropdown-toggle'  data-toggle='dropdown'>";
                      htm+=" 操作&nbsp;<span class='caret'></span>";
                      htm+="</button>";
                      htm+="<ul class='dropdown-menu' role='menu'>";
                      htm+="<li><a href='javascript:filedownload("+item.id+")'>下载</a></li>";
                      htm+="<li><a href='javascript:fileReName("+item.id+",\""+item.fileName+"\")'>重命名</a></li>";
                      htm+="<li><a href='javascript:movefile("+item.id+")'>移动</a></li>";
                      htm+="<li><a href='javascript:filedelete("+item.id+")'>删除</a></li>";
                      htm+="</ul>";
                      htm+="</div>";
                      if(item.isFile=='0'){
                      	htm+="<a href='getnetdisk.do?parentLevel="+item.level+"&parentId="+item.id+"'>"
                      	htm+="<i class='icon-folder-close'></i>&nbsp;"+item.fileName; 
                      	htm+="</a>";
                      	
                      	htm+="</td>";
                      	htm+=("<td class='col-sm-2'>—</td>");
                      }if(item.isFile=='1'){
                      	if(item.filestore.filetype=='0'){
                      		htm+="<i class='icon-cloud'></i>"+item.fileName;
                      	}if(item.filestore.filetype=='1'){
                      		htm+="<i class='icon-file'></i>&nbsp;"+item.fileName;
                      	}if(item.filestore.filetype=='2'){
                      		htm+="<i class='icon-music'></i>&nbsp;"+item.fileName;
                      	}if(item.filestore.filetype=='3'){
                      		htm+="<i class='icon-film'></i>&nbsp;"+item.fileName;
                      	}if(item.filestore.filetype=='4'){
                      		htm+="<i class='icon-picture'></i>&nbsp;"+item.fileName;
                      	}
                      	htm+="</td>";
                      	htm+=("<td class='col-sm-2'>"+item.filestore.filesize+ "</td>");
                      }
                      htm+=("<td class='col-sm-2'>"+item.createDateStr+"</td>");
                      htm+=('</tr>');
                  });
                  $("#list").html(htm);
                  var pageCount = eval("(" + data + ")").pageCount; //取到pageCount的值(把返回数据转成object类型)
                  var currentPage = eval("(" + data + ")").CurrentPage; //得到urrentPage
                  var options = {
                  	bootstrapMajorVersion: 3, //版本
                  	numberOfPages:5,
                      currentPage: currentPage, //当前页数
                      totalPages: pageCount, //总页数
                      itemTexts: function (type, page, current) {
                          switch (type) {
                              case "first":
                                  return "首页";
                              case "prev":
                                  return "上一页";
                              case "next":
                                  return "下一页";
                              case "last":
                                  return "末页";
                              case "page":
                                  return page;
                          }
                      },//点击事件，用于通过Ajax来刷新整个list列表
                      onPageClicked: function (event, originalEvent, type, page) {
//                      	alert(page);
                      	$.ajax({
                              url: "getnetdiskJson.do",
                              type: "Post",
                              data:  {
                            	"parentId":$("#parentId").val(),
                            	"fileType":$("#fileType").val(),
                              	"page":page
                              },
                              success: function (data2) {
                                  if (data2 != null) {
                                  	var htm2="";
                                      $.each(eval("(" + data2 + ")").list, function (index, item) { //遍历返回的json
                                          htm2+=("<tr role='row'>");
                                          htm2+="<td class='col-sm-8'>";
                                          htm2+="<div class='btn-group pull-right'>";  
                                          htm2+="<button type='button' class='btn btn-default dropdown-toggle'  data-toggle='dropdown'>";
                                          htm2+=" 操作&nbsp;<span class='caret'></span>";
                                          htm2+="</button>";
                                          htm2+="<ul class='dropdown-menu' role='menu'>";
                                          htm2+="<li><a href='javascript:filedownload("+item.id+")'>下载</a></li>";
                                          htm2+="<li><a href='javascript:fileReName("+item.id+",\""+item.fileName+"\")'>重命名</a></li>";
                                          htm2+="<li><a href='javascript:movefile("+item.id+")'>移动</a></li>";
                                          htm2+="<li><a href='javascript:filedelete("+item.id+")'>删除</a></li>";
                                          htm2+="</ul>";
                                          htm2+="</div>";
                                          if(item.isFile=='0'){
                                          	htm2+="<a href='getnetdisk.do?parentLevel="+item.level+"&parentId="+item.id+"'>"
                                          	htm2+="<i class='icon-folder-close'></i>&nbsp;"+item.fileName; 
                                          	htm2+="</a>";
                                          	htm2+="</td>";
                                          	htm2+=("<td class='col-sm-2'>—</td>");
                                          }if(item.isFile=='1'){
                                          	if(item.filestore.filetype=='0'){
                                          		htm2+="<i class='icon-cloud'></i>"+item.fileName;
                                          	}if(item.filestore.filetype=='1'){
                                          		htm2+="<i class='icon-file'></i>&nbsp;"+item.fileName;
                                          	}if(item.filestore.filetype=='2'){
                                          		htm2+="<i class='icon-music'></i>&nbsp;"+item.fileName;
                                          	}if(item.filestore.filetype=='3'){
                                          		htm2+="<i class='icon-film'></i>&nbsp;"+item.fileName;
                                          	}if(item.filestore.filetype=='4'){
                                          		htm2+="<i class='icon-picture'></i>&nbsp;"+item.fileName;
                                          	}
                                          	htm2+="</td>";
                                          	htm2+=("<td class='col-sm-2'>"+item.filestore.filesize+ "</td>");
                                          }
                                          htm2+=("<td class='col-sm-2'>"+item.createDateStr+"</td>");
                                          htm2+=('</tr>');
                                      });
                                      $("#list").html(htm2);
                                  }
                              }
                          });
                      }
                  }
                  $('#pageshow').bootstrapPaginator(options);
              }
          }
      });
  }
  //文件重命名
  function fileReName(id,fileName){
	  var mass="<form>";
		mass+="<div class='form-group'>";
		mass+="<input type='text' class='form-control' name='fileName' value="+fileName+" placeholder='请输入新建文件名'>";
		mass+="<div/>";
		mass+="<form/>";
	    BootstrapDialog.show({
	    	title: '文件重命名',
	        message: mass,
	        buttons: [{
	            label: '确定',
	            cssClass: 'btn-primary',
	            action: function(dialogRef) {
	            	var fileName = dialogRef.getModalBody().find("input[name='fileName']").val();
	            	$.ajax({
	                    url: "fileReName.do",
	                    datatype: 'json',
	                    type: "Post",
	                    data: {
	                    	"userfileId":id,
	                    	"userfileName":fileName
	                    },
	                    success: function (data) {
	                    	if(data!=null){
	                    		var result=eval("(" + data + ")").result;
	                    		if(result==1){
	                    			loadPageData();
	                    			dialogRef.close();
	                    		}else{
	                    	           BootstrapDialog.alert({
	                    	               title: '提醒',
	                    	               message: '请求错误!',
	                    	               type: BootstrapDialog.TYPE_WARNING, 
	                    	               closable: true, 
	                    	               draggable: true,
	                    	               buttonLabel: '确定', 
	                    	           });	        			
	                    		}
	                    	}
	                    },
	                }); 
	                return false;
	            }
	        }]
	    });	
	    return false;
  }
  function filedelete(id){
	  $.ajax({
          url: "deleteUserfile.do",
          datatype: 'json',
          type: "Post",
          data: {
          	"userfileId":id
          },
          success: function (data) {
          	if(data!=null){
          		var result=eval("(" + data + ")").result;
          		if(result==1){
          			loadPageData();
          			dialogRef.close();
          		}else{
          	           BootstrapDialog.alert({
          	               title: '提醒',
          	               message: '请求错误!',
          	               type: BootstrapDialog.TYPE_WARNING, 
          	               closable: true, 
          	               draggable: true,
          	               buttonLabel: '确定', 
          	           });	        			
          		}
          	}
          },
      }); 
      return false;
  }
  function filedownload(id){
	  window.location.href="netdiskLoadFile.do?userfileId="+id;
      return false;
  }
  //文件移动   
  var clickValue;
  function movefile(id){
	  var ztree;
	  var setting = {
	            data:    {
	                simpleData:{
	                    enable:true
	                }
	            },
				callback: {
					onClick: zTreeOnClick
				}
			};
	  var zNodes =[];
	    var mass="<ul id='treeDemo' class='ztree' name='treeDemo'></ul>";
//		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
	    BootstrapDialog.show({
	    	title: '文件转移',
	        message: mass,
	        onshow: function(dialogRef){//获取网盘目录
	        	 $.ajax({
	                 url: "getDirectoryJson.do",
	                 datatype: 'json',
	                 type: "Post",
	                 data: {
	                 },
	                 success: function (data) {
	                 	if(data!=null){
	                 		var result=eval("(" + data + ")").result;
	                 		var lists=eval("(" + data + ")").lists;
	                 		if(result==1){
	                 			var treeDemo = dialogRef.getModalBody().find("ul[name='treeDemo']");
	                 			ztree=$.fn.zTree.init(treeDemo, setting, lists);
	                 		}else{
	                 	           BootstrapDialog.alert({
	                 	               title: '提醒',
	                 	               message: '请求错误!',
	                 	               type: BootstrapDialog.TYPE_WARNING, 
	                 	               closable: true, 
	                 	               draggable: true,
	                 	               buttonLabel: '确定', 
	                 	           });	        			
	                 		}
	                 	}
	                 },
	             }); 
            },
	        buttons: [{
	            label: '确定',
	            cssClass: 'btn-primary',
	            action: function(dialogRef) {//文件转移
	            	if(clickValue==null||clickValue==""){
	            		dialogRef.setTitle('请选择目录');
	            		dialogRef.setCssClass("login-dialog");
	            		return false;
	            	}
	            	$.ajax({
		                 url: "fileMove.do",
		                 datatype: 'json',
		                 type: "Post",
		                 data: {
		                 	"userfileId":id,
		                 	"targetId":clickValue
		                 },
		                 success: function (data) {
		                 	if(data!=null){
		                 		var result=eval("(" + data + ")").result;
		                 		if(result==1){
	                    			dialogRef.close();
	                    			loadPageData();
		                 		}else{
		                 	           BootstrapDialog.alert({
		                 	               title: '提醒',
		                 	               message: '请求错误!',
		                 	               type: BootstrapDialog.TYPE_WARNING, 
		                 	               closable: true, 
		                 	               draggable: true,
		                 	               buttonLabel: '确定', 
		                 	           });	        			
		                 		}
		                 	}
		                 },
		             }); 
	                return false;
	            }
	        }]
	    });	
	    return false;
  }
  function zTreeOnClick(event, treeId, treeNode) {
	  clickValue=treeNode.id;
	};
	
function navChoose(num){
	var ctx=$("#ctx").val();
	var fileType=-1;
	if(num==-1){
		fileType=-1;
	}if(num==0){
		fileType=0;
	}if(num==1){
		fileType=1;
	}if(num==2){
		fileType=2;
	}if(num==3){
		fileType=3;
	}if(num==4){
		fileType=4;
	}
	$("#fileType").val(fileType);
	$("#parentId").val(-1);
	$("#level").val(0);
	for(var i=-1;i<=4;i++){
		if(num!=i) $("#subnav_li_"+i).removeClass("active"); 
		if(num==i) $("#subnav_li_"+i).addClass("active"); 
	}
	loadPageData();
}