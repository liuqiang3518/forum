  $(function(){	  
      $.ajax({
          url: "getUserJson.do",
          datatype: 'json',
          type: "Post",
          data: {},
          success: function (data) {
          	if(data!=null){
          		var user=eval("(" + data + ")").user;
          		if(null!=user){
          			$("#p_img").attr("src", user.imagePath);
          			$("#p_email").val(user.email);
          			$("#p_email").attr("readonly","");
          			$("#p_username").val(user.userName);
          			if(user.role=="0"){
          				$("#p_role").val("学生");
          				$("#ps_userid").html("学号");
          			}else if(user.role=="1"){
          				$("#p_role").val("教师");
          				$("#ps_userid").html("教师号");
          			}
          			$("#p_role").attr("readonly","");
          			$("#p_userid").val(user.userId);
          			$("#p_academy").val(user.academyId);
          			if(user.sex=='0'){
          				$("#p_sex").val('0');
          			}else if(user.sex=='1'){
          				$("#p_sex").val('1');
          			}
          			$("#p_birthday").val(user.birthday);
          			$("#p_phoneNumber").val(user.phoneNumber);
          			$("#p_nativePlace").val(user.nativePlace);
          			$("#p_abode").val(user.abode);
          		}else{
          	           BootstrapDialog.alert({
          	               title: '提醒',
          	               message: '数据获取出错!',
          	               type: BootstrapDialog.TYPE_WARNING, 
          	               closable: true, 
          	               draggable: true,
          	               buttonLabel: '确定', 
          	           });	        			
          		}
          	}
          },
      }); 
	  
	  return false;
  });
  //加载图片上传控件
  $(function(){	
	  var uploader = new plupload.Uploader({
			runtimes : 'html5,html4',
			browse_button : 'photosubmit', // you can pass in id...
			url : 'file/uploadPhotos.do',
			multi_selection: false,
			filters : {
				max_file_size : '10mb',
				mime_types: [
					{title : "Image files", extensions : "jpg,png"},
				]
			},

			init: {
				PostInit: function() {
				},
				FilesAdded: function(up, files) {
					uploader.start();
					return false;
				},
				Error: function(up, err) {
   	           BootstrapDialog.alert({
  	               title: '提醒',
  	               message: '提交错误!',
  	               type: BootstrapDialog.TYPE_WARNING, 
  	               closable: true, 
  	               draggable: true,
  	               buttonLabel: '确定', 
  	           });	
				}
			}
		});
	  uploader.bind('UploadComplete',function(up,files){ 
		  window.location.reload();
	  });
	  
	    uploader.init();
  });

  $(function(){
	  $("#updadeform").validation();
	  $("#updatesubmit").on('click',function(event){
		  if ($("#updadeform").valid('填写信息不完整。')==false){
		        //alert('填写信息不完整。');
		        return false;
		  }else{
              $.ajax({
                  url: "getPersonUpdateJson.do",
                  datatype: 'json',
                  type: "Post",
                  data: {
                  	"username":$("#p_username").val(),
                  	"role":$("#p_role").val(),
                  	"userid":$("#p_userid").val(),
                  	"email":$("#p_email").val(),
                  	"academyid":$("#p_academy").val(),
                  	"sex":$("#p_sex").val(),
                  	"birthday":$("#p_birthday").val(),
                  	"phoneNumber":$("#p_phoneNumber").val(),
                  	"nativePlace":$("#p_nativePlace").val(),
                  	"abode":$("#p_abode").val(),
                  },
                  success: function (data) {
                  	if(data!=null){
                  		var result=eval("(" + data + ")").result;
                  		if(result==1){
                  			BootstrapDialog.show({
                  	            title: '确认',
                  	            message: '信息修改成功',
                  	            buttons: [{
                  	                label: '确认',
                  	                cssClass: 'btn-primary',
                  	                action: function(dialog) {
                  	                	window.location.reload();
                  	                	 dialogItself.close();
                  	                }
                  	            }]
                  	        });
                  		}else{
                  	           BootstrapDialog.alert({
                  	               title: '提醒',
                  	               message: '提交错误!',
                  	               type: BootstrapDialog.TYPE_WARNING, 
                  	               closable: true, 
                  	               draggable: true,
                  	               buttonLabel: '确定', 
                  	           });	        			
                  		}
                  	}
                  },
              });   
		  }
		  return false;
	  });
	  return false;
  });
  //文件后缀
  function test(file_name){
	  var result =/\.[^\.]+/.exec(file_name);
	  return result;
	  }