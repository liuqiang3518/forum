    $(function () {
        $.ajax({
            url: "systemMessageJson.do",
            datatype: 'json',
            type: "Post",
            data: {
            	"page":1
            },
            success: function (data) {
                if (data != null) {
                	var htm="";
                    $.each(eval("(" + data + ")").list, function (index, item) { //遍历返回的json
                        htm+=("<tr role='row'>");
                        htm+="<td class='col-sm-10'><a href='javascript:sytemMessageRead("+item.id+")'>";
                        if(item.isRead==0){
                        	htm+="<strong>"+item.title+"</strong>";
                        }else{
                        	htm+=item.title;
                        }
                        htm+="<a></td>";
                        htm+=("<td class='col-sm-2'>" +item.createDateStr+ "</td>");
                        htm+=('</tr>');
                    });
                    $("#list").html(htm);
                    var pageCount = eval("(" + data + ")").pageCount; //取到pageCount的值(把返回数据转成object类型)
                    var currentPage = eval("(" + data + ")").CurrentPage; //得到urrentPage
                    var options = {
                    	bootstrapMajorVersion: 3, //版本
                    	numberOfPages:5,
                        currentPage: currentPage, //当前页数
                        totalPages: pageCount, //总页数
                        itemTexts: function (type, page, current) {
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },//点击事件，用于通过Ajax来刷新整个list列表
                        onPageClicked: function (event, originalEvent, type, page) {
//                        	alert(page);
                        	$.ajax({
                                url: "systemMessageJson.do",
                                type: "Post",
                                data:  {
                                	"page":page
                                },
                                success: function (data2) {
                                    if (data2 != null) {
                                    	var htm2="";
                                        $.each(eval("(" + data2 + ")").list, function (index, item) { //遍历返回的json
                                            htm2+=("<tr role='row'>");
                                            htm+="<td class='col-sm-10'><a href='javascript:sytemMessageRead("+item.id+")'>";
                                            if(item.isRead==0){
                                            	htm2+="<strong>"+item.title+"</strong>";
                                            }else{
                                            	htm2+=item.title;
                                            }
                                            htm2+="<a></td>";
                                            htm2+=("<td class='col-sm-2'>" +item.createDateStr+ "</td>");
                                            htm2+=('</tr>');
                                        });
                                        $("#list").html(htm2);
                                    }
                                }
                            });
                        }
                    }
                    $('#pageshow').bootstrapPaginator(options);
                }
            }
        });
  });
    function sytemMessageRead(id){
    	$.ajax({
            url: "systemMessageReader.do",
            datatype: 'json',
            type: "Post",
            data: {
            	"id":id
            },
            success: function (data) {
            	if(data!=null){
            		var message=eval("(" + data + ")").message;
            		if(null!=message){
            			BootstrapDialog.show({
            		    	title: '消息内容',
            		        message: message.content,
            		        buttons: [{
            		            label: '确定',
            		            cssClass: 'btn-primary',
            		            action: function(dialogRef) {
            		            	dialogRef.close();
            		            	window.location.reload();
            		            }
            		        }]
            		    });	
            		}else{
            			BootstrapDialog.alert({
         	               title: '提醒',
         	               message: '消息获取错误!',
         	               type: BootstrapDialog.TYPE_WARNING, 
         	               closable: true, 
         	               draggable: true,
         	               buttonLabel: '确定', 
         	           });	
            		}
            	}
            },
        }); 
    	return false;
  }
 