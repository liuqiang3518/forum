var bool;
  $(function(){

    $("#regform").validation();
    //.注册
    $("#regbtn").on('click',function(event){
      // 2.最后要调用 valid()方法。
      if ($("#regform").valid('填写信息不完整。')==false){
        //alert('填写信息不完整。');
        return false;
      }else{
	      var el=$("#repassword");
	      if(el.val()!=$("#password").val()){
		     var controlGroup = el.parents('.form-group');
             controlGroup.removeClass('has-error has-success');
			 controlGroup.addClass('has-error');
			 controlGroup.find("#valierr").remove();
			 el.parent().after('<span class="help-block" id="valierr" >确认密码和密码必须一致</span>');
			 return false;
		  }else{
			  var name=$('#name').val();
			  var userId=$('#userId').val();
			  var academy=$('#academy').val();
			  var password=$('#password').val();
			  var email=$('#email').val();
			  userRemoting.registJson(name,userId,academy,password, email,function(data){
				  if(data==0){
	                     BootstrapDialog.alert({
	                         title: '提醒',
	                         message: '对不起，注册出现错误！',
	                         type: BootstrapDialog.TYPE_WARNING, 
	                         closable: true, 
	                         draggable: true,
	                         buttonLabel: '确定', 
	                     });
				  }if(data==1){
	                     BootstrapDialog.alert({
	                         title: '提醒',
	                         message: '该邮箱已经注册过本网站！',
	                         type: BootstrapDialog.TYPE_WARNING, 
	                         closable: true, 
	                         draggable: true,
	                         buttonLabel: '确定', 
	                     });					  
				  }if(data==2){
					  BootstrapDialog.show({
						    title: '提醒',
						    message: '恭喜您，您的注册申请已成功，请注意查收激活邮件',
						    buttons: [{
						        id: 'btn-ok',         
						        label: 'OK',
						        cssClass: 'btn-primary', 
						        autospin: false,
						        action: function(){    
						        	window.location.reload();
						        }
						    }]
						});					  
				  }
			  });
		  }
	  }
       return false;
    });
  });
