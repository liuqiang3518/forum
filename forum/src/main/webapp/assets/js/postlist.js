    $(function () {
        $.ajax({
            url: "plateDetail.do",
            datatype: 'json',
            type: "Post",
            data: {
            	"plateId":$("#plateid").val(),
            	"page":1
            },
            success: function (data) {
                if (data != null) {
                	var htm="";
                    $.each(eval("(" + data + ")").list, function (index, item) { //遍历返回的json
                        htm+=("<tr role='row'>");
                        htm+=("<td class='col-sm-6'><a href='postdetail.do?id="+item.id+"'>" + item.name+ "<a></td>");
                        htm+=("<td class='col-sm-3'>" + item.userName +"/"+item.postDateStr+ "</td>");
                        htm+=("<td class='col-sm-1'>"+item.pageView+"/"+item.replyNumber+"</td>");
                        htm+=("<td class='col-sm-2'>"+item.updateDateStr+"</td>");
                        htm+=('</tr>');
                    });
                    $("#list").html(htm);
                    var pageCount = eval("(" + data + ")").pageCount; //取到pageCount的值(把返回数据转成object类型)
                    var currentPage = eval("(" + data + ")").CurrentPage; //得到urrentPage
                    var options = {
                    	bootstrapMajorVersion: 3, //版本
                    	numberOfPages:5,
                        currentPage: currentPage, //当前页数
                        totalPages: pageCount, //总页数
                        itemTexts: function (type, page, current) {
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },//点击事件，用于通过Ajax来刷新整个list列表
                        onPageClicked: function (event, originalEvent, type, page) {
//                        	alert(page);
                        	$.ajax({
                                url: "plateDetail.do",
                                type: "Post",
                                data:  {
                                	"plateId":$("#plateid").val(),
                                	"page":page
                                },
                                success: function (data2) {
                                    if (data2 != null) {
                                    	var htm2="";
                                        $.each(eval("(" + data2 + ")").list, function (index, item) { //遍历返回的json
                                            htm2+=("<tr role='row'>");
                                            htm2+=("<td class='col-sm-6'><a href='postdetail.do?id="+item.id+"'>" + item.name+ "<a></td>");
                                            htm2+=("<td class='col-sm-3'>" + item.userName +"/"+item.postDateStr+ "</td>");
                                            htm2+=("<td class='col-sm-1'>"+item.pageView+"/"+item.replyNumber+"</td>");
                                            htm2+=("<td class='col-sm-2'>"+item.updateDateStr+"</td>");
                                            htm2+=('</tr>');
                                        });
                                        $("#list").html(htm2);
                                    }
                                }
                            });
                        }
                    }
                    $('#pageshow').bootstrapPaginator(options);
                }
            }
        });
        
        //提交按钮事件绑定
        $("#submitbtn").on('click',function(event){
        	//先判断用户是否已登录
     		var arrstr = document.cookie.split("; ");
     		var islogin=false;
     		for(var i = 0;i < arrstr.length;i ++){
     		  var temp = arrstr[i].split("=");
     		  if(temp[0] == 'forum_user'){
     			 islogin=true;
     			 break;
     		  }
     		}
     		if(!islogin){
     			login(document.URL);
     			return false;
     		}
        	
     		//已登录，提交数据
        	var content=UE.getEditor('newsEditor').getContent();
        	var plate=$("#plate").val();
        	var title=$("#title").val();
        	if(0==plate){
 	           BootstrapDialog.alert({
	               title: '提醒',
	               message: '请选择主题分类!',
	               type: BootstrapDialog.TYPE_WARNING, 
	               closable: true, 
	               draggable: true,
	               buttonLabel: '确定', 
	           });	
 	           return false;
        	}
        	if(null==title||title.length<1){
   	           BootstrapDialog.alert({
	               title: '提醒',
	               message: '请输入标题!',
	               type: BootstrapDialog.TYPE_WARNING, 
	               closable: true, 
	               draggable: true,
	               buttonLabel: '确定', 
	           });
  	           return false;       		
        	}
        	if(content==null||content==''){
        		BootstrapDialog.alert({
 	               title: '提醒',
 	               message: '请填写内容!',
 	               type: BootstrapDialog.TYPE_WARNING, 
 	               closable: true, 
 	               draggable: true,
 	               buttonLabel: '确定', 
 	           });	
        	   return false;
        	}
        	if(content.length<10){
  	           BootstrapDialog.alert({
	               title: '提醒',
	               message: '输入长度不能小于10!',
	               type: BootstrapDialog.TYPE_WARNING, 
	               closable: true, 
	               draggable: true,
	               buttonLabel: '确定', 
	           });
  	           return false;
        	}
            $.ajax({
                url: "sendPost.do",
                datatype: 'json',
                type: "Post",
                data: {
                	"plate":$("#plate").val(),
                	"content":content,
                	"title":title
                },
                success: function (data) {
                	if(data!=null){
                		var result=eval("(" + data + ")").result;
                		if(result==1){
                			window.location.reload();
                		}else{
                	           BootstrapDialog.alert({
                	               title: '提醒',
                	               message: '提交错误!',
                	               type: BootstrapDialog.TYPE_WARNING, 
                	               closable: true, 
                	               draggable: true,
                	               buttonLabel: '确定', 
                	           });	        			
                		}
                	}
                },
            }); 
        	return false;
        });
        
        
    });
    
  	$(document).ready(function() {
		var slider = new bootslider('#bootslider', {
			animationIn: "fadeInUp",
			animationOut: "flipOutX",
			timeout: 5000,
			autoplay: true,
			preload: true,
			pauseOnHover: false,
			thumbnails: true,
			pagination: true,
			mousewheel: false,
			keyboard: true,
			touchscreen: true
		});
		slider.init();
	});
  	//加载编译器
  	$(document).ready(function() {
        var editor=new baidu.editor.ui.Editor();
        editor.render("newsEditor");  
  	});
  //搜索
  	function search(){
  		var target=$("#search").val();
  		window.location.href="luceneSeach.do?target="+target;
  	}