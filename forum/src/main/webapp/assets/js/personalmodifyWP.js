  $(function(){
	  $("#updadeform").validation();
	  $("#modifysubmit").on('click',function(event){
		  if ($("#updadeform").valid('填写信息不完整。')==false){
		        //alert('填写信息不完整。');
		        return false;
		  }else{
			  var newpassword=$("#newpassword").val();
			  var renewpassword=$("#renewpassword");
			  if(newpassword!=renewpassword.val()){
				  var controlGroup = renewpassword.parents('.form-group');
				  controlGroup.addClass('has-error');
                  controlGroup.find("#valierr").remove();
                  renewpassword.parent().after('<span class="help-block" id="valierr">新密码和确认密码不匹配</span>');
                  return false;
			  }else{
				  var controlGroup = renewpassword.parents('.form-group');
				  controlGroup.addClass('has-success');
                  controlGroup.find("#valierr").remove();
                  $.ajax({
                      url: "modifyPasswordJson.do",
                      datatype: 'json',
                      type: "Post",
                      data: {
                      	"password":$("#password").val(),
                      	"newpassword":newpassword
                      },
                      success: function (data) {
                      	if(data!=null){
                      		var result=eval("(" + data + ")").result;
                      		if(result==1){
                      			BootstrapDialog.show({
                      	            title: '确认',
                      	            message: '信息修改成功',
                      	            buttons: [{
                      	                label: '确认',
                      	                cssClass: 'btn-primary',
                      	                action: function(dialog) {
                      	                	window.location.reload();
                      	                	 dialogItself.close();
                      	                }
                      	            }]
                      	        });
                      		}else if(result=='-1'){
                      	           BootstrapDialog.alert({
                      	               title: '提醒',
                      	               message: '旧密码错误!',
                      	               type: BootstrapDialog.TYPE_WARNING, 
                      	               closable: true, 
                      	               draggable: true,
                      	               buttonLabel: '确定', 
                      	           });	        			
                      		}else{
                      			BootstrapDialog.alert({
                   	               title: '提醒',
                   	               message: '提交错误!',
                   	               type: BootstrapDialog.TYPE_WARNING, 
                   	               closable: true, 
                   	               draggable: true,
                   	               buttonLabel: '确定', 
                   	           });	
                      		}
                      	}
                      },
                  });   
			  }
		  }
	  return false;
	  });
  });
 