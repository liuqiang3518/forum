    $(function () {
        $.ajax({
            url: "postJson.do",
            datatype: 'json',
            type: "Post",
            data: {
            	"postId":$("#postid").val(),
            	"page":1
            },
            success: function (data) {
                if (data != null) {
                	var htm="";
                    $.each(eval("(" + data + ")").list, function (index, item) { //遍历返回的json				        
                    	htm+="<tr role='row'>";
					    htm+="<td class='col-sm-3' style='background: #f7f9fb;'>";
						htm+="<div style='min-height:100px;max-height:auto;text-align:center;'>";
						htm+="<div class='row-fluiid' >";
						htm+="<img  src="+item.user.imagePath+" style='width:80px; height:80px;'>";
						htm+="</div>";
						htm+="<div class='row-fluid'>";
						htm+="<a href=''>"+item.user.userName+"</a>";										   
						htm+="</div>";
						htm+="</div>";
						htm+="</td>";
						htm+="<td class='col-sm-9'>";
						htm+="<div class='row-fluid'>";
						htm+="<div class='col-sm-offset-1 col-sm-11' style='margin-bottom:20px;'>";
					    htm+="<span>发表于：</span><span>"+item.dateStr+"</span>&nbsp;&nbsp;&nbsp;&nbsp;";
					    htm+="<a href='javascript:void();' onclick='reply(\""+item.user.userName+"\" ,\""+item.user.id+"\" ,\""+item.id+"\");'>回复</a>";
						htm+="</div>";										   
						htm+="</div>";
						htm+="<div class='row-fluid'>";
						htm+=item.content
						htm+="</div>";	
						htm+="<div class='row-fluid'>";
						htm+="<div class='col-sm-offset-1 col-sm-11' style='margin-top:20px;'>";
						for(var i=0;i<item.lists.length;i++){
							htm+="<div class='row-fluid'>";
							htm+="<a>"+item.lists[i].user.userName+"</a>回复<a>"+item.lists[i].tuser.userName+"</a>："+item.lists[i].content;
							htm+="</div><br/>";
						}
						htm+="</div>";
						htm+="</div>";
						htm+="</td>";
					    htm+="</tr>";                      
                    });
                    $("#list").html(htm);
                    var pageCount = eval("(" + data + ")").pageCount; //取到pageCount的值(把返回数据转成object类型)
                    var currentPage = eval("(" + data + ")").CurrentPage; //得到urrentPage
                    var options = {
                    	bootstrapMajorVersion: 3, //版本
                    	numberOfPages:5,
                        currentPage: currentPage, //当前页数
                        totalPages: pageCount, //总页数
                        itemTexts: function (type, page, current) {
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },//点击事件，用于通过Ajax来刷新整个list列表
                        onPageClicked: function (event, originalEvent, type, page) {
//                        	alert(page);
                        	$.ajax({
                                url: "postJson.do",
                                type: "Post",
                                data:  {
                                	"postId":$("#postid").val(),
                                	"page":page
                                },
                                success: function (data2) {
                                    if (data2 != null) {
                                    	var htm2="";
                                        $.each(eval("(" + data2 + ")").list, function (index, item) { //遍历返回的json
                                        	htm2+="<tr role='row'>";
                                        	htm2+="<td class='col-sm-3' style='background: #f7f9fb;'>";
                                        	htm2+="<div style='min-height:100px;max-height:auto;text-align:center;'>";
                                        	htm2+="<div class='row-fluiid' >";
                                        	htm2+="<img  src="+item.user.imagePath+" style='width:80px; height:80px;'>";
                                        	htm2+="</div>";
                                        	htm2+="<div class='row-fluid'>";
                                        	htm2+="<a href=''>"+item.user.userName+"</a>";										   
                                        	htm2+="</div>";
                                        	htm2+="</div>";
                    						htm2+="</td>";
                    						htm2+="<td class='col-sm-9'>";
                    						htm2+="<div class='row-fluid'>";
                    						htm2+="<div class='col-sm-offset-1 col-sm-11' style='margin-bottom:20px;'>"
                    					    htm2+="<span>发表于：</span><span>"+item.dateStr+"</span>&nbsp;&nbsp;&nbsp;&nbsp;"
                    					    htm2+="<a href='javascript:void();' onclick='reply(\""+item.user.userName+"\" ,\""+item.user.id+"\" ,\""+item.id+"\");'>回复</a>";
                    					    htm2+="</div>";										   
                    						htm2+="</div>";
                    						htm2+="<div class='row-fluid'>";
                    						htm2+=item.content
                    						htm2+="</div>";
                    						htm2+="<div class='row-fluid'>";
                    						htm2+="<div class='col-sm-offset-1 col-sm-11' style='margin-top:20px;'>";
                    						for(var i=0;i<item.lists.length;i++){
                    							htm2+="<div class='row-fluid'>";
                    							htm2+="<a>"+item.lists[i].user.userName+"</a>回复<a>"+item.lists[i].tuser.userName+"</a>："+item.lists[i].content;
                    							htm2+="</div><br/>";
                    						}
                    						htm2+="</div>";
                    						htm2+="</div>";
                    						htm2+="</td>";
                    					    htm2+="</tr>";  
                                        });
                                        $("#list").html(htm2);
                                    }
                                }
                            });
                        }
                    }
                    $('#pageshow').bootstrapPaginator(options);
                }
            }
        });
        //提交按钮事件绑定
        $("#submitbtn").on('click',function(event){
        	//先判断用户是否已登录
     		var arrstr = document.cookie.split("; ");
     		var islogin=false;
     		for(var i = 0;i < arrstr.length;i ++){
     		  var temp = arrstr[i].split("=");
     		  if(temp[0] == 'forum_user'){
     			 islogin=true;
     			 break;
     		  }
     		}
     		if(!islogin){
     			login(document.URL);
     			return false;
     		}
        	//已登录，提交数据
     		var content=UE.getEditor('newsEditor').getContent();
        	if(content.length<10){
   	           BootstrapDialog.alert({
 	               title: '提醒',
 	               message: '输入长度不能小于10!',
 	               type: BootstrapDialog.TYPE_WARNING, 
 	               closable: true, 
 	               draggable: true,
 	               buttonLabel: '确定', 
 	           });
   	           return false;
         	}
        	if(content==null||content==''){
        		BootstrapDialog.alert({
 	               title: '提醒',
 	               message: '请填写内容!',
 	               type: BootstrapDialog.TYPE_WARNING, 
 	               closable: true, 
 	               draggable: true,
 	               buttonLabel: '确定', 
 	           });	
        	   return false;
        	}
        	if(content.length<10){
   	           BootstrapDialog.alert({
 	               title: '提醒',
 	               message: '输入长度不能小于10!',
 	               type: BootstrapDialog.TYPE_WARNING, 
 	               closable: true, 
 	               draggable: true,
 	               buttonLabel: '确定', 
 	           });
   	           return false;
         	}
            $.ajax({
                url: "postComment.do",
                datatype: 'json',
                type: "Post",
                data: {
                	"postId":$("#postid").val(),
                	"content":UE.getEditor('newsEditor').getContent()
                },
                success: function (data) {
                	if(data!=null){
                		var result=eval("(" + data + ")").result;
                		if(result==1){
                			window.location.reload();
                		}else{
                	           BootstrapDialog.alert({
                	               title: '提醒',
                	               message: '提交错误!',
                	               type: BootstrapDialog.TYPE_WARNING, 
                	               closable: true, 
                	               draggable: true,
                	               buttonLabel: '确定', 
                	           });	        			
                		}
                	}
                },
            }); 
        	
        	return false;
        });
    });   
    
  	$(document).ready(function() {
		var slider = new bootslider('#bootslider', {
			animationIn: "fadeInUp",
			animationOut: "flipOutX",
			timeout: 5000,
			autoplay: true,
			preload: true,
			pauseOnHover: false,
			thumbnails: true,
			pagination: true,
			mousewheel: false,
			keyboard: true,
			touchscreen: true
		});
		slider.init();
	});
  	$(document).ready(function() {
        var editor=new baidu.editor.ui.Editor();
        editor.render("newsEditor");  
  	});

//帖子回复
function reply(username,tuserid,commentid){
	//先判断用户是否已登录
		var arrstr = document.cookie.split("; ");
		for(var i = 0;i < arrstr.length;i ++){
		  var temp = arrstr[i].split("=");
		  var islogin=false;
		  if(temp[0] == 'forum_user'){
			 islogin=true;
			 break;
		  }
		}
		if(!islogin){
			login(document.URL);
			return false;
		}
    BootstrapDialog.show({
    	title: '帖子回复',
    	draggable: true,
        message: "你回复"+username+":<textarea class='form-control rows='4'></textarea>",
        buttons: [{
            label: '提交',
            cssClass: 'btn-primary',
            action: function(dialogRef) {
            	var content = dialogRef.getModalBody().find('textarea').val();
            	if(content==null||content==''){
            		BootstrapDialog.alert({
     	               title: '提醒',
     	               message: '请填写内容!',
     	               type: BootstrapDialog.TYPE_WARNING, 
     	               closable: true, 
     	               draggable: true,
     	               buttonLabel: '确定', 
     	           });	
            	   return false;
            	}
                $.ajax({
                    url: "replyJson.do",
                    datatype: 'json',
                    type: "Post",
                    data: {
                    	"tuserid":tuserid,
                    	"commentid":commentid,
                    	"content":content
                    },
                    success: function (data) {
                    	if(data!=null){
                    		var result=eval("(" + data + ")").result;
                    		if(result==1){
                    			dialogRef.close();
                    			window.location.reload();
                    		}else{
                    	           BootstrapDialog.alert({
                    	               title: '提醒',
                    	               message: '提交错误!',
                    	               type: BootstrapDialog.TYPE_WARNING, 
                    	               closable: true, 
                    	               draggable: true,
                    	               buttonLabel: '确定', 
                    	           });	        			
                    		}
                    	}
                    },
                }); 
                return false;
            }
        }]
    });
}
//搜索
function search(){
		var target=$("#search").val();
  		window.location.href="luceneSeach.do?target="+target;
}