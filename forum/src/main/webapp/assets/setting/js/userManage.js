﻿$(function () {
    //发送消息窗口
    dlg_Edit = $('#Dlg-Edit').dialog({
        closed: true,
        modal: true,
        toolbar: [{
            text: '保存',
            iconCls: 'icon-save',
            handler: sendMessage
        }, '-', {
            text: '关闭',
            iconCls: 'icon-no',
            handler: function () {
                dlg_Edit.dialog('close');
            }
        }]
    });
    dlg_Edit_form = dlg_Edit.find('form');
    
    $('#btn-search,#btn-search-cancel').linkbutton();
    searchWin = $('#search-window').window({
        closed: true,
        modal: true
    });
    searchForm = searchWin.find('form');

    grid = $('#grid').datagrid({
        title: '用户管理',
        iconCls: 'icon-save',
        methord: 'post',
        url: $("#ctx").val()+"/setting/userManageJson.do",
        sortName: 'id',
        sortOrder: 'desc',
        idField: 'id',
        pageSize: 30,
        frozenColumns: [[
	                { field: 'ck', checkbox: true },
	                { title: 'id', field: 'id', width: 80, hidden: true}
				]],
        columns: [[
					{ field: 'userName', title: '用户名', width: 100,sortable: true },
					{ field: 'sex', title: '性别', width: 50,formatter:sexformatter,sortable: true },
					{ field: 'role', title: '角色', width: 50,formatter:roleformatter,sortable: true },
					{ field: 'isfrozen', title: '状态', width:50,formatter:isfrozenformatter,sortable: true },
					{ field: 'email', title: '邮箱', width:150,sortable: true },
					{ field: 'userId', title: '学号/教师号', width: 100 ,sortable: true },
					{ field: 'academyName', title: '学院', width: 150 ,sortable: true},
					{ field: 'phoneNumber', title: '电话号码', width: 150 ,sortable: true},
					{ field: 'birthday', title: '出生日期', width: 100, sortable: true },
					{ field: 'createDateStr', title: '注册日期', width: 100,sortable: true},
					{ field: 'nativePlace', title: '籍贯', width: 200 },
					{ field: 'abode', title: '现居地', width: 200 }
				]],
        fit:true,
        pagination: true,
        rownumbers: true,
        fitColumns: true,
        singleSelect: false,
        toolbar: [{
            text: '冻结',
            iconCls: 'icon-edit',
            handler: frozen
        }, '-', {
            text: '解冻',
            iconCls: 'icon-edit',
            handler: thaw
        }, '-', {
            text: '发送消息',
            iconCls: 'icon-edit',
            handler: openMessage
        }, '-', {
            text: '查找',
            iconCls: 'icon-search',
            handler: OpensearchWin
        }, '-', {
            text: '所有',
            iconCls: 'icon-search',
            handler: showAll
        }],

    });
    $('body').layout();
});
function roleformatter(value,row,index){
	if(value==0){ 
		return "学生";
	}else{
		return "教师";
	}
}
function sexformatter(value,row,index){
	if(value==0){ 
		return "男";
	}else{
		return "女";
	}
}
function isfrozenformatter(value,row,index){
	if(value==0){ 
		return "未冻结";
	}else{
		return "已冻结";
	}	
}
var grid;
var dlg_Edit;//发送消息窗口
var dlg_Edit_form;
var searchWin;//查找窗口
var searchForm;


//已选择id
function getSelectedArr() {
    var ids = [];
    var rows = grid.datagrid('getSelections');
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].id);
    }
    return ids;
}
function getSelectedID() {
    var ids = getSelectedArr();
    return ids.join(',');
}
function arr2str(arr) {
    return arr.join(',');
}

//删除
function del() {
    var arr = getSelectedArr();
    if (arr.length > 0) {
        $.messager.confirm('提示信息', '您确认要删除吗?', function (data) {
            if (data) {
                $.ajax({
                    url: $("#ctx").val()+'/setting/delUser.do',
                    type: 'post',
                    data: {
                    	"id":arr2str(arr)
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "删除出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '删除失败!', 'error');
                    }
                });
            }
        });
    } else {
        Msgshow('请先选择要删除的记录。');
    }
}

function Msgshow(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        showType: 'show'
    });
}
function Msgslide(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'slide'
    });
}
function Msgfade(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'fade'
    });
}

function OpensearchWin() {
    searchWin.window('open');
    searchForm.form('clear');
}

//展示所有的数据
function showAll() {
	grid.datagrid({ url: $("#ctx").val()+"/setting/userManageJson.do", 
		queryParams: { name: "",academyId:"",role:"",email:"",isfrozen:"",starTime:"",endTime:""} });
}
//查找
function SearchOK() {
    var s_name = $("#s_name").val();
    var s_academy =$('#s_academy').combobox('getValue');
    var s_role =$('#s_role').combobox('getValue');
    var s_email = $("#s_email").val();
    var s_isfrozen =$('#s_isfrozen').combobox('getValue');
    var s_starTime = $("#s_starTime").val();
    var s_endTime = $("#s_endTime").val();
    searchWin.window('close');
    grid.datagrid({ url: $("#ctx").val()+"/setting/userManageJson.do", 
    	queryParams: { name: s_name,academyId:s_academy,role:s_role,email:s_email,isfrozen:s_isfrozen,starTime:s_starTime,endTime:s_endTime} });
}
//关闭查找窗口
function closeSearchWindow() {
    searchWin.window('close');
}
 function frozen(){
	    var rows = grid.datagrid('getSelections');
	    
	    var num = rows.length;
	    if (num == 0) {
	        Msgslide('请选择一条记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
	        return;
	    }
	    else if (num > 1) {
	        Msgfade('您选择了多条记录,只能选择一条记录进行修改!'); //$.messager.alert('提示', '您选择了多条记录,只能选择一条记录进行修改!', 'info');
	        return;
	    }
	    else {
	    	if(rows[0].isfrozen!=0){
	    		Msgfade('该用户已为冻结状态了!');
	    	}else{
	    		$.ajax({
                    url: $("#ctx").val()+'/setting/frozenUser.do',
                    type: 'post',
                    data: {
                    	"id":rows[0].id
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "操作出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '失败失败!', 'error');
                    }
                });
	    	}
	    }	 
 }
 function thaw(){
	    var rows = grid.datagrid('getSelections');
	    
	    var num = rows.length;
	    if (num == 0) {
	        Msgslide('请选择一条记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
	        return;
	    }
	    else if (num > 1) {
	        Msgfade('您选择了多条记录,只能选择一条记录进行修改!'); //$.messager.alert('提示', '您选择了多条记录,只能选择一条记录进行修改!', 'info');
	        return;
	    }
	    else {
	    	if(rows[0].isfrozen!=1){
	    		Msgfade('该用户已为未冻结状态了!');
	    	}else{
	    		$.ajax({
                    url: $("#ctx").val()+'/setting/thawUser.do',
                    type: 'post',
                    data: {
                    	"id":rows[0].id
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "操作出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '操作失败!', 'error');
                    }
                });
	    	}
	    }		 
 }
 function openMessage() {
	    var rows = grid.datagrid('getSelections');
	    var num = rows.length;
	    if (num == 0) {
	        Msgslide('请选择记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
	        return;
	    }
	    else {
	        dlg_Edit.dialog('open');
	        dlg_Edit_form.form('clear');
	    }
	}
 function sendMessage() {
	 var arr = getSelectedArr();
	 $("#id").val(arr2str(arr));
	 dlg_Edit_form.form('submit', {
		 url: $("#ctx").val()+"/setting/sendMessage.do",
		 onSubmit: function () {
			 return $(this).form('validate');
		 },
		 success: function (data) {
			 var result=eval("(" + data + ")").result;
			 if (result==1) {
				 $.messager.alert('错误', "提交成功！", 'info');
				 dlg_Edit.dialog('close');
			 } else {
				 $.messager.alert('错误', "提交出错！", 'error');
			 }
		 }
	 });
	}