﻿$(function () {
    InitLeftMenu();
    $('body').layout();
    
    $.ajax({
        url: $("#ctx").val()+'/setting/getAdminName.do',
        type: 'post',
        data: {},
        success: function (dat) {
            var result=eval("(" + dat + ")").result;
            if (null!=result) {
            	$("#adminName").html(result);
            } else {
                $.messager.alert('错误', "操作出错！", 'error');
            }
        },
        error: function () {
            $.messager.alert('错误', '操作失败!', 'error');
        }
    });
})

function InitLeftMenu() {
    $('.easyui-accordion li a').click(function () {
        var tabTitle = $(this).text();
        var url = $(this).attr("href");
        addTab(tabTitle, url);
        $('.easyui-accordion li div').removeClass("selected");
        $(this).parent().addClass("selected");
    }).hover(function () {
        $(this).parent().addClass("hover");
    }, function () {
        $(this).parent().removeClass("hover");
    });
}

function addTab(subtitle, url) {
    if (!$('#tabs').tabs('exists', subtitle)) {
        $('#tabs').tabs('add', {
            title: subtitle,
            content: createFrame(url),
            closable: true,
            width: $('#mainPanle').width() - 10,
            height: $('#mainPanle').height() - 26
        });
    } else {
        $('#tabs').tabs('select', subtitle);
    }
}

function createFrame(url) {
    var s = '<iframe name="mainFrame" scrolling="no" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';
    return s;
}