﻿$(function () {
    //编辑窗口
    dlg_Edit = $('#Dlg-Edit').dialog({
        closed: true,
        modal: true,
        toolbar: [{
            text: '保存',
            iconCls: 'icon-save',
            handler: saveData
        }, '-', {
            text: '关闭',
            iconCls: 'icon-no',
            handler: function () {
                dlg_Edit.dialog('close');
            }
        }]
    });
    dlg_Edit_form = dlg_Edit.find('form');

    $('#btn-search,#btn-search-cancel').linkbutton();
    searchWin = $('#search-window').window({
        closed: true,
        modal: true
    });
    searchForm = searchWin.find('form');
   
    grid = $('#grid').datagrid({
        title: '主题管理',
        iconCls: 'icon-save',
        methord: 'get',
        url: $("#ctx").val()+"/setting/themeManageJson.do",
        sortName: 'id',
        sortOrder: 'desc',
        idField: 'id',
        pageSize: 30,
        frozenColumns: [[
	                { field: 'ck', checkbox: true },
	                { title: 'id', field: 'id', width: 80, hidden: true}
				]],
        columns: [[
					{ field: 'themeName', title: '名称', width: 150 },
					{ field: 'postNumber', title: '帖子数量', width: 80, sortable: true },
					{ field: 'introduuction', title: '介绍', width: 200 }
				]],
        fit:true,
        pagination: true,
        rownumbers: true,
        fitColumns: true,
        singleSelect: false,
        toolbar: [{
            text: '新增',
            iconCls: 'icon-add',
            handler: add
        }, '-', {
            text: '修改',
            iconCls: 'icon-edit',
            handler: edit
        }, '-', {
            text: '删除',
            iconCls: 'icon-remove',
            handler: del
        }, '-', {
            text: '查找',
            iconCls: 'icon-search',
            handler: OpensearchWin
        }, '-', {
            text: '所有',
            iconCls: 'icon-search',
            handler: showAll
        }],

    });
    $('body').layout();
});

function createColumnMenu() {
    var tmenu = $('<div id="tmenu" style="width:100px;"></div>').appendTo('body');
    var fields = grid.datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        $('<div iconCls="icon-ok"/>').html(fields[i]).appendTo(tmenu);
    }
    tmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                grid.datagrid('hideColumn', item.text);
                tmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                grid.datagrid('showColumn', item.text);
                tmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
}

var grid;
var dlg_Edit;//编辑窗口
var dlg_Edit_form;
var searchWin;//查找窗口
var searchForm;


//已选择id
function getSelectedArr() {
    var ids = [];
    var rows = grid.datagrid('getSelections');
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].id);
    }
    return ids;
}
function getSelectedID() {
    var ids = getSelectedArr();
    return ids.join(',');
}
function arr2str(arr) {
    return arr.join(',');
}

function add() {
    dlg_Edit.dialog('open');
    dlg_Edit_form.form('clear');
    
}
function edit() {
    var rows = grid.datagrid('getSelections');
    var num = rows.length;
    if (num == 0) {
        Msgslide('请选择一条记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
        return;
    }
    else if (num > 1) {
        Msgfade('您选择了多条记录,只能选择一条记录进行修改!'); //$.messager.alert('提示', '您选择了多条记录,只能选择一条记录进行修改!', 'info');
        return;
    }
    else {
        dlg_Edit.dialog('open');
        dlg_Edit_form.form('load', $("#ctx").val()+'/setting/getThemeById.do?id=' + rows[0].id);
    }
}
//删除
function del() {
    var arr = getSelectedArr();
    if (arr.length > 0) {
        $.messager.confirm('提示信息', '您确认要删除吗?', function (data) {
            if (data) {
                $.ajax({
                    url: $("#ctx").val()+'/setting/delTheme.do',
                    type: 'post',
                    data: {
                    	"id":arr2str(arr)
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "删除出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '删除失败!', 'error');
                    }
                });
            }
        });
    } else {
        Msgshow('请先选择要删除的记录。');
    }
}

function Msgshow(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        showType: 'show'
    });
}
function Msgslide(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'slide'
    });
}
function Msgfade(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'fade'
    });
}

function OpensearchWin() {
    searchWin.window('open');
    searchForm.form('clear');
}
//
function saveData() {
    dlg_Edit_form.form('submit', {
        url: $("#ctx").val()+"/setting/saveOrUpdateTheme.do",
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (data) {
        	 var result=eval("(" + data + ")").result;
            if (result==1) {
                grid.datagrid('reload');
                dlg_Edit.dialog('close');
            } else {
                $.messager.alert('错误', "提交出错！", 'error');
            }
        }
    });
}
//展示所有的数据
function showAll() {
	grid.datagrid({ url: $("#ctx").val()+"/setting/themeManageJson.do", queryParams: { name: ""} });
}
//查找，按主题名字查找
function SearchOK() {
    var s_title = $("#s_title").val();
    searchWin.window('close');
    grid.datagrid({ url: $("#ctx").val()+"/setting/themeManageJson.do", queryParams: { name: s_title} });
}
//关闭查找窗口
function closeSearchWindow() {
    searchWin.window('close');
}
 