﻿$(function () {

    $('#btn-search,#btn-search-cancel').linkbutton();
    searchWin = $('#search-window').window({
        closed: true,
        modal: true
    });
    searchForm = searchWin.find('form');


    grid = $('#grid').datagrid({
        title: '热帖管理',
        iconCls: 'icon-save',
        methord: 'get',
        url: $("#ctx").val()+"/setting/hotpostManageJson.do",
        sortName: 'id',
        sortOrder: 'desc',
        idField: 'id',
        pageSize: 30,
        frozenColumns: [[
	                { field: 'ck', checkbox: true },
	                { title: 'id', field: 'id', width: 80, hidden: true}
				]],
        columns: [[
					{ field: 'title', title: '名称', width: 100 },
					{ field: 'type', title: '类型', width: 100,formatter:typeformatter,sortable: true },
					{ field: 'data', title: '创建时间', width: 150 ,formatter:dataformatter, sortable: true},
					{ field: 'dataStr', title:'', width: 100, hidden: true},
					{ field: 'imagePath', title: '路径', width: 200 }
				]],
        fit:true,
        pagination: true,
        rownumbers: true,
        fitColumns: true,
        singleSelect: false,
        toolbar: [{
            text: '删除',
            iconCls: 'icon-remove',
            handler: del
        }, '-', {
            text: '查找',
            iconCls: 'icon-search',
            handler: OpensearchWin
        }, '-', {
            text: '所有',
            iconCls: 'icon-search',
            handler: showAll
        }],

    });
    $('body').layout();
});
function typeformatter(value,row,index){
	return value+1;
}
function dataformatter(value,row,index){
	return row.dataStr;
}
function createColumnMenu() {
    var tmenu = $('<div id="tmenu" style="width:100px;"></div>').appendTo('body');
    var fields = grid.datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        $('<div iconCls="icon-ok"/>').html(fields[i]).appendTo(tmenu);
    }
    tmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                grid.datagrid('hideColumn', item.text);
                tmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                grid.datagrid('showColumn', item.text);
                tmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
}

var grid;
var searchWin;//查找窗口
var searchForm;


//已选择id
function getSelectedArr() {
    var ids = [];
    var rows = grid.datagrid('getSelections');
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].id);
    }
    return ids;
}
function getSelectedID() {
    var ids = getSelectedArr();
    return ids.join(',');
}
function arr2str(arr) {
    return arr.join(',');
}


//删除
function del() {
    var arr = getSelectedArr();
    if (arr.length > 0) {
        $.messager.confirm('提示信息', '您确认要删除吗?', function (data) {
            if (data) {
                $.ajax({
                    url: $("#ctx").val()+'/setting/delHotpost.do',
                    type: 'post',
                    data: {
                    	"id":arr2str(arr)
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "删除出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '删除失败!', 'error');
                    }
                });
            }
        });
    } else {
        Msgshow('请先选择要删除的记录。');
    }
}

function Msgshow(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        showType: 'show'
    });
}
function Msgslide(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'slide'
    });
}
function Msgfade(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'fade'
    });
}

function OpensearchWin() {
    searchWin.window('open');
    searchForm.form('clear');
}

//展示所有的数据
function showAll() {
	grid.datagrid({ url: $("#ctx").val()+"/setting/hotpostManageJson.do", queryParams: { title: ""}});
}
//查找，按主题名字查找
function SearchOK() {
    var s_title = $("#s_title").val();
    searchWin.window('close');
    grid.datagrid({ url: $("#ctx").val()+"/setting/hotpostManageJson.do", queryParams: { title: s_title} });
}
//关闭查找窗口
function closeSearchWindow() {
    searchWin.window('close');
}
 