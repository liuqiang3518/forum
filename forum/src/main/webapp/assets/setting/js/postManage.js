﻿$(function () {
    //图片上传窗口
    dlg_Edit = $('#Dlg-Edit').dialog({
        closed: true,
        modal: true,

    });

	
    $('#btn-search,#btn-search-cancel').linkbutton();
    searchWin = $('#search-window').window({
        closed: true,
        modal: true
    });
    searchForm = searchWin.find('form');

    grid = $('#grid').datagrid({
        title: '帖子管理',
        iconCls: 'icon-save',
        methord: 'post',
        url: $("#ctx").val()+"/setting/postManageJson.do",
        sortName: 'id',
        sortOrder: 'desc',
        idField: 'id',
        pageSize: 30,
        frozenColumns: [[
	                { field: 'ck', checkbox: true },
	                { title: 'id', field: 'id', width: 80, hidden: true}
				]],
        columns: [[
					{ field: 'name', title: '帖子名称', width: 200,sortable: true },
					{ field: 'userName', title: '发帖人', width: 80,sortable: true },
					{ field: 'pageView', title: '浏览数', width: 80,sortable: true },
					{ field: 'replyNumber', title: '评论数', width:80,sortable: true },
					{ field: 'isfrozen', title: '状态', width:50,formatter:isfrozenformatter,sortable: true },
					{ field: 'postDate', title: '发布日期', width:150,formatter:postDateformatter,sortable: true },
					{ field: 'updateDate', title: '更新日期', width: 100 ,formatter:updateDateformatter,sortable: true },
					{ field: 'postDateStr', title: '', width:50, hidden: true},
					{ field: 'updateDateStr', title: '', width:50,hidden: true}
				]],
        fit:true,
        pagination: true,
        rownumbers: true,
        fitColumns: true,
        singleSelect: false,
        toolbar: [{
            text: '冻结',
            iconCls: 'icon-edit',
            handler: frozen
        }, '-', {
            text: '解冻',
            iconCls: 'icon-edit',
            handler: thaw
        }, '-', {
            text: '设置热帖1',
            iconCls: 'icon-edit',
            handler: addHotPost_1
        }, '-', {
            text: '设置热帖2',
            iconCls: 'icon-edit',
            handler: adHotPost_2
        }, '-', {
            text: '查找',
            iconCls: 'icon-search',
            handler: OpensearchWin
        }, '-', {
            text: '所有',
            iconCls: 'icon-search',
            handler: showAll
        }],

    });
    $('body').layout();
});
function postDateformatter(value,row,index){
	return row.postDateStr;
}
function updateDateformatter(value,row,index){
	return row.updateDateStr;
}
function isfrozenformatter(value,row,index){
	if(value==0){ 
		return "未冻结";
	}else{
		return "已冻结";
	}	
}
var grid;
var dlg_Edit;//编辑窗口
var dlg_Edit_form;
var searchWin;//查找窗口
var searchForm;


//已选择id
function getSelectedArr() {
    var ids = [];
    var rows = grid.datagrid('getSelections');
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].id);
    }
    return ids;
}
function getSelectedID() {
    var ids = getSelectedArr();
    return ids.join(',');
}
function arr2str(arr) {
    return arr.join(',');
}

//删除
function del() {
    var arr = getSelectedArr();
    if (arr.length > 0) {
        $.messager.confirm('提示信息', '您确认要删除吗?', function (data) {
            if (data) {
                $.ajax({
                    url: $("#ctx").val()+'/setting/delUser.do',
                    type: 'post',
                    data: {
                    	"id":arr2str(arr)
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "删除出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '删除失败!', 'error');
                    }
                });
            }
        });
    } else {
        Msgshow('请先选择要删除的记录。');
    }
}

function Msgshow(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        showType: 'show'
    });
}
function Msgslide(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'slide'
    });
}
function Msgfade(msg) {
    $.messager.show({
        title: '提示',
        msg: msg,
        timeout: 3000,
        showType: 'fade'
    });
}

function OpensearchWin() {
    searchWin.window('open');
    searchForm.form('clear');
}

//展示所有的数据
function showAll() {
	grid.datagrid({ url: $("#ctx").val()+"/setting/postManageJson.do", 
		queryParams: { title: "",userName:"",starTime:"",endTime:""} });
}
//查找
function SearchOK() {
    var s_title = $("#s_title").val();
    var s_userName = $("#s_userName").val();
    var s_starTime = $("#s_starTime").val();
    var s_endTime = $("#s_endTime").val();
    searchWin.window('close');
    grid.datagrid({ url: $("#ctx").val()+"/setting/postManageJson.do", 
    	queryParams: { title: s_title,userName:s_userName,starTime:s_starTime,endTime:s_endTime} });
}
//关闭查找窗口
function closeSearchWindow() {
    searchWin.window('close');
}
 function frozen(){
	    var rows = grid.datagrid('getSelections');
	    
	    var num = rows.length;
	    if (num == 0) {
	        Msgslide('请选择一条记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
	        return;
	    }
	    else if (num > 1) {
	        Msgfade('您选择了多条记录,只能选择一条记录进行修改!'); //$.messager.alert('提示', '您选择了多条记录,只能选择一条记录进行修改!', 'info');
	        return;
	    }
	    else {
	    	if(rows[0].isfrozen!=0){
	    		Msgfade('该用户已为冻结状态了!');
	    	}else{
	    		$.ajax({
                    url: $("#ctx").val()+'/setting/frozenPost.do',
                    type: 'post',
                    data: {
                    	"id":rows[0].id
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "操作出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '失败失败!', 'error');
                    }
                });
	    	}
	    }	 
 }
 function thaw(){
	    var rows = grid.datagrid('getSelections');
	    
	    var num = rows.length;
	    if (num == 0) {
	        Msgslide('请选择一条记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
	        return;
	    }
	    else if (num > 1) {
	        Msgfade('您选择了多条记录,只能选择一条记录进行修改!'); //$.messager.alert('提示', '您选择了多条记录,只能选择一条记录进行修改!', 'info');
	        return;
	    }
	    else {
	    	if(rows[0].isfrozen!=1){
	    		Msgfade('该用户已为未冻结状态了!');
	    	}else{
	    		$.ajax({
                    url: $("#ctx").val()+'/setting/thawPost.do',
                    type: 'post',
                    data: {
                    	"id":rows[0].id
                    },
                    success: function (dat) {
                        var result=eval("(" + dat + ")").result;
                        if (result==1) {
                            grid.datagrid('reload');
                            grid.datagrid('clearSelections');
                        } else {
                            $.messager.alert('错误', "操作出错！", 'error');
                        }
                    },
                    error: function () {
                        $.messager.alert('错误', '操作失败!', 'error');
                    }
                });
	    	}
	    }		 
 }
 function addHotPost_1() {
	    var rows = grid.datagrid('getSelections');
	    var num = rows.length;
	    if (num == 0) {
	        Msgslide('请选择一条记录进行操作!'); //$.messager.alert('提示', '请选择一条记录进行操作!', 'info');
	        return;
	    }
	    else if (num > 1) {
	        Msgfade('您选择了多条记录,只能选择一条记录进行修改!'); //$.messager.alert('提示', '您选择了多条记录,只能选择一条记录进行修改!', 'info');
	        return;
	    }
	    else {
	    	$("#d_id").val(rows[0].id);
	    	dlg_Edit.dialog('open');
	    }
	}
 function adHotPost_2(){
	    var arr = getSelectedArr();
	    if (arr.length > 0) {
	        $.messager.confirm('提示信息', '您确认要设置为热帖2吗?', function (data) {
	            if (data) {
	                $.ajax({
	                    url: $("#ctx").val()+'/setting/setHotPost_2.do',
	                    type: 'post',
	                    data: {
	                    	"id":arr2str(arr)
	                    },
	                    success: function (dat) {
	                        var result=eval("(" + dat + ")").result;
	                        if (result==1) {
	                        	 $.messager.alert('错误', "操作成功！", 'info');
	                        	 grid.datagrid('clearSelections');
	                        } else {
	                            $.messager.alert('错误', "操作出错！", 'error');
	                        }
	                    },
	                    error: function () {
	                        $.messager.alert('错误', '操作失败!', 'error');
	                    }
	                });
	            }
	        });
	    } else {
	        Msgshow('请先选择要操作的记录。');
	    }
 }
 
 
 //加载图片上传控件  设置热帖1
 $(function(){	
	  var uploader = new plupload.Uploader({
			runtimes : 'html5,html4',
			browse_button : 'loadImg', // you can pass in id...
			url : $("#ctx").val()+"/setting/setHotPost_1.do",
			multi_selection: false,
			filters : {
				max_file_size : '10mb',
				mime_types: [
					{title : "Image files", extensions : "jpg,png"},
				]
			},
			init: {
				PostInit: function() {
				},
				FilesAdded: function(up, files) {
					uploader.settings.multipart_params= { 'id': $("#d_id").val() };
					uploader.start();
					return false;
				},
				Error: function(up, err) {
					$.messager.alert('错误', '操作失败!', 'error');
				}
			}
		});
	  uploader.bind('UploadComplete',function(up,files){ 
		  $.messager.alert('提醒', '操作成功!','info',function(){
			  dlg_Edit.dialog('close');
			  grid.datagrid('clearSelections');
		  });
	  });
	    uploader.init();
 });