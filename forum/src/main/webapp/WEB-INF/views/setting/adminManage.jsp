﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>板块管理</title>
    <%@ page  isELIgnored="false"%>  
    <c:set var="ctx" value="<%=request.getContextPath() %>"/>
    <script type="text/javascript" src="${ctx }/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx }/assets/setting/js/adminManage.js"></script>
    
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/default/easyui.css" >
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/icon.css" >	
</head>
<body class="easyui-layout" style="overflow-y: hidden" scroll="no">
<input id="ctx" value="${ctx }" type="hidden">

    <div region="center" style="width: 500px; height: 300px; padding: 1px;overflow-y: hidden">
        <div id="grid" >
        </div>
    </div>
    <div id="Dlg-Edit" title="编辑窗口" style="width: 500px; height: 400px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post"  >
            <input id="id" name="id" type="hidden" value="">
            <input id="createDate" name="createDate" type="hidden" value="">
            <table>
                <tr>
                    <td>姓名:</td>
                    <td>
                        <input id="name" name="name" class="easyui-validatebox" required="true" style="width: 200px;" />
                </tr>
                <tr>
                    <td>  密码：</td>
                    <td>
                        <input id="password" name="password" class="easyui-validatebox" required="true" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td>  等级：</td>
                    <td>
                        <select id="level" class="easyui-combobox" name="level" style="width:204px;">  
                           <option value="1" >1</option>
                           <option value="2" >2</option>
                        </select>
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
    <div id="search-window" title="查询窗口" style="width: 400px; height: 200px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post">
            <table>
                <tr>
                    <td>  用户名：</td>
                    <td>
                        <input name="s_name" id="s_name" style="width: 200px;" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="text-align: center; padding: 5px;">
            <a href="javascript:void(0)" onclick="SearchOK()" id="btn-search" icon="icon-ok">确定</a>
            <a href="javascript:void(0)" onclick="closeSearchWindow()" id="btn-search-cancel" icon="icon-cancel">
                取消</a>
        </div>
    </div>
</body>
</html>
