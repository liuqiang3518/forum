﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<!DOCTYPE html>
<html >
    <head>
        <title>登录</title>
        <%@ page  isELIgnored="false"%>  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <c:set var="ctx" value="<%=request.getContextPath() %>"/>
		
		<script type="text/javascript" src="${ctx }/js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="${ctx }/js/base64.js"></script>
		<script type="text/javascript" src="${ctx }/assets/setting/js/adminlogin.js" ></script>
        <!-- CSS -->
        <link rel="stylesheet" type="text/css"  href="${ctx }/assets/setting/css/reset.css" >
        <link rel="stylesheet" type="text/css"  href="${ctx }/assets/setting/css/style.css" >
        <link rel="stylesheet" type="text/css"  href="${ctx }/assets/setting/css/supersized.css" >

    </head>

    <body style="background-image:url(${ctx }/img/login3.jpg);">

        <div class="page-container">
            <h1>登录</h1>
            <form action="" method="post">
                <input type="text" name="username" id="username" class="username" placeholder="请输入您的用户名！">
                <input type="password" name="password" id="password" class="password" placeholder="请输入您的用户密码！">
                <button type="button" class="submit_button" onclick="login();">登录</button>
                <br><br>
                <div style="color: red;"><span id="errorMessage"></span></div>
            </form>
        </div>

    </body>

</html>

