<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>后台管理</title>
    <%@ page  isELIgnored="false"%>  
    <c:set var="ctx" value="<%=request.getContextPath()%>"/>
    <script type="text/javascript" src="${ctx }/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx }/assets/setting/js/Default.js"></script>

    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/default/easyui.css" >
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/icon.css" >	
    <style type="text/css">
        .easyui-accordion ul
        {
            list-style-type: none;
            margin: 0px;
            padding: 10px;
        }
        .easyui-accordion ul li
        {
            padding: 0px;
        }
        .easyui-accordion ul li a
        {
            line-height: 24px;
        }
        .easyui-accordion ul li div
        {
            margin: 2px 0px;
            padding-left: 10px;
            padding-top: 2px;
        }
        .easyui-accordion ul li div.hover
        {
            border: 1px dashed #99BBE8;
            background: #E0ECFF;
            cursor: pointer;
        }
        .easyui-accordion ul li div.hover a
        {
            color: #416AA3;
        }
        .easyui-accordion ul li div.selected
        {
            border: 1px solid #99BBE8;
            background: #E0ECFF;
            cursor: default;
        }
        .easyui-accordion ul li div.selected a
        {
            color: #416AA3;
            font-weight: bold;
        }
    </style>
</head>
<body class="easyui-layout" style="overflow-y: hidden" scroll="no">
<input id="ctx" value="${ctx }" type="hidden">
    <div region="north" split="true" style="overflow: hidden; height: 32px; background: #D2E0F2 repeat-x center 50%;
        line-height: 20px; color: #fff;">
        <span> 欢迎使用&nbsp;</span> <span id="adminName"></span> &nbsp;
        <a id="btn" href="${ctx }/setting/logout.do" class="easyui-linkbutton" >退出</a> 
    </div>
    <div region="south" style="height: 20px; background: #D2E0F2;">
        <div style="text-align: center; font-weight: bold">
                                     论坛后台管理
        </div>
    </div>
    <div region="west" split="true" title="导航菜单" style="width: 180px;overflow:hidden;" icon="icon-redo">
        <div id="menu" class="easyui-accordion" fit="true" border="false">
            <div title="系统管理" style="padding: 10px;" icon="icon-edit">
                <div title="模块管理">
                    <ul>
                        <li>
                            <div><a target="mainFrame" href="${ctx}/setting/themeManage.do">主题管理</a></div>
                        </li>
                        <li>
                            <div><a target="mainFrame" href="${ctx}/setting/plateManage.do">模块管理</a></div>
                        </li>
                        <li>
                            <div><a target="mainFrame" href="${ctx}/setting/postManage.do">帖子管理</a></div>
                        </li>
                        <li>
                            <div><a target="mainFrame" href="${ctx}/setting/hotpostManage.do">热帖管理</a></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div title="用户管理" style="padding: 10px;" icon="icon-edit">
                <div title="用户管理">
                    <ul>
                        <li>
                            <div><a target="mainFrame" href="${ctx}/setting/userManage.do">普通用户</a></div>
                        </li>
                        <c:if test="${level==1}">
                            <li>
                                <div><a target="mainFrame" href="${ctx}/setting/adminManage.do">管理员</a></div>
                            </li>
                        </c:if>
                    </ul>
                </div>
            </div>
            <div title="关于" icon="icon-help">
                <h4>
                    EntWebSite Ver 1.0</h4>
            </div>
        </div>
    </div>
    <div region="center" id="mainPanle" style="background: #eee;overflow:hidden;">
        <div id="tabs" class="easyui-tabs" fit="true" border="false">
            <div title="主页" style="padding: 20px;" id="home">
              <iframe name="mainFrame" scrolling="no" frameborder="0"  src="${ctx}/setting/getThemeStatistics.do" style="width:100%;height:100%;"></iframe>
            </div>
        </div>
    </div>
</body>
</html>
