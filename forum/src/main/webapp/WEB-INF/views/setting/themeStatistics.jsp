﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>主题统计</title>
    <%@ page  isELIgnored="false"%>  
    <c:set var="ctx" value="<%=request.getContextPath()%>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ECharts">
	
    <script type="text/javascript" src="${ctx }/js/echart/echarts.js"></script>
    <script type="text/javascript" src="${ctx }/js/echart/codemirror.js"></script>
    <script type="text/javascript" src="${ctx }/js/echart/javascript.js"></script>
    <script type="text/javascript" src="${ctx }/js/echart/echartsHome.js"></script>
    <script type="text/javascript" src="${ctx }/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap.min.js"></script>
        
    <link rel="shortcut icon" href="../asset/ico/favicon.png">

    <link rel="stylesheet" href="${ctx }/css/echart/css/font-awesome.min.css" >
    <link rel="stylesheet" href="${ctx }/css/echart/css/bootstrap.css" >
    <link rel="stylesheet" href="${ctx }/css/echart/css/carousel.css" >
    <link rel="stylesheet" href="${ctx }/css/echart/css/echartsHome.css" >
    <link rel="stylesheet" href="${ctx }/css/echart/css/codemirror.css" >
    <link rel="stylesheet" href="${ctx }/css/echart/css/monokai.css" >
</head>
<body  style="overflow-y: hidden" >
   <div class="navbar navbar-default navbar-fixed-top" role="navigation" id="head"></div>


    <div class="container-fluid">
        <div class="row-fluid example">
            <div id="sidebar-code"  style="display: none;">
                <div class="well sidebar-nav">
                    <div class="nav-header"><a href="#" onclick="autoResize()" class="glyphicon glyphicon-resize-full" id ="icon-resize" ></a>option</div>
                    <textarea id="code" name="code">
option = {
    title : {
        text: '当前主题下的帖子统计',
    },
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['帖子数量','帖子评论数量']
    },
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : ${category }
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'帖子数量',
            type:'bar',
            data:${postNum },
            markPoint : {
                data : [
                    {type : 'max', name: '最大值'},
                    {type : 'min', name: '最小值'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        },
        {
            name:'帖子评论数量',
            type:'bar',
            data:${repyNum },
            markPoint : {
                data : [
                    {type : 'max', name: '最大值'},
                    {type : 'min', name: '最小值'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name : '平均值'}
                ]
            }
        }
    ]
};
                    </textarea>
              </div><!--/.well -->
            </div><!--/span-->
            <div id="graphic" class="col-md-12">
                <div id="main" class="main"></div>
                <div>
                    <button type="button" class="btn btn-sm btn-success" onclick="refresh(true)">刷 新</button>
                    <span class="text-primary">切换主题</span>
                    <select id="theme-select"></select>

                    <span id='wrong-message' style="color:red"></span>
                </div>
            </div><!--/span-->
        </div><!--/row-->
        
        </div><!--/.fluid-container-->

    <footer id="footer"></footer>

    <script type="text/javascript" src="${ctx }/js/echart/echartsHome.js"></script>
    <script src="${ctx }/js/echart/echartsExample.js"></script>
</body>
</html>
