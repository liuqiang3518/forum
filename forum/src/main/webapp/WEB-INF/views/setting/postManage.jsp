﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>帖子管理</title>
    <%@ page  isELIgnored="false"%>  
    <c:set var="ctx" value="<%=request.getContextPath()%>"/>
    <script type="text/javascript" src="${ctx }/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx }/js/My97DatePicker/WdatePicker.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/setting/js/postManage.js"></script>
    <script type="text/javascript" src="${ctx }/js/plupload.full.min.js"></script>
    
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/default/easyui.css" >
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/icon.css" >	
</head>
<body class="easyui-layout" style="overflow-y: hidden" scroll="no">
<input id="ctx" value="${ctx }" type="hidden">

    <div region="center" style="width: 500px; height: 300px; padding: 1px;overflow-y: hidden">
        <div id="grid" >
        </div>
    </div>

    <div id="search-window" title="查询窗口" style="width: 500px; height: 350px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post">
            <table>
                <tr>
                    <td>帖子名称：</td>
                    <td>
                        <input name="s_title" id="s_title" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td>发布人：</td>
                    <td>
                        <input name="s_userName" id="s_userName" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td>注册时间从：</td>
                    <td>
                        <input type="text" name="s_starTime" id="s_starTime" readonly="readonly" style="width: 200px;"
                        onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'1900-1-1',maxDate:'2100-03-10'})" >
                    </td>
                </tr>
                <tr>
                    <td>到：</td>
                    <td>
                        <input type="text" name="s_endTime" id="s_endTime" readonly="readonly" style="width: 200px;"
                        onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'1900-1-1',maxDate:'2100-03-10'})" >
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="text-align: center; padding: 5px;">
            <a href="javascript:void(0)" onclick="SearchOK()" id="btn-search" icon="icon-ok">确定</a>
            <a href="javascript:void(0)" onclick="closeSearchWindow()" id="btn-search-cancel" icon="icon-cancel">
                取消</a>
        </div>
    </div>
    
        <div id="Dlg-Edit" title="编辑窗口" style="width: 400px; height: 300px;">
        <div style="padding: 50px 50px 50px 80px;">
            <form method="post"  >
            <input id="d_id" name="d_id" type="hidden" value="">
            <table>
                <tr>
                    <td>选择图片:</td>
                    <td>
                        <button id="loadImg" >选择上传图片并提交</button>
                </tr>
            </table>
            </form>
        </div>
    </div>
</body>
</html>
