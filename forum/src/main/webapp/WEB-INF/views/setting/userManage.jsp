﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>用户管理</title>
    <%@ page  isELIgnored="false"%>  
    <c:set var="ctx" value="<%=request.getContextPath()%>"/>
    <script type="text/javascript" src="${ctx }/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx }/js/My97DatePicker/WdatePicker.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/setting/js/userManage.js"></script>
    
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/default/easyui.css" >
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/icon.css" >	
</head>
<body class="easyui-layout" style="overflow-y: hidden" scroll="no">
<input id="ctx" value="${ctx }" type="hidden">

    <div region="center" style="width: 500px; height: 300px; padding: 1px;overflow-y: hidden">
        <div id="grid" >
        </div>
    </div>
    <div id="Dlg-Edit" title="编辑窗口" style="width: 500px; height: 400px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post"  >
            <input id="id" name="id" type="hidden" value="">
            <table>
                <tr>
                    <td>标题:</td>
                    <td>
                        <input id="title" name="title" class="easyui-validatebox" required="true" style="width: 200px;" />
                </tr>
                <tr>
                    <td> 内容：</td>
                    <td>
                        <textarea style="overflow:auto;width:250px;height:80px;" id="content" name="content" class="easyui-validatebox" required="true">
                        </textarea>
                </tr>
            </table>
            </form>
        </div>
    </div>
    <div id="search-window" title="查询窗口" style="width: 500px; height: 350px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post">
            <table>
                <tr>
                    <td>用户名：</td>
                    <td>
                        <input name="s_name" id="s_name" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td>邮箱：</td>
                    <td>
                        <input name="s_email" id="s_email" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td>角色：</td>
                    <td>
                        <select class="easyui-combobox" id="s_role" name="s_role"  style="width:204px;">
                           <option value="0">学生</option>
                           <option value="1">教师</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>状态：</td>
                    <td>
                        <select class="easyui-combobox" id="s_isfrozen" name="s_isfrozen"  style="width:204px;">
                           <option value="0">未冻结</option>
                           <option value="1">已冻结</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>  学院：</td>
                    <td>
                        <select class="easyui-combobox" id="s_academy" name="s_academy"  style="width:204px;">
                           <c:if test="${!empty academy}">    
                           <c:forEach items="${academy}" var="item" varStatus="status"> 
                              <option value="${item.id }">${item.academyName }</option>
                           </c:forEach>
                           </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>注册时间从：</td>
                    <td>
                        <input type="text" name="s_starTime" id="s_starTime" readonly="readonly" style="width: 200px;"
                        onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd',minDate:'1900-1-1',maxDate:'2100-03-10'})" >
                    </td>
                </tr>
                <tr>
                    <td>到：</td>
                    <td>
                        <input type="text" name="s_endTime" id="s_endTime" readonly="readonly" style="width: 200px;"
                        onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd',minDate:'1900-1-1',maxDate:'2100-03-10'})" >
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="text-align: center; padding: 5px;">
            <a href="javascript:void(0)" onclick="SearchOK()" id="btn-search" icon="icon-ok">确定</a>
            <a href="javascript:void(0)" onclick="closeSearchWindow()" id="btn-search-cancel" icon="icon-cancel">
                取消</a>
        </div>
    </div>
</body>
</html>
