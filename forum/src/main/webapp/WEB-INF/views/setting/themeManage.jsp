﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>主题管理</title>
    <%@ page  isELIgnored="false"%>  
    <c:set var="ctx" value="<%=request.getContextPath() %>"/>
    <script type="text/javascript" src="${ctx }/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/easyui/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx }/assets/setting/js/themeManage.js"></script>
    
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/default/easyui.css" >
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/easyui/themes/icon.css" >	
</head>
<body class="easyui-layout" style="overflow-y: hidden" scroll="no">
<input id="ctx" value="${ctx }" type="hidden">

    <div region="center" style="width: 500px; height: 300px; padding: 1px;overflow-y: hidden">
        <div id="grid" >
        </div>
    </div>
    <div id="Dlg-Edit" title="编辑窗口" style="width: 500px; height: 400px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post"  >
            <input id="themeId" name="themeId" type="hidden" value="">
            <table>
                <tr>
                    <td>
                                                                 名称:
                    </td>
                    <td>
                        <input name="name" name="name" class="easyui-validatebox" required="true" style="width: 200px;" />
                </tr>
                <tr>
                    <td>
                                                                学院：
                    </td>
                    <td>
                        <select class="easyui-combobox" name="academy"  style="width:204px;">
                           <option selected="selected" value="0">请选择主题分类</option>
                           <c:if test="${!empty academy}">    
                           <c:forEach items="${academy}" var="item" varStatus="status"> 
                              <option value="${item.id }">${item.academyName }</option>
                           </c:forEach>
                           </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                                                                背景色：
                    </td>
                    <td>
                        <select id="color" class="easyui-combobox" name="color" style="width:204px;">  
                           <option value="blue" selected="selected">blue</option>
                           <option value="red" >red</option>
                           <option value="green" >green</option>
                           <option value="brown" >brown</option>
                           <option value="lime" >lime</option>
                           <option value="teal" >teal</option>
                           <option value="purple" >purple</option>
                           <option value="pink" >pink</option>
                           <option value="magenta" >magenta</option>
                           <option value="grey" >grey</option>
                           <option value="darkblue" >darkblue</option>
                           <option value="lightred" >lightred</option>
                           <option value="lightgrey" >lightgrey</option>
                           <option value="satblue" >satblue</option>
                           <option value="satgreen" >satgreen</option>
                           <option value="darkblue" >darkblue</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                                                                图标：
                    </td>
                    <td>
                        <select id="icon" class="easyui-combobox" name="icon" style="width:204px;">  
                           <option value="icon-adjust" selected="selected">icon-adjust</option>
                           <option value="icon-asterisk" >icon-asterisk</option>
                           <option value="icon-bar-chart" >icon-bar-chart</option>
                           <option value="icon-barcode" >icon-barcode</option>
                           <option value="icon-beaker" >icon-beaker</option>
                           <option value="icon-beer" >icon-beer</option>
                           <option value="icon-bell" >icon-bell</option>
                           <option value="icon-bell-alt" >icon-bell-alt</option>
                           <option value="icon-book" >icon-book</option>
                           <option value="icon-bookmark" >icon-bookmark</option>
                           <option value="icon-bullhorn" >icon-bullhorn</option>
                           <option value="icon-certificate" >icon-certificate</option>
                           <option value="icon-cloud" >icon-cloud</option>
                           <option value="icon-globe" >icon-globe</option>
                           <option value="icon-picture" >icon-picture</option>
                           <option value="icon-star" >icon-star</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                                                                  介绍：                                          
                    </td>
                    <td>
                        <textarea style="overflow:auto;width:250px;height:80px;" id="introduction" name="introduction">
                        </textarea>
                </tr>
            </table>
            </form>
        </div>
    </div>
    <div id="search-window" title="查询窗口" style="width: 350px; height: 200px;">
        <div style="padding: 20px 20px 40px 80px;">
            <form method="post">
            <table>
                <tr>
                    <td>
                        名称：
                    </td>
                    <td>
                        <input name="s_title" id="s_title" style="width: 150px;" />
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="text-align: center; padding: 5px;">
            <a href="javascript:void(0)" onclick="SearchOK()" id="btn-search" icon="icon-ok">确定</a>
            <a href="javascript:void(0)" onclick="closeSearchWindow()" id="btn-search-cancel" icon="icon-cancel">
                取消</a>
        </div>
    </div>
</body>
</html>
