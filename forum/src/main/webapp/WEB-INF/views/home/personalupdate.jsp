<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
  <head>
  <%@ page  isELIgnored="false"%>  
  <%@ include file="/common/forum_meta.jsp"%>
  <script type="text/javascript" src="${ctx }/assets/js/personalupdate.js" ></script>
  <script type="text/javascript" src="${ctx }/js/plupload.full.min.js"></script>
   <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
  </head>
  <body>
        <br/><br/><br/>
        <div class="row">
      	    <div class="col-sm-offset-1 col-sm-3">
      	       <div class="row">
		           <img src="" id="p_img" alt="..." class="img-rounded pull-left" width="200px" height="200px">      	    	
      	       </div>
      	       <br/>
      	       <div class="row" id="photoupdatediv">
      	           <button id="photosubmit" class="btn btn-primary col-sm-4 " >上传头像</button>
      	       </div>
            </div>
			<div class="col-sm-8 ">
                <div class="row">
                    <form class="form-horizontal" id="updadeform">
                        <div class="form-group">
                            <label for="p_email" class=" col-sm-2 control-label"><span class="pull-right">邮箱</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_email"  placeholder="邮箱" check-type="required mail" minlength="6" maxlength="20" required-message="请输入正确邮箱">
                            </div>
                        </div>	
           	            <div class="form-group">
                            <label for="p_username" class=" col-sm-2 control-label"><span class="pull-right">用户名</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_username" placeholder="用户名" check-type="required " minlength="3" maxlength="20" required-message="请输入用户名">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="p_role" class=" col-sm-2 control-label"><span class="pull-right">角色</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_role" placeholder="角色" >
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="p_userid" class=" col-sm-2 control-label"><span class="pull-right" id="ps_userid">学号</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_userid"  placeholder="学号" check-type="required number" minlength="6" maxlength="20" required-message="请输入正确学号">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="p_academy" class=" col-sm-2 control-label"><span class="pull-right">学院</span></label>
                            <div class="col-sm-6">
                                <select class="form-control" id="p_academy" check-type="required" required-message="请选择所属学院">
                                       <option value="" selected="selected">请选择</option>
	                                   <c:if test="${!empty academys}">    
                                         <c:forEach items="${academys}" var="item"> 
                                           <option value="${item.id}">${item.academyName}</option> 
                                         </c:forEach>  
                                       </c:if>  
                                   </select>
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="p_sex" class=" col-sm-2 control-label"><span class="pull-right">性别</span></label>
                            <div class="col-sm-6">
                                <select class="form-control" id="p_sex" check-type="required" required-message="请选择性别">
                                   <option value="" selected="selected">请选择</option>
                                   <option value="0" selected="selected">男</option>
                                   <option value="1" selected="selected">女</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="p_birthday" class=" col-sm-2 control-label"><span class="pull-right">出生日期</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_birthday"  placeholder="出生日期" check-type="required "  readonly="readonly"
                                onfocus="WdatePicker({skin:'whyGreen',dateFmt:'yyyy-MM-dd',minDate:'1900-1-1',maxDate:'2100-03-10'})" required-message="请输入选择出生日期">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="p_phoneNumber" class=" col-sm-2 control-label"><span class="pull-right">手机号码</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_phoneNumber"  placeholder="手机号码" check-type="required " minlength="6" maxlength="20" required-message="请输入正确手机号码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="p_nativePlace" class=" col-sm-2 control-label"><span class="pull-right">籍贯</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_nativePlace"  placeholder="籍贯" check-type="required "  maxlength="30" required-message="请输入正确籍贯">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="p_abode" class=" col-sm-2 control-label"><span class="pull-right">现居地</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="p_abode" placeholder="现居地" check-type="required"  maxlength="20" required-message="请输入正确现居地">
                            </div>
                        </div>
                        <div class="form-group">
                             <button id="updatesubmit" class="btn btn-primary col-sm-offset-4 col-sm-2 " >提交</button>
                        </div>
                    </form>        
                </div>
            </div>
        </div>
</body>