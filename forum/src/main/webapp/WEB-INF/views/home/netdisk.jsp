<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 
<!DOCTYPE html>
<html>
  <head>
    <title>个人网盘</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>
	
    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-paginator.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/js/netdisk.js" ></script>
    <script type="text/javascript" src="${ctx }/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
    <script type="text/javascript" src="${ctx }/js/application.min.js" ></script>
    <script type="text/javascript" src="${ctx }/js/ztree/jquery.ztree.all-3.5.min.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
	<script type="text/javascript" src="${ctx }/js/demonstration.min.js"></script>
	<script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script>  
	
	<link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/bs-theme-metro.css" />
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/zTreeStyle/zTreeStyle.css" />
	
  </head>

  <body >
  <input id="level" value="${level}" type="hidden">
  <input id="parentId" value="${parentId}" type="hidden">
  <input id="fileType" value="${fileType}" type="hidden">
  <input id="ctx" value="${ctx }" type="hidden">
  <%@ include file="navigation.jsp"%>
<div class="container-fluid" id="content">
	<div id="left" class="ui-sortable ui-resizable">
			<div class="subnav">
			   <ul class="subnav-menu" style="display: block;">
                 <li class="active" id="subnav_li_-1">
		           <a href="javascript:navChoose(-1)"><i class="icon-hdd"></i>全部文件</a>
		         </li>
                 <li id="subnav_li_1">
		           <a href="javascript:navChoose(1)"><i class="icon-file"></i>文档</a>
		         </li>
                 <li id="subnav_li_2">
		           <a href="javascript:navChoose(2)"><i class="icon-music"></i>音乐</a>
		         </li>
                 <li id="subnav_li_3">
		            <a href="javascript:navChoose(3)"><i class="icon-film"></i>视频</a>
		         </li>
                 <li id="subnav_li_4">
		           <a href="javascript:navChoose(4)"><i class="icon-picture"></i>图片</a>
		         </li>
                 <li id="subnav_li_0">
		            <a href="javascript:navChoose(0)"><i class="icon-minus"></i>其他</a>
		         </li>
			   </ul>
			</div>
	</div>
	<div id="main">
	    <div class="row-fluid">
		    <div class="span12">
               <div class="box">
                  <div class="box-title">
					    <div class="row">
						   <div class="col-sm-4">
						      <i class="icon-th-list"></i>&nbsp;&nbsp;
                              <button id="uploadsubmit" class="btn btn-primary "><i class="icon-upload-alt"></i>&nbsp;上传文件</button>&nbsp;&nbsp;
			                  <button id="newfilesubmit" class="btn btn-primary " onclick="newFile();"><i class="icon-folder-close"></i>&nbsp;新建文件夹</button>
						   </div>
						   <div class="col-sm-4" style="display: block;">
						      <div class="row">
							  <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" role="progressbar" id="upprogress"
                                aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" >
                                   <span id="progressdescription"></span>
                                </div>
                              </div>
							  </div> 
						   </div>
						</div>
                  </div>
				  <div class="box-content">
				     <table class="table table-hover ">
					       <thead>
						       <tr role="row">
							        <td class="col-sm-8">
									     文件名
									</td>
									<td class="col-sm-2">
									    大小
									</td>
									<td class="col-sm-2">
									    修改时间
									</td>
							   </tr>
						   </thead>
						   <tbody id="list">
						   </tbody>
					 </table>
					 <ul id="pageshow"></ul>  
				  </div>				  
			   </div>
		    </div>
	    </div>
    </div>
</div>
 </body>
</html>