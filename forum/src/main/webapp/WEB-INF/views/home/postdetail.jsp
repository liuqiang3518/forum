<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>
  <head>
    <title>postDetail</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>
    
    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/js/postdetail.js" ></script>
	<script type="text/javascript" src="${ctx }/js/touchSwipe.js"></script>
    <script type="text/javascript" src="${ctx }/js/application.min.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-paginator.js" ></script>
    <script type="text/javascript" src="${ctx }/js/fitvids.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootslider.js"></script>
	<script type="text/javascript" src="${ctx }/js/jquery.hotkeys.js"></script>
	<script type="text/javascript" src="${ctx }/js/demonstration.min.js"></script>
	<script type="text/javascript" src="${ctx }/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="${ctx }/ueditor/ueditor.all.min.js"></script>	
	<script type="text/javascript" src="${ctx }/js/base64.js"></script> 
	<script type="text/javascript" src="${ctx }/common/loginDialog.js"></script> 
	<script type='text/javascript' src="${ctx }/dwr/engine.js"></script> 
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script> 
    <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
     		
    <link rel="stylesheet" type="text/css" href="${ctx }/assets/css/postlist.css">
    <link rel='stylesheet' type="text/css" href="${ctx }/css/jquery.cleditor.css" >
    <link rel="stylesheet" type="text/css" href="${ctx }/css/bootslider.css" />
    <link rel="stylesheet" type="text/css" href="${ctx }/css/bs-theme-metro.css" />
    <link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">
    
  </head>

  <body>
  <input type="hidden" id="postid" value="${post.id }">
<%@ include file="navigation.jsp"%>
<%@ include file="hotpost.jsp"%>
<%@ include file="search.jsp"%><br/>
  <div class="row">
      <div class="col-sm-offset-1 col-sm-10">
	     <div class="row-fluid">
		 	<ol class="breadcrumb">
               <li><a href="${ctx }/index.do">主页</a></li>
               <li><a href="${ctx }/plate.do?id=${post.plateId }">板块</a></li>
			   <li class="active">${post.name }</li>
            </ol>
		 </div>
		 <div class="row-fluid">
		    <div class="span8">
                <div class="box box-color  box-bordered green">
                   <div class="box-title">
                       <h3>
                          <i class="icon-th-list"></i>
                          ${post.name }
                       </h3>
                   </div>
				   <div class="box-content nopadding">
				       <table class="table table-bordered">
				           <thead>
				              <tr role='row'>
				                 <td class="col-sm-3" style="background: #f7f9fb;">
				                    <div style="min-height:100px;max-height:auto;text-align:center;">
				                       <div class="row-fluiid" >
				                           <img  src="${user.imagePath }" style="width:80px; height:80px;">
				                       </div>
				                       <div class="row-fluid">
				                          <a href=''>${user.userName }</a>
				                       </div>
				                    </div>
				                 </td>
				                 <td class='col-sm-9'>
				                    <div class='row-fluid'>
				                       <div class="col-sm-offset-1 col-sm-11" style="margin-bottom:20px;">
				                          <span>发表于：</span><span>${post.postDateStr }</span>
				                       </div>
				                    </div>
				                    <div class='row-fluid'>
				                       ${post.content }
				                    </div>
				                 </td>
				              </tr>
				           </thead>
						   <tbody id="list">
						   </tbody>
                       </table>
                       <ul id="pageshow"></ul>  
				   </div>
				</div>
			</div>		    
		 </div>
		 <div class="row-fluid">
		    <div class="span12">
               <div class="box">
                  <div class="box-title">
                      <h3>
                         <i class="icon-edit"></i>
                                                                            评论
                      </h3>
                  </div>
				  <div class="box-content">
				     <form class="form-horizontal" method="POST" action="#">
                        <div class="row">
						    <div class="form-group">
						     <textarea id="newsEditor"  name="content" style="height: 250px"></textarea>
							</div>
						</div>
		                <div class="form-group">
						    <div class=" col-sm-12">
							   <button id="submitbtn" class="btn btn-primary col-sm-2 pull-right">提交</button>
							</div>						
                        </div>   
					 </form>
				  </div>				  
			   </div>
		 </div>
	  </div>
	</div>
 </body>
</html>