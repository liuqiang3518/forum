<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 

<!DOCTYPE html>
<html>
  <head>
    <title>注册</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>

<script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="${ctx }/assets/js/reg.js" ></script>
    <script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script> 
    
    <link rel="stylesheet" type="text/css" href="${ctx }/assets/css/reg.css">
	<link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">
  </head>

  <body>
  <input type="hidden" id="ctx" value="${ctx }">
<%@ include file="navigation.jsp"%>
<div class="row">
    <div class="col-sm-offset-3  col-sm-6">
	    <div class="row-fluid">
            <div class="span6">
	            <div class="box box-color box-bordered ">
                   <div class="box-title">
                     <h3>
                       <i class="icon-user"></i>
                                                                     注册
                     </h3>
                   </div>
				   <div class="box-content" style="min-height:350px;max-height:auto;">
                       <form class="form-horizontal" id="regform" style="margin-top:30px;">
                          <div class="form-group">
                               <label for="name" class=" col-sm-2 control-label"><span class="pull-right">姓名</span></label>
                               <div class="col-sm-6">
                                    <input type="text" class="form-control" id="name"  placeholder="姓名" check-type="required" maxlength="20" required-message="请输入姓名">
                               </div>
                          </div>
	                      <div class="form-group">
                               <label for="userId" class=" col-sm-2 control-label"><span class="pull-right">学号</span></label>
                               <div class="col-sm-6">
                                    <input type="text" class="form-control" id="userId"  placeholder="学号" check-type="required number" minlength="6" maxlength="20" required-message="请输入正确学号">
                               </div>
                          </div>
                          <div class="form-group">
                               <label for="academy" class=" col-sm-2 control-label"><span class="pull-right">学院</span></label>
                               <div class="col-sm-6">
                                    <select class="form-control" id="academy" check-type="required" required-message="请选择所属学院">
                                       <option value="" selected="selected">请选择</option>
	                                   <c:if test="${!empty academys}">    
                                         <c:forEach items="${academys}" var="item"> 
                                           <option value="${item.id}">${item.academyName}</option> 
                                         </c:forEach>  
                                       </c:if>  
                                   </select>
                               </div>
                          </div>		  
                          <div class="form-group">
                               <label for="password" class="col-sm-2 control-label"><span class="pull-right">密码</span></label>
                               <div class="col-sm-6">
                                   <input type="password" class="form-control" id="password" placeholder="密码" check-type="required" minlength="6" maxlength="20" required-message="输入长度6到20位的密码">
                               </div>
                          </div>	
						  <div class="form-group">
                               <label for="repassword" class="col-sm-2 control-label"><span class="pull-right">确认密码</span></label>
                               <div class="col-sm-6">
                                   <input type="password" class="form-control" id="repassword" placeholder="确认密码" check-type="required" minlength="6" maxlength="20" required-message="输入长度6到20位的确认密码">
                               </div>
                          </div>
                          <div class="form-group">
                               <label for="email" class="col-sm-2 control-label"><span class="pull-right">邮箱</span></label>
                               <div class="col-sm-6">
                                   <input type="text" class="form-control" id="email" placeholder="邮箱" check-type="requiredb mail" minlength="6" maxlength="20" required-message="输入长度大于等于6">
                               </div>
                          </div>						  
		                  <div class="form-group">
						      <div class="col-sm-offset-2 col-sm-6">
							        <button type="btn" id="regbtn"class="btn btn-primary col-sm-4 pull-right">注册</button>
							  </div>						
                          </div>   
	                   </form>				      
				   </div>
				</div>
			</div>
		</div>
	</div>
</div>

 </body>
</html>