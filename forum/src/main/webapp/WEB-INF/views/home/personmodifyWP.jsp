<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
  <head>
  <%@ page  isELIgnored="false"%>  
  <%@ include file="/common/forum_meta.jsp"%>
   <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
   <script type="text/javascript" src="${ctx }/assets/js/personalmodifyWP.js"></script>
  </head>
  <body>
    
    	<br/><br/><br/>
        <div class="row">
		   <div class="col-sm-offset-3 col-sm-6">
		       <form class="form-horizontal" id="updadeform">
		          <div class="form-group">
                       <label for="password" class=" col-sm-2 control-label"><span class="pull-right">旧密码</span></label>
                        <div class="col-sm-6">
                             <input type="password" class="form-control" id="password"  placeholder="旧密码" check-type="required" minlength="6" maxlength="20" required-message="请输入旧密码">
                        </div>
                  </div>
                  <div class="form-group">
                       <label for="newpassword" class=" col-sm-2 control-label"><span class="pull-right">新密码</span></label>
                        <div class="col-sm-6">
                             <input type="password" class="form-control" id="newpassword"  placeholder="新密码" check-type="required " minlength="6" maxlength="20" required-message="请输入新密码">
                        </div>
                  </div>
                  <div class="form-group">
                       <label for="renewpassword" class=" col-sm-2 control-label"><span class="pull-right">确认密码</span></label>
                        <div class="col-sm-6">
                             <input type="password" class="form-control" id="renewpassword"  placeholder="确认密码" check-type="required " minlength="6" maxlength="20" required-message="请输入确认密码">
                        </div>
                  </div>
                  <div class="form-group">
                        <button id="modifysubmit" class="btn btn-primary col-sm-offset-5 col-sm-2 " >提交</button>
                  </div>	
		       </form>
		   </div>
		</div>
</body>