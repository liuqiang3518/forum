<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page  isELIgnored="false"%>  
 <div class="row">
 <!--幻灯片-->
   <div class="col-sm-offset-1 col-sm-4">
      <div class="bootslider" id="bootslider">
	    <!-- Bootslider Loader -->
	    <div class="bs-loader">
		  <img src="images/loader.gif" width="31" height="31" alt="Loading.." id="loader"/>
	    </div>
	    <!-- /Bootslider Loader -->

	    <!-- Bootslider Container -->
	    <div class="bs-container">
	       <c:if test="${!empty hotposts_0}">    
           <c:forEach items="${hotposts_0}" var="item" varStatus="status"> 
             <c:if test="${status.index==0}">
             	<div class="bs-slide active" data-animate-in="tada" data-animate-out="bounceOutDown">
			       <div class="bs-background">
				      <a href="postdetail.do?id=${item.postId}"><img src="${item.imagePath }"  alt="" /></a>
			       </div>
		        </div> 
             </c:if> 
             <c:if test="${status.index!=0}">
             	<div class="bs-slide" data-animate-in="openDownLeftReturn" data-animate-out="slideUp">
			       <div class="bs-background">
				      <a href="postdetail.do?id=${item.postId}"><img src="${item.imagePath }"  alt="" /></a>
			       </div>
		        </div> 
             </c:if>                        
           </c:forEach>
           </c:if>

	    </div>
	    <!-- /Bootslider Container -->

	    <!-- Bootslider Progress -->
	    <div class="bs-progress progress">
		   <div class="progress-bar alizarin"></div>
	    </div>
	    <!-- /Bootslider Progress -->

	    <!-- Bootslider Thumbnails -->
	    <div class="bs-thumbnails text-center text-alizarin">
		   <ul class=""></ul>
	    </div>
      </div> 
   </div>
   <!--热点帖子-->
   <div class=" col-sm-6">
    <div class="span6">
       <div class="box box-color">	  
         <div class="box-title">
		    <h3>
              <i class="icon-bar-chart"></i>
                                          热点帖子
            </h3>
		 </div>
         <div class="box-content nopadding" style="height:250px">
		 <div  class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">
            <div class="box-content nopadding scrollable" data-visible="true" data-height="400" style="white-space:nowrap;overflow: hidden;text-overflow:ellipsis; word-break:break-all;width: auto; height: 250px;">
			   <table id="randomFeed" class="table table-nohead">
                   <tbody >
                     <c:if test="${!empty hotposts_1}">    
                     <c:forEach items="${hotposts_1}" var="ite" varStatus="status" step="2">
				      <tr style="display: table-row;">
				         <c:forEach items="${hotposts_1}" var="item" begin="${ status.index}" end="${ status.index+1}">
					     <td style="50%">
						     <div class="row">
							     <div class=" col-sm-8 pull-left">
								 	<a href="postdetail.do?id=${item.id}"  style=" font-size:14px">
								 	    ${item.name}
		                            </a>
								 </div>
								 <div class="col-sm-4 pull-left">
								    ${item.date}
								 </div>
							 </div>
						 </td>				         
				         </c:forEach>
					  </tr>                         
                     </c:forEach>
                     </c:if> 
				   </tbody>
			   </table>
			</div>
		 </div>
		 </div>
       </div>
    </div>
   </div>
  </div>
