<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
  <head>
  <%@ page  isELIgnored="false"%>  
  <%@ include file="/common/forum_meta.jsp"%>
   <script type="text/javascript" src="${ctx }/js/bootstrap-paginator.js" ></script>
   <script type="text/javascript" src="${ctx }/assets/js/personalpostmessage.js"></script>
  </head>
  <body>
    
    	<br/><br/><br/>
        <div class="row">
		   <div class="col-sm-offset-1 col-sm-10">
		       <table class="table">
				   <thead>
					  <tr role="row">
						 <td class="col-sm-10">
							 标题
						</td>
						<td class="col-sm-2">
							时间
						</td>
					</tr>
				</thead>
				<tbody id="list">
				</tbody>
             </table>
             <ul id="pageshow"></ul>  
		   </div>
		</div>
</body>