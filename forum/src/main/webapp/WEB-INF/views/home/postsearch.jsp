<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>postsearch</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>

    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/js/postsearch.js" ></script>
	<script type="text/javascript" src="${ctx }/js/touchSwipe.js"></script>
    <script type="text/javascript" src="${ctx }/js/application.min.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-paginator.js" ></script>
    <script type="text/javascript" src="${ctx }/js/fitvids.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootslider.js"></script>
	<script type="text/javascript" src="${ctx }/js/jquery.hotkeys.js"></script>
	<script type="text/javascript" src="${ctx }/js/demonstration.min.js"></script>
	<script type="text/javascript" src="${ctx }/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="${ctx }/ueditor/ueditor.all.min.js"></script>	
	<script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script>  
     <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
        
    <link rel="stylesheet" type="text/css" href="${ctx }/assets/css/postsearch.css">
    <link rel='stylesheet' type="text/css" href="${ctx }/css/jquery.cleditor.css" >
    <link rel="stylesheet" type="text/css" href="${ctx }/css/bootslider.css" />
    <link rel="stylesheet" type="text/css" href="${ctx }/css/bs-theme-metro.css" />
     <link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">

  </head>

  <body>
  <input type="hidden" id="target" value="${target}">
<%@ include file="navigation.jsp"%>
<%@ include file="hotpost.jsp"%>
<%@ include file="search.jsp"%><br/>
  <div class="row">
      <div class="col-sm-offset-1 col-sm-10">
		 <div class="row-fluid">
		    <div class="span8">
                <div class="box box-color  box-bordered green">
                   <div class="box-title">
                       <h3>
                          <i class="icon-th-list"></i>
                                                                                帖子
                       </h3>
                   </div>
				   <div class="box-content nopadding">
				       <table class="table">
					       <thead>
						       <tr role="row">
							        <td class="col-sm-6">
									     标题
									</td>
									<td class="col-sm-2">
									    作者
									</td>
									<td class="col-sm-2">
									    查看/回复
									</td>
									<td class="col-sm-2">
									    最后更新
									</td>
							   </tr>
						   </thead>
						   <tbody id="list">
						   </tbody>
                       </table>
                       <ul id="pageshow"></ul>  
				   </div>
				</div>
			</div>		    
		 </div>
	</div>
 </body>
</html>