<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 

<!DOCTYPE html>
<html>
  <head>
    <title>主页</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>
    
    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/js/index.js" ></script>
	<script type="text/javascript" src="${ctx }/js/touchSwipe.js"></script>
    <script type="text/javascript" src="${ctx }/js/application.min.js" ></script>
    <script type="text/javascript" src="${ctx }/js/fitvids.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootslider.js"></script>
	<script type="text/javascript" src="${ctx }/js/jquery.hotkeys.js"></script>
	<script type="text/javascript" src="${ctx }/js/demonstration.min.js"></script>
	<script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script>  
	
    <link rel="stylesheet" type="text/css"  href="${ctx }/assets/css/index.css" >
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/bootslider.css" />
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/bs-theme-metro.css" />
  </head>

  <body>
<%@ include file="navigation.jsp"%>
<%@ include file="hotpost.jsp"%>
<%@ include file="search.jsp"%><br/>
  <div class="row">
      <div class="col-sm-offset-1 col-sm-10">
		<c:if test="${!empty themes}">    
        <c:forEach items="${themes}" var="item">  
         <div class="row">
            <div class="row-fluid">
              <div class="span6">
                 <div class="box box-color ${item.color}  box-bordered">
                    <div class="box-title">
                     <h3>
                       <i class="${item.icon }"></i>
                         ${item.themeName }
                     </h3>
					 <div class="actions">					 
                        <a class="btn btn-mini content-slideUp" href="#">
                            <i class="icon-angle-down"></i>
                        </a>
                     </div>
                   </div>  
                     <div class="box-content">
                        <div class="row">
                           <c:if test="${!empty item.plates}">  
                           <c:forEach items="${item.plates}" var="ite"> 
                              <div class="col-sm-3" >
                                 <ul class="stats" onmouseover="this.style.cursor='pointer'" onclick="window.location.href='${ctx}/plate.do?id=${ite.id }';">
                                    <li class="${ite.color }" style="width:180px;margin-left:30px;margin-right:50px;margin-bottom:50px;">
                                       <i class="${ite.icon }"></i>
                                       <div class="details">
                                          <span class="big">${ite.plateName }</span>
                                          <span>${ite.postNumber }个帖子</span>
                                       </div>                                      
                                    </li>
                                 </ul>
                              </div>
                           </c:forEach>
                           </c:if>                          
                        </div>
                     </div>                                    
                 </div>
              </div>
            </div>
         </div>
        </c:forEach>
        </c:if>			
	  </div>
	</div>
 </body>
</html>