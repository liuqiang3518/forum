<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 

<!DOCTYPE html>
<html>
  <head>
    <title>主页</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>
    
    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/assets/js/personalcenter.js" ></script>
    
    <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
    <script type="text/javascript" src="${ctx }/js/application.min.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/My97DatePicker/WdatePicker.js" ></script>
	<script type="text/javascript" src="${ctx }/js/demonstration.min.js"></script>
	<script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script>  
	
	<link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">
    <link rel="stylesheet" type="text/css"  href="${ctx }/css/bs-theme-metro.css" />
  </head>

  <body>
  <c:if test="${!empty active}"> 
     <input type="hidden" id="active" value="${active}">
  </c:if>
  <input type="hidden" id="ctx" value="${ctx }">
<%@ include file="navigation.jsp"%>
<div class="row">
<div class="container-fluid" id="content">
	<div id="left" class="ui-sortable ui-resizable">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><span>个人信息</span></a>
				</div>
				<ul class="subnav-menu" style="display: block;">
					<li>
						<a href="personCenter.do?active=1" >基本信息</a>
					</li>
					<li>
						<a href="personCenter.do?active=2">修改信息</a>
					</li>
					<li>
						<a href="personCenter.do?active=3">修改密码</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><span>帖子</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="personCenter.do?active=4">帖子发布</a>
					</li>
					<li >
						<a href="personCenter.do?active=5"> 帖子评论</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><span>消息</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="personCenter.do?active=6" id="postMessageShow">帖子消息</a>
					</li>
					<li>
						<a href="personCenter.do?active=7" id="systemMessageShow">系统消息</a>
					</li>
				</ul>
			</div>
	</div>
	<div id="main">
    </div>
</div>

</div>
 </body>
</html>