<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<div id="navigation">
  	<div id="head" class="navbar  navbar-inner " role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="${ctx }/index.do">学习论坛</a>
				<a class="navbar-brand" >
                    <i class="icon-reorder"></i>
                </a>
			</div>

			<div class="nav-collapse">
				<ul class="nav navbar-nav navbar-right">
				    <c:if test="${!empty cookie['forum_user'].value}">
				     <li> 
				        <a href="personCenter.do?active=6"  title="消息" data-placement="bottom" id="messageShow"></a>
				     </li>
                      <li><a href="javascript:netdisk();"><i class="icon-cloud"></i>网盘</a></li>
                    <li id="user-menu" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <span id="userName"></span>
                            <i class="icon-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="javascript:personcenter();">个人中心</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1"  onclick="logout();">注销</a></li>
                        </ul>
                    </li>  
                    </c:if>
                    <c:if test="${empty cookie['forum_user'].value}">
                        <li><a href="${ctx }/login.do" class="hidden-phone visible-tablet visible-desktop" >登录</a></li>
                        <li><a href="${ctx }/regist.do" class="hidden-phone visible-tablet visible-desktop" >注册</a></li>
                    </c:if>
				
				</ul>	
		    </div>
	    </div>	
	</div>
</div>

