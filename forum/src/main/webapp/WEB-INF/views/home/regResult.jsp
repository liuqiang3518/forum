<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 

<!DOCTYPE html>
<html>
  <head>
    <title>注册结果</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>
    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="${ctx }/assets/js/reg.js" ></script>
    <script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script> 
    
    <link rel="stylesheet" type="text/css" href="${ctx }/assets/css/reg.css">
	<link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">
  </head>

  <body>
  <input type="hidden" id="ctx" value="${ctx }">
<%@ include file="navigation.jsp"%>
<div class="row">
    <div class="col-sm-offset-3  col-sm-6">
	    <div class="row-fluid">		
	       <c:if test="${!empty result}">      		    
		      <h1>"${result}"</h1>
		   </c:if> 
		</div>
	</div>
</div>

 </body>
</html>