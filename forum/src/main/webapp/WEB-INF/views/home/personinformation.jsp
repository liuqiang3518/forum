<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
  <head>
  <%@ page  isELIgnored="false"%>  
  <%@ include file="/common/forum_meta.jsp"%>
   <script type="text/javascript" src="${ctx }/assets/js/personalinformation.js" ></script>
  </head>
  <body>
    
    	<br/><br/><br/>
        <div class="row">
		   <div class="col-sm-offset-1 col-sm-3">
		       <img id="d_img" src="" alt="..." class="img-rounded pull-left" width="200px" height="200px">
		   </div>
		   <div class="col-sm-8 ">
		       <dl class="dl-horizontal pull-left">
                <dt>用户名：</dt>
                <dd id="d_username">张三</dd>
                <dt>账号：</dt>
                <dd id="d_email">liuyzi@163.com</dd>
                <dt>角色：</dt>
                <dd id="d_role">学生</dd>
                <dt id="dt_userid">学号/教师号：</dt>
                <dd id="d_userid">1341124241</dd>
                <dt>学院：</dt>
                <dd id="d_academy">计算机</dd>
                <dt>性别：</dt>
                <dd id="d_sex">男</dd>
                <dt>出生日期：</dt>
                <dd id="d_birthday">2012-12-12</dd>
                <dt>手机号码：</dt>
                <dd id="d_phoneNumber">13411242412</dd>
                <dt>籍贯：</dt>
                <dd id="d_nativePlace">江西</dd>
                <dt>现居地：</dt>
                <dd id="d_abode">重庆</dd>
              </dl>
		   </div>
		</div>
</body>