<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 

<!DOCTYPE html>
<html>
  <head>
    <title>登录</title>
    <%@ page  isELIgnored="false"%>  
    <%@ include file="/common/forum_meta.jsp"%>
    <script type="text/javascript" src="${ctx }/assets/js/navigation.js" ></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap3-validation.js"></script>
    <script type="text/javascript" src="${ctx }/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/base64.js"></script>
    <script type="text/javascript" src="${ctx }/assets/js/login.js" ></script>  
    <script type='text/javascript' src="${ctx }/dwr/engine.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/util.js"></script>  
    <script type='text/javascript' src="${ctx }/dwr/interface/userRemoting.js"></script>   
       	
    <link rel="stylesheet" type="text/css" href="${ctx }/assets/css/login.css">
    <link rel="stylesheet" type="text/css" href="${ctx }/css/bootstrap-dialog.min.css">

  </head>

  <body>
  <input type="hidden" id="ctx" value="${ctx }">
  <c:if test="${!empty url}">
    <input type="hidden" id="url" value="${url}">
  </c:if>
  <c:if test="${empty url}">
    <input type="hidden" id="url" value="">
  </c:if>
<%@ include file="navigation.jsp"%>
<div class="row">
    <div class="col-sm-offset-3  col-sm-6">
	    <div class="row-fluid">
            <div class="span6">
	            <div class="box box-color box-bordered ">
                   <div class="box-title">
                     <h3>
                       <i class="icon-user"></i>
                                                                      登录
                     </h3>
                   </div>
				   <div class="box-content" style="height:350px">
                       <form class="form-horizontal"  id="loginform" action="${ctx }/index.do" style="margin-top:60px;">
	                      <div class="form-group">
                               <label for="email" class=" col-sm-2 control-label"><span class="pull-right">email</span></label>
                               <div class="col-sm-6">
                                    <input type="text" class="form-control" id="email"  placeholder="email" check-type="required mail" minlength="6" maxlength="20" required-message="请输入正确email">
                               </div>
                          </div>	  
                          <div class="form-group">
                               <label for="password" class="col-sm-2 control-label"><span class="pull-right">密码</span></label>
                               <div class="col-sm-6">
                                   <input type="password" class="form-control" id="password" placeholder="密码" check-type="required" minlength="6" maxlength="20" required-message="输入长度大于等于6">
                               </div>
                          </div>	
		                  <div class="form-group">
						      <div class="col-sm-offset-2 col-sm-6">
							        <button id="regbtn"class="btn btn-warning col-sm-4 ">注册</button>
									<button id="loginbtn" class="btn btn-primary col-sm-4 pull-right">登录</button>
							  </div>						

                          </div>   
	                   </form>				      
				   </div>
				</div>
			</div>
		</div>
	</div>
</div>

 </body>
</html>