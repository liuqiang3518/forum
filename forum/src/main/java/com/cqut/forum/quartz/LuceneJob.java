package com.cqut.forum.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.cqut.forum.service.ILuceneService;


/**
* @ClassName: LuceneJob
* @Description: TODO
* @author 刘强
* @date 2015-5-13 上午1:00:02
*/ 
public class LuceneJob extends QuartzJobBean{
	
	private ILuceneService luceneService = null;

	public ILuceneService getLuceneService() {
		return luceneService;
	}

	public void setLuceneService(ILuceneService luceneService) {
		this.luceneService = luceneService;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		this.luceneService.luceneIndex();
		
	}
}
