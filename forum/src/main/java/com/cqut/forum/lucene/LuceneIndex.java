package com.cqut.forum.lucene;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.stereotype.Component;

import com.cqut.forum.entity.Post;
import com.cqut.forum.util.ConfigProperties;
 
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
 
@Component 
public class LuceneIndex {
 

	/**
	* @Title: getIndexWriter
	* @Description: TODO  
	* @return IndexWriter  
	* @author 刘强   
	* @date 2015-5-9 上午1:10:36
	*/ 
	public IndexWriter getIndexWriter(){
//		IndexWriter indexWriter = null;
//        File indexDir = new File("F:\\index");        
//        Analyzer analyzer = new IKAnalyzer();	
//        IndexWriterConfig iwConfig = new IndexWriterConfig(Version.LUCENE_4_10_2, analyzer);
//        iwConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
//        try {
//            Directory fsDirectory = FSDirectory.open(indexDir);
//            indexWriter = new IndexWriter(fsDirectory, iwConfig);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return indexWriter;
		return null;
	}
	
	
    /**
    * @Title: addIndex
    * @Description: 添加索引  
    * @return void  
    * @author 刘强   
    * @date 2015-5-8 下午8:10:13
    */ 
    public  void addIndex(Post post) {
    	IndexWriter indexWriter = getIndexWriter();
        TextField postIdField = new TextField("id", post.getId().toString(), Store.YES);  // 不要用StringField
        TextField postTitleField = new TextField("title",post.getName(), Store.YES);
        TextField postPostDateField = new TextField("postDate",post.getPostDate().toString(), Store.YES);
        System.out.println("建立索引："+post.getId()+"  "+post.getName());
        Document doc = new Document();
        doc.add(postIdField);
        doc.add(postTitleField);
        doc.add(postPostDateField);
        try {
            indexWriter.addDocument(doc);
            indexWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void addIndex(List<Post> posts){
    	for(Post post:posts){
    		this.addIndex(post);
    	}
    }
    /**
    * @Title: deleteIndex
    * @Description: 删除索引  
    * @return void  
    * @author 刘强   
    * @date 2015-5-8 下午8:10:33
    */ 
    public void deleteIndex(Post post){
    	IndexWriter indexWriter = getIndexWriter();
    	Term term=getTerm(post);
    	try {
            indexWriter.deleteDocuments(term);
            indexWriter.close();
//            indexWriter.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }  	
    }
    /**
    * @Title: deleteIndexAll
    * @Description: 删除所有索引 
    * @return void  
    * @author 刘强   
    * @date 2015-5-8 下午8:10:50
    */ 
    public void deleteIndexAll(){
    	IndexWriter indexWriter = getIndexWriter();
    	try {
			indexWriter.deleteAll();
			indexWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    /**
    * @Title: updateIndex
    * @Description: TODO  
    * @return void  
    * @author 刘强   
    * @date 2015-5-9 上午1:15:28
    */ 
    public void updateIndex(Post post){
    	Term term=getTerm(post);
    	IndexWriter indexWriter = getIndexWriter();
        TextField postIdField = new TextField("id", post.getId().toString(), Store.YES);  // 不要用StringField
        TextField postContentField = new TextField("title",post.getName(), Store.YES);
        Document doc = new Document();
        doc.add(postIdField);
        doc.add(postContentField);
    	try {
			indexWriter.updateDocument(term, doc);
			indexWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
	/**
	* @Title: getTerm
	* @Description: TODO  
	* @return Term  
	* @author 刘强   
	* @date 2015-5-8 下午8:22:14
	*/ 
	private Term getTerm(Post post){
		Term term=new Term("id", post.getId().toString());
		return term;
	}
    
//  public static void main(String[] args) {
//  String ID;
//  String content;
//
//  ID = "1231";
//  content = "BuzzFeed has compiled an amazing array of " +
//          "ridiculously strange bridesmaid snapshots, courtesy of Awkward Family Photos. ";
//  indexPost(ID, content);
//
//  ID = "1234";
//  content = "Lucene是apache软件基金会4 jakarta项目组的一个子项目，是一个开放源代码的全文检索引擎工具包";
//  indexPost(ID, content);
//
//  ID = "1235";
//  content = "Lucene不是一个完整的全文索引应用，而是是一个用Java写的全文索引引擎工具包，它可以方便的嵌入到各种应用中实现";
//  indexPost(ID, content);
//  Term term=new Term("id","1235");
//  deleteIndex(term);
//  		
//}
}