package com.cqut.forum.lucene;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.index.DirectoryReader;
import org.springframework.stereotype.Component;
 
import java.io.File;
import java.util.ArrayList;
import java.util.List;
@Component
public class LuceneSearch {
	private static int searchNum=1000;//搜索相似度最高的5条记录
	private IndexSearcher isearcher;
	private Analyzer analyzer;
	
	/**
	* @Title: init
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-9 上午2:00:26
	*/ 
	public void init(){
//		analyzer = new IKAnalyzer();
//        File indexDir = new File("F:\\index");
//        try {
//           Directory fsDirectory = FSDirectory.open(indexDir);
//           DirectoryReader ireader = DirectoryReader.open(fsDirectory);
//           isearcher = new IndexSearcher(ireader);
//        } catch (Exception e) {
//        	 
//        }
	}
	
    /**
    * @Title: search
    * @Description: TODO  
    * @return List<String>  
    * @author 刘强   
    * @date 2015-5-9 上午2:00:21
    */ 
    public List<String> search(String target,int curPage,int size) {
        List<String> result=new ArrayList<String>();
        QueryParser qp = new QueryParser("title", analyzer);         //使用QueryParser查询分析器构造Query对象
        qp.setDefaultOperator(QueryParser.AND_OPERATOR);
        Sort sort=new Sort(new SortField("postDate",SortField.Type.STRING, true));//按postDate降序排列
        try {
        	Query query = qp.parse(target);     // 搜索目标词
        	TopDocs topDocs = isearcher.search(query , searchNum,sort); 
        	System.out.println("命中:" + topDocs.totalHits);
        	ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        	int start=(curPage-1)*size;//开始位置
        	int end=Math.min(start + size, scoreDocs.length);//结束位置
        	for (int i = start; i < end; i++){
        		Document targetDoc = isearcher.doc(scoreDocs[i].doc);
        		result.add(targetDoc.get("id"));
//        		System.out.println("内容:" + targetDoc.toString());
        	}
        } catch (Exception e) {
 
        }
        return result;
    }
    /**
    * @Title: total
    * @Description: TODO  
    * @return int  
    * @author 刘强   
    * @date 2015-5-9 上午2:00:17
    */ 
    public int total(String target){
    	int result=0;
        QueryParser qp = new QueryParser("title", analyzer);         //使用QueryParser查询分析器构造Query对象
        qp.setDefaultOperator(QueryParser.AND_OPERATOR);
        try {
        	Query query = qp.parse(target);     // 搜索目标词
        	TopDocs topDocs = isearcher.search(query , searchNum); 
//        	System.out.println("命中:" + topDocs.totalHits);
        	result=topDocs.totalHits;
        } catch (Exception e) {
        }
        return result;
    }
	public IndexSearcher getIsearcher() {
		return isearcher;
	}

	public void setIsearcher(IndexSearcher isearcher) {
		this.isearcher = isearcher;
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}
    
}