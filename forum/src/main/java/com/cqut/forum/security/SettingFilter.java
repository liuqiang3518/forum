package com.cqut.forum.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.cqut.forum.util.CookieHandler;
import com.cqut.forum.util.DefaultElements;

public class SettingFilter implements HandlerInterceptor {

    private String ignore_method;
    private String jump_url;
    
    private String[] ignore_methods;

	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub

	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2) throws Exception {
        String url=request.getRequestURL().toString();

        // 是否过滤
     	boolean doFilter = true;
     	for (String s : ignore_methods) {
     		if (url.toString().contains(s)) {
     			// 如果uri中包含不过滤的uri，则不进行过滤
     			doFilter = false;
     			break;
     		}
     	}
     	if (doFilter) {
     		String cookieValue=CookieHandler.getCookieValue(request, DefaultElements.ADMINCOOKIE_NAME);
     		if(null==cookieValue||"".equals(cookieValue)){
//     		   request.getRequestDispatcher("/WEB-INF/views/setting/adminlogin.jsp").forward(request, response);
     			response.sendRedirect(jump_url);
     			return false;  
     		}else{
     			return true;
     		}
     	}
//     	response.sendRedirect(outTimePageUrl); // 返回提示页面  
     	return true;
	}

	public String getIgnore_method() {
		return ignore_method;
	}

	public void setIgnore_method(String ignore_method) {
		this.ignore_method = ignore_method;
		this.ignore_methods=ignore_method.trim().split(",");
	}

	public String[] getIgnore_methods() {
		return ignore_methods;
	}

	public void setIgnore_methods(String[] ignore_methods) {
		this.ignore_methods = ignore_methods;
	}

	public String getJump_url() {
		return jump_url;
	}

	public void setJump_url(String jump_url) {
		this.jump_url = jump_url;
	}

}
