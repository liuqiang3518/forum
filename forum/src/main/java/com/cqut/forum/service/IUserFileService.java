package com.cqut.forum.service;

import java.util.List;

import com.cqut.forum.entity.Userfile;
import com.cqut.forum.entity.Ztree;

public interface IUserFileService extends IBaseService {

	/**
	* @Title: save
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-14 下午5:24:23
	*/ 
	public void save(Userfile userfile);
	
	/**
	* @Title: getUserfile
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-14 下午7:23:06
	*/ 
	public List<Userfile> getUserfile(long userId, Long parentId, int indexpage, int size);
	/**
	* @Title: getUserfileTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-14 下午7:23:35
	*/ 
	public int getUserfileTotal(long userId, Long parentId);
	/**
	* @Title: getUserfileByMd5
	* @Description: TODO  
	* @return Userfile  
	* @author 刘强   
	* @date 2015-5-15 上午11:28:44
	*/ 
	public Userfile getUserfileByUserIdAndFileName(long userId, String fileName);
	/**
	* @Title: getUserfileById
	* @Description: TODO  
	* @return Userfile  
	* @author 刘强   
	* @date 2015-5-15 下午2:14:29
	*/ 
	public Userfile getUserfileById(Long id);
	/**
	* @Title: Update
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-15 下午2:14:31
	*/ 
	public void updateUserFile(Userfile userfile);
	/**
	* @Title: delete
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-15 下午2:18:35
	*/ 
	public void deleteUserfile(Userfile userfile);
	/**
	* @Title: delete
	* @Description: 删除该文件下所有文件  
	* @return void  
	* @author 刘强   
	* @date 2015-5-15 下午2:20:01
	*/ 
	public void deleteUserfile(long id);
	/**
	* @Title: getUserfileChildNum
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-15 下午4:46:55
	*/ 
	public int getUserfileChildNum(Long id);
	/**
	* @Title: getUserfileByParentId
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-15 下午4:49:31
	*/ 
	public List<Userfile> getUserfileByParentId(Long id);
	/**
	* @Title: getUserfileByUserIdAndisFile
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-15 下午8:06:01
	*/ 
	public List<Ztree> getUserfileByUserIdAndisFile(Long userId, int isFile);
	/**
	* @Title: getUserfileByUserIdAndfileType
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-16 上午9:28:35
	*/ 
	public List<Userfile> getUserfileByUserIdAndfileType(long userId, int fileType, int indexpage, int size);
	/**
	* @Title: getUserfileTotalByUserIdAndfileType
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-16 上午9:44:34
	*/ 
	public int getUserfileTotalByUserIdAndfileType(long userId, int fileType);
}
