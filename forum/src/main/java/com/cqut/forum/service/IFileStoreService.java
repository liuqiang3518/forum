package com.cqut.forum.service;

import com.cqut.forum.entity.Filestore;

public interface IFileStoreService extends IBaseService {

	/**
	* @Title: save
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-14 下午2:12:37
	*/ 
	public void save(Filestore filestore);
	/**
	* @Title: getFilestoreByMD5
	* @Description: TODO  
	* @return Filestore  
	* @author 刘强   
	* @date 2015-5-14 下午5:12:02
	*/ 
	public Filestore getFilestoreByMD5(String MD5);
}
