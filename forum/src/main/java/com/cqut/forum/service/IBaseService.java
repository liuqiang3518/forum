package com.cqut.forum.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.impl.BaseDao;


/**
 * @author 刘强
 *service层的共有父类，提供了简单的增删改查。
 */
@Service
public interface IBaseService {

	
	
	/**
	 * 根据主键，返回实体
	 * @param id 主键
	 * @return	实体或空（如果没有查到）
	 */
	public <T> T get(Class<T> entityClass, Serializable id);
	
	/**
	 * 保存实体对象，返回主键
	 * @param model
	 * @return
	 */
	public Serializable save(Object model);
	
	/**
	 * 更新实体对象，不返回值
	 * @param entity
	 */
	public void update(Object model);
	
	
	/**
	 * 保存或更新
	 * @param model 实体类
	 */
    public void saveOrUpdate(Object model) ;
	/**
	 * 删除指定对象，不返回值
	 * @param model
	 */
	public void delete(Object model);
	
	/**
	 * 查出所有对象
	 * @param entityClass
	 * @return
	 */
	public <T> List<T> list(Class<T> entityClass);

	/**
	 * 统计所有对象个数
	 * @param entityClass
	 * @return
	 */
	public int count(Class<?> entityClass);
	

}
