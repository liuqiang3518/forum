package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IAcademyDao;
import com.cqut.forum.entity.Academy;
import com.cqut.forum.service.IAcademyService;

@Service
public class AcademyService extends BaseService implements IAcademyService {
    
	IAcademyDao academyDao;
	
	public IAcademyDao getAcademyDao() {
		return academyDao;
	}
	@Autowired 
	public void setAcademyDao(IAcademyDao academyDao) {
		this.academyDao = academyDao;
	}

	public Academy getAcademy(long id) {
		return academyDao.get(Academy.class, id);
	}

	public List<Academy> getAcademys() {
		return academyDao.getAcademys();
	}

}
