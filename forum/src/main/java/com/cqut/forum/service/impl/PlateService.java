package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;


import com.cqut.forum.dao.IPlateDao;
import com.cqut.forum.dao.impl.BaseDao;
import com.cqut.forum.entity.Plate;
import com.cqut.forum.entity.Theme;
import com.cqut.forum.service.IPlateService;

@Service
public class PlateService extends BaseDao implements IPlateService {
    private IPlateDao plateDao;
    
	public IPlateDao getPlateDao() {
		return plateDao;
	}

	public void setPlateDao(IPlateDao plateDao) {
		this.plateDao = plateDao;
	}

	public List<Plate> getPlatesByTheme(Long themeId) {
		return plateDao.getPlatesByTheme(themeId);
	}

	public Plate getPlateById(Long id) {
		return plateDao.getPlateById(id);
	}

	public List<Plate> getPlateByPlateId(long plateid) {
		return plateDao.getPlateByPlateId(plateid);
	}

	public List<Plate> getPlate(int currentPage, int pageSize, String name,
			long themeId, String order, String sort) {
		return plateDao.getPlate(currentPage, pageSize, name, themeId, order, sort);
	}

	public int getPlateTotal(String name, long themeId) {
		return plateDao.getPlateTotal(name, themeId);
	}

	public void delete(long id) {
		super.delete(Plate.class, id);
	}

	public void update(Plate plate) {
		plateDao.update(plate);
	}

	public void save(Plate plate) {
		plateDao.save(plate);
	}

}
