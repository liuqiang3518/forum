package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IMessageDao;
import com.cqut.forum.entity.Message;
import com.cqut.forum.service.IMessageService;
@Service
public class MessageService extends BaseService implements IMessageService {

	private IMessageDao messageDao;
	
	public IMessageDao getMessageDao() {
		return messageDao;
	}

	public void setMessageDao(IMessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public void saveMessage(Message message) {
		messageDao.save(message);
	}

	public void update(Message message) {
		messageDao.update(message);
	}

	public void delete(Message message) {
		messageDao.delete(message);
	}

	public Message getMessageById(Long id) {
		return messageDao.get(Message.class, id);
	}

	public List<Message> getPostMessage(long userId, int indexpage, int size) {
		return messageDao.getPostMessage(userId, indexpage, size);
	}

	public int getPostMessageTotal(long userId) {
		return messageDao.getPostMessageTotal(userId);
	}

	public List<Message> getSystemMessage(long userId, int indexpage, int size) {
		return messageDao.getSystemMessage(userId, indexpage, size);
	}

	public int getSystemMessageTotal(long userId) {
		return messageDao.getSystemMessageTotal(userId);
	}

	public int getMessageNum(long userId) {
		return messageDao.getMessageNum(userId);
	}

	public int getPostMessageNum(long userId) {
		return messageDao.getPostMessageNum(userId);
	}

	public int getSystemMessageNum(long userId) {
		// TODO Auto-generated method stub
		return messageDao.getSystemMessageNum(userId);
	}

}
