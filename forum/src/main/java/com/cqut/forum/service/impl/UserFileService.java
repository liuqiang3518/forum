package com.cqut.forum.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IUserFileDao;
import com.cqut.forum.dao.impl.UserDao;
import com.cqut.forum.dao.impl.UserFileDao;
import com.cqut.forum.entity.Userfile;
import com.cqut.forum.entity.Ztree;
import com.cqut.forum.service.IFileStoreService;
import com.cqut.forum.service.IUserFileService;
@Service
public class UserFileService extends BaseService implements IUserFileService {
    private IUserFileDao userFileDao;
	public IUserFileDao getUserFileDao() {
		return userFileDao;
	}

	public void setUserFileDao(IUserFileDao userFileDao) {
		this.userFileDao = userFileDao;
	}


	public void save(Userfile userfile) {
		userFileDao.save(userfile);
	}

	public List<Userfile> getUserfile(long userId, Long parentId,
			int indexpage, int size) {
		return userFileDao.getUserfile(userId, parentId, indexpage, size);
	}


	public int getUserfileTotal(long userId, Long parentId) {
		return userFileDao.getUserfileTotal(userId, parentId);
	}

	public Userfile getUserfileByUserIdAndFileName(long userId,String fileName) {
		return userFileDao.getUserfileByUserIdAndFileName(userId,fileName);
	}

	public Userfile getUserfileById(Long id) {
		return userFileDao.get(Userfile.class, id);
	}

	public void updateUserFile(Userfile userfile) {
		userFileDao.updateUserFile(userfile);
	}

	public void deleteUserfile(Userfile userfile) {
		userFileDao.delete(userfile);
	}

	public void deleteUserfile(long id) {
		int childNum=userFileDao.getUserfileChildNum(id);
		if(childNum==0){
			userFileDao.delete(Userfile.class, id);
		}else{
			List<Userfile> lists=userFileDao.getUserfileByParentId(id);
			for(Userfile userfile:lists){
				this.deleteUserfile(userfile.getId());
			}
			userFileDao.delete(Userfile.class, id);
		}
		
	}


	public int getUserfileChildNum(Long id) {
		return userFileDao.getUserfileChildNum(id);
	}


	public List<Userfile> getUserfileByParentId(Long id) {
		return userFileDao.getUserfileByParentId(id);
	}


	public List<Ztree> getUserfileByUserIdAndisFile(Long userId, int isFile) {
		List<Ztree> lists=new ArrayList<Ztree>();
		List<Userfile> userfiles=userFileDao.getUserfileByUserIdAndisFile(userId, isFile);
		for(Userfile userfile:userfiles){
			Ztree ztree=new Ztree();
			ztree.setId(userfile.getId());
			ztree.setpId(userfile.getParentId());
			ztree.setName(userfile.getFileName());
			if(userfile.getParentId()==-1){
				ztree.setOpen(true);
			}else{
				ztree.setOpen(false);
			}
			lists.add(ztree);
		}
		Ztree ztree=new Ztree();
		ztree.setId((long)-1);
		ztree.setpId((long)-2);
		ztree.setName("全部文件");
		ztree.setOpen(true);
		lists.add(ztree);
		return lists;
	}


	public List<Userfile> getUserfileByUserIdAndfileType(long userId,
			int fileType,int indexpage, int size) {
		return userFileDao.getUserfileByUserIdAndfileType(userId, fileType,indexpage,size);
	}

	public int getUserfileTotalByUserIdAndfileType(long userId, int fileType) {
		return userFileDao.getUserfileTotalByUserIdAndfileType(userId, fileType);
	}
	
}
