package com.cqut.forum.service;

import java.util.List;

import com.cqut.forum.entity.Theme;

public interface IThemeService extends IBaseService  {
	/**
	* @Title: getAdmin
	* @Description: TODO  
	* @return List<Admin>  
	* @author 刘强   
	* @date 2015-5-17 下午4:28:16
	*/ 
	public List<Theme> getTheme(int currentPage, int pageSize, String name, String order, String sort);
	/**
	* @Title: getAdminTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-17 下午4:34:28
	*/ 
	public int getThemeTotal(String name);
	/**
	* @Title: delete
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 下午7:40:42
	*/ 
	public void delete(long id);
	/**
	* @Title: update
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 下午10:32:41
	*/ 
	public void update(Theme theme);
	/**
	* @Title: save
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 下午10:32:37
	*/ 
	public void save(Theme theme);
	/**
	* @Title: getTheme
	* @Description: TODO  
	* @return Theme  
	* @author 刘强   
	* @date 2015-5-17 下午10:52:47
	*/ 
	public Theme getTheme(long id);
	/**
	* @Title: getThemes
	* @Description: TODO  
	* @return List<Theme>  
	* @author 刘强   
	* @date 2015-5-18 上午10:37:12
	*/ 
	public List<Theme> getThemes();
	
	/**
	* @Title: commentNumByThemeId
	* @Description: 统计主题下的帖子评论量  
	* @return int  
	* @author 刘强   
	* @date 2015-6-6 上午2:40:43
	*/ 
	public Long commentNumByThemeId(Long id);
}
