package com.cqut.forum.service.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IUserDao;
import com.cqut.forum.entity.User;
import com.cqut.forum.service.IUserService;

/**
 * @author 刘强
 *
 */
@Service
public class UserService extends BaseService implements IUserService{
	IUserDao userDao;

	public IUserDao getUserDao() {
		return userDao;
	}
	@Autowired 
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}


	public User getUserByEmailAndPassword(String email,String password){
		return userDao.getUserByEmailAndPassword(email, password);
	}

	public User getUserByUserIdAndPassword(String studentId,String password){
		return userDao.getUserByUserIdAndPassword(studentId, password);
	}
	public boolean isExeistEmail(String email){
		return userDao.isExeistEmail(email);
	}

	public User getUserByUserId(long userId) {
		return userDao.getUserByUserId(userId);
	}

	public User getUserByEmail(String email) {
		return userDao.getUserByEmail(email);
	}

	public User getUserById(long id) {
		return userDao.getUserById(id);
	}

	public void saveOrUpdate(User user) {
		userDao.saveOrUpdate(user);
	}

	public List<User> getUser(String name, String academyId, String starTime,
			String endTime,String role,String isfrozen,String email, int currentPage, int pageSize, 
			String order, String sort) {
		return userDao.getUser(name, academyId, starTime, endTime,role,isfrozen,email, currentPage, pageSize, order, sort);
	}

	public int getUserTotal(String name, String academyId, String starTime,
			String endTime,String role,String isfrozen,String email) {
		return userDao.getUserTotal(name, academyId, starTime, endTime,role,isfrozen,email);
	}

	public void delete(long id) {
		userDao.delete(User.class, id);
	}

	public void updateUserForFrozen(long id) {
		User user=userDao.get(User.class, id);
		user.setIsfrozen(1);
		userDao.update(user);
	}

	public void updateUserForThaw(long id) {
		User user=userDao.get(User.class, id);
		user.setIsfrozen(0);
		userDao.update(user);
	}
	
	
}
