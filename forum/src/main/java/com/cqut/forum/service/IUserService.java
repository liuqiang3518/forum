package com.cqut.forum.service;

import java.util.List;

import com.cqut.forum.entity.User;




public interface IUserService extends IBaseService{

	/**
	* @author 刘强
	* @Title: getUserByEmailAndPassword
	* @Description: 通过Email和password获取User对象
	* @return User    
	* @date 2015年4月21日 下午3:29:49
	*/ 
	public User getUserByEmailAndPassword(String email, String password);

	/**
	* @author 刘强
	* @Title: getUserByStudentIdAndPassword
	* @Description: 通过studentId和password获取User对象
	* @return User    返回类型
	* @date 2015年4月21日 下午3:29:55
	*/ 
	public User getUserByUserIdAndPassword(String studentId, String password);
	/**
	* @Title: isExeistEmail
	* @Description: 检查邮箱账号是否已存在  
	* @return boolean  
	* @throws
	* @author 刘强   
	* @date 2015-4-24 下午3:31:03
	*/ 
	public boolean isExeistEmail(String email);
	/**
	* @Title: getUserByUserId
	* @Description: 通过userId获取user对象  
	* @return User  
	* @throws
	* @author 刘强   
	* @date 2015-4-25 下午1:32:09
	*/ 
	public User getUserByUserId(long userId);
	/**
	* @Title: getUserByEmail
	* @Description: 通过email获取user对象  
	* @return User  
	* @throws
	* @author 刘强   
	* @date 2015-4-26 下午12:41:35
	*/ 
	public User getUserByEmail(String email);
	/**
	* @Title: getUserById
	* @Description: TODO  
	* @return User  
	* @author 刘强   
	* @date 2015-5-5 下午4:06:00
	*/ 
	public User getUserById(long id);	
	/**
	* @Title: saveOrUpdate
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-11 下午6:28:32
	*/ 
	public void saveOrUpdate(User user);
	
	/**
	* @Title: getUser
	* @Description: TODO  
	* @return List<User>  
	* @author 刘强   
	* @date 2015-5-18 下午2:15:11
	*/ 
	public  List<User> getUser(String name, String academyId, String starTime, String endTime, String role, String isfrozen, String email,
							   int currentPage, int pageSize, String order, String sort);
	/**
	* @Title: getUserTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-18 下午2:15:48
	*/ 
	public int getUserTotal(String name, String academyId, String starTime, String endTime, String role, String isfrozen, String email);
    /**
    * @Title: delete
    * @Description: TODO  
    * @return void  
    * @author 刘强   
    * @date 2015-5-18 下午4:45:50
    */ 
    public void delete(long id);
	/**
	* @Title: frozen
	* @Description: 冻结用户  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午5:04:01
	*/ 
	public void updateUserForFrozen(long id);
	/**
	* @Title: thaw
	* @Description: 解冻用户  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午5:04:03
	*/ 
	public void updateUserForThaw(long id);
}
