package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IPostDao;
import com.cqut.forum.entity.Hotpost;
import com.cqut.forum.entity.Post;
import com.cqut.forum.service.IPostService;
@Service
public class PostService extends BaseService implements IPostService {
	private IPostDao postDao;
	

	public IPostDao getPostDao() {
		return postDao;
	}


	public void setPostDao(IPostDao postDao) {
		this.postDao = postDao;
	}

	public List<Hotpost> getHotPostByType(int type, int size) {
		return postDao.getHotPostByType(type, size);
	}

	public List<Post> getPostByPlateId(long plateId, int pageIndex, int size) {
		return  postDao.getPostByPlateId(plateId, pageIndex, size);
	}

	public int getPostTotalByPlateId(long plateId) {
		
		return postDao.getPostTotalByPlateId(plateId);
	}

	public List<Post> getPostByHotpost(int type, int size) {
		return postDao.getPostByHotpost(type, size);
	}

	public Post getPostById(Long id) {
		return postDao.getPostById(id);
	}

	public void save(Post post) {
		postDao.save(post);
	}

	public void saveOrupdate(Post post) {
		postDao.saveOrUpdate(post);
	}

	public List<Post> getAll() {
		return postDao.getAll();
	}

	public List<Post> getPostByUserId(long userId, int pageIndex, int size) {
		return postDao.getPostByUserId(userId, pageIndex, size);
	}

	public int getPostTotalByUserId(long userId) {
		return postDao.getPostTotalByUserId(userId);
	}

	public List<Post> getPost(String title, String userName, String starTime,
			String endTime, int currentPage, int pageSize, String order,
			String sort) {
		return postDao.getPost(title, userName, starTime, endTime, currentPage, pageSize, order, sort);
	}

	public int getPostTotal(String title, String userName, String starTime,
			String endTime) {
		return postDao.getPostTotal(title, userName, starTime, endTime);
	}

	public Post getPost(long id) {
		return postDao.get(Post.class, id);
	}

	public List<Hotpost> getHotPost(String title,int currentPage, int pageSize,
			String order, String sort) {
		return postDao.getHotPost(title, currentPage, pageSize, order, sort);
	}

	public int getHotPostTotal(String title) {
		return postDao.getHotPostTotal(title);
	}

	public void saveHotpost(Hotpost hotpost) {
		postDao.save(hotpost);
	}

	public void deleteHotpost(long id) {
		postDao.delete(Hotpost.class, id);
	}
    


}
