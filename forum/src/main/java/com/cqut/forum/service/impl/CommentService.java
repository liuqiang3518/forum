package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.ICommentDao;
import com.cqut.forum.entity.Comment;
import com.cqut.forum.entity.Post;
import com.cqut.forum.service.ICommentService;
@Service
public class CommentService extends BaseService implements ICommentService {

	private ICommentDao commentDao;
	
	public ICommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}

	public List<Comment> getComment(Long postId, int pageIndex, int size) {
		return commentDao.getComment(postId, pageIndex, size);
	}

	public int getCommentTotal(Long postId) {
		return commentDao.getCommentTotal(postId);
	}

	public List<Comment> getComment(Long postId, Long targetId) {
		return commentDao.getComment(postId, targetId);
	}

	public Comment get(long id) {
		return commentDao.get(id);
	}

	public Post getPostByCommentId(long id) {
		return commentDao.getPostByCommentId(id);
	}

	public void save(Comment comment) {
		commentDao.save(comment);
	}

	public List<Comment> getCommentByUserId(Long userId, int pageIndex, int size) {
		return commentDao.getCommentByUserId(userId, pageIndex, size);
	}

	public Integer getCommentTotalByUserId(Long userId) {
		return commentDao.getCommentTotalByUserId(userId);
	}


}
