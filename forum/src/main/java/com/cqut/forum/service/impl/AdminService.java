package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IAdminDao;
import com.cqut.forum.entity.Admin;
import com.cqut.forum.service.IAdminService;
@Service
public class AdminService extends BaseService implements IAdminService {
    private IAdminDao adminDao;

	public void setAdminDao(IAdminDao adminDao) {
		this.adminDao = adminDao;
	}

	public Admin getAdmin(String name, String password) {
		return adminDao.getAdmin(name, password);
	}

	public List<Admin> getAdmin(int currentPage, int pageSize, String name,
			String order, String sort) {
		return adminDao.getAdmin(currentPage, pageSize, name, order, sort);
	}

	public int getAdminTotal(String name) {
		// TODO Auto-generated method stub
		return adminDao.getAdminTotal(name);
	}

	public void delete(long id) {
		adminDao.delete(Admin.class, id);
	}

	public Admin getAdmin(long id) {
		return adminDao.get(Admin.class, id);
	}

	public void update(Admin admin) {
		adminDao.update(admin);
	}

	public void save(Admin admin) {
		adminDao.save(admin);
	}

	public Admin getAdminByUserName(String userName) {
		return adminDao.getAdminByUserName(userName);
	}

}
