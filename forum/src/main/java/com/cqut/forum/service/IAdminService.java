package com.cqut.forum.service;

import java.util.List;

import com.cqut.forum.entity.Admin;

public interface IAdminService extends IBaseService {

	/**
	* @Title: getAdmin
	* @Description: 根据用户名密码获取admin  
	* @return Admin  
	* @author 刘强   
	* @date 2015-5-17 上午10:35:18
	*/ 
	public Admin getAdmin(String name, String password);
	/**
	* @Title: getAdmin
	* @Description: TODO  
	* @return List<Admin>  
	* @author 刘强   
	* @date 2015-5-18 下午11:37:25
	*/ 
	public List<Admin> getAdmin(int currentPage, int pageSize, String name, String order, String sort);
	/**
	* @Title: getAdminTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-18 下午11:37:27
	*/ 
	public int getAdminTotal(String name);
	/**
	* @Title: delete
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午11:55:51
	*/ 
	public void delete(long id);
	/**
	* @Title: getAdmin
	* @Description: TODO  
	* @return Admin  
	* @author 刘强   
	* @date 2015-5-19 上午12:05:03
	*/ 
	public Admin getAdmin(long id);
	/**
	* @Title: update
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-19 上午12:14:38
	*/ 
	public void update(Admin admin);
	/**
	* @Title: save
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-19 上午12:14:35
	*/ 
	public void save(Admin admin);
	/**
	* @Title: getAdminByUserName
	* @Description: TODO  
	* @return Admin  
	* @author 刘强   
	* @date 2015-5-19 上午12:43:10
	*/ 
	public Admin getAdminByUserName(String userName);
}
