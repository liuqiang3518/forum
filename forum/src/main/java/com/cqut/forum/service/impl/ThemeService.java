package com.cqut.forum.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IThemeDao;
import com.cqut.forum.entity.Theme;
import com.cqut.forum.service.IThemeService;

@Service
public class ThemeService extends BaseService  implements IThemeService{
    private IThemeDao themeDao;

	public void setThemeDao(IThemeDao themeDao) {
		this.themeDao = themeDao;
	}

	public List<Theme> getTheme(int currentPage, int pageSize, String name,
			String order, String sort) {
		return themeDao.getTheme(currentPage, pageSize, name, order, sort);
	}

	public int getThemeTotal(String name) {
		return themeDao.getThemeTotal(name);
	}

	public void delete(long id) {
		Theme theme=super.get(Theme.class, id);
		themeDao.delete(theme);
	}

	public void update(Theme theme) {
		themeDao.update(theme);
	}

	public void save(Theme theme) {
		themeDao.save(theme);
	}

	public Theme getTheme(long id) {
		return themeDao.get(Theme.class, id);
	}

	public List<Theme> getThemes() {
		List<Theme> list=themeDao.getThemes();
		return list;
	}

	public Long commentNumByThemeId(Long id) {
		Long result=themeDao.commentNumByThemeId(id);
		if(result!=null){
			return result;
		}else{
			return (long)0;
		}
	}


}
