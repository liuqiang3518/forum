package com.cqut.forum.service.impl;

import org.springframework.stereotype.Service;

import com.cqut.forum.dao.IFileStoreDao;
import com.cqut.forum.entity.Filestore;
import com.cqut.forum.service.IFileStoreService;
@Service
public class FileStoreService extends BaseService implements IFileStoreService {
    private IFileStoreDao fileStoreDao;
    
    
	public IFileStoreDao getFileStoreDao() {
		return fileStoreDao;
	}


	public void setFileStoreDao(IFileStoreDao fileStoreDao) {
		this.fileStoreDao = fileStoreDao;
	}


	public void save(Filestore filestore) {
		fileStoreDao.save(filestore);
	}


	public Filestore getFilestoreByMD5(String MD5) {
		return fileStoreDao.getFilestoreByMD5(MD5);
	}

}
