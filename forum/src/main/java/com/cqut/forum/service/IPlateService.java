package com.cqut.forum.service;

import java.util.List;

import com.cqut.forum.entity.Plate;
import com.cqut.forum.entity.Theme;

public interface IPlateService extends IBaseService {
	/**
	* @Title: getPlatesByThemeId
	* @Description: 通过themeId获取其下面的plate(板块)对象 
	* @return List<Plate>  
	* @throws
	* @author 刘强   
	* @date 2015-4-26 下午4:18:24
	*/ 
	public List<Plate> getPlatesByTheme(Long themeId);
	/**
	* @Title: getPlateById
	* @Description: TODO  
	* @return Plate  
	* @author 刘强   
	* @date 2015-4-28 下午5:53:39
	*/ 
	public Plate getPlateById(Long id);
	/**
	* @Title: getPlateByPlateId
	* @Description: 获取plateid所属plate的相同themeid的plate
	* @return List<Plate>  
	* @author 刘强   
	* @date 2015-5-5 下午9:16:38
	*/ 
	public List<Plate> getPlateByPlateId(long plateid);
	/**
	* @Title: getPlate
	* @Description: TODO  
	* @return List<Plate>  
	* @author 刘强   
	* @date 2015-5-18 上午10:05:37
	*/ 
	public List<Plate> getPlate(int currentPage, int pageSize, String name, long themeId, String order, String sort);
	/**
	* @Title: getPlateTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-18 上午10:06:08
	*/ 
	public int getPlateTotal(String name, long themeId);
	/**
	* @Title: delete
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 上午10:28:33
	*/ 
	public void delete(long id);
	/**
	* @Title: update
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午12:47:42
	*/ 
	public void update(Plate plate);
	/**
	* @Title: save
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午12:47:44
	*/ 
	public void save(Plate plate);
}
