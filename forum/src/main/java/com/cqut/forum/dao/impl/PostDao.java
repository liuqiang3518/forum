package com.cqut.forum.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IPostDao;
import com.cqut.forum.entity.Hotpost;
import com.cqut.forum.entity.Post;
import com.cqut.forum.util.DefaultElements;
import com.cqut.forum.util.StringUtil;
@Repository
public class PostDao extends BaseDao implements IPostDao {

	public List<Hotpost> getHotPostByType(int type,int size) {
		List<Hotpost> hotposts=null;
		StringBuilder hql=new StringBuilder("select h from Hotpost h where ");
		hql.append(" h.type =").append(type);
		hotposts=super.list(hql.toString(), null, 0, size, null);
		return hotposts;
	}
	public List<Post> getPostByPlateId(long plateId,int pageIndex,int size){
		List<Post> posts=null;
		StringBuilder hql=new StringBuilder("select p from Post p where ");
		hql.append("p.plateId =").append(plateId)
		.append(" and p.isfrozen =0 ")
		.append(" order by p.updateDate desc,p.replyNumber desc");
		posts=super.list(hql.toString(), null, pageIndex-1, size, null);
		return posts;
	}

	public int getPostTotalByPlateId(long plateId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Post p where ");
		hql.append("p.plateId =").append(plateId).append(" and p.isfrozen =0 ");
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Post> getPostByHotpost(int type, int size) {
		List<Post> posts=null;
		StringBuilder hql=new StringBuilder("select p from Hotpost h, Post p where ");
		hql.append(" h.postId =p.id and ");
		hql.append(" h.type =").append(type);
		posts=super.list(hql.toString(), null, 0, size, null);
		return posts;
	}

	public Post getPostById(Long id) {
		Post post =null;
		Object obj=super.get(Post.class, id);
		if(null!=obj){
			post=(Post)obj;
		}
		return post;
	}

	public List<Post> getAll() {
		List<Post> posts=null;
		StringBuilder hql=new StringBuilder("from Post");
		posts=super.list(hql.toString(), null);
		return posts;
	}

	public List<Post> getPostByUserId(long userId ,int pageIndex,int size) {
		List<Post> lists=new ArrayList<Post>();
		StringBuilder hql=new StringBuilder("select p from Post where  ");
		hql.append(" p.userId =").append(userId);
		lists=super.list(hql.toString(), null, pageIndex-1, size, null);
		return lists;
	}

	public int getPostTotalByUserId(long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Post p where ");
		hql.append("p.userId =").append(userId);
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Post> getPost(String title, String userName, String starTime,
			String endTime, int currentPage, int pageSize, String order,String sort) {
		List<Post> list=new ArrayList<Post>();
		if("".equals(sort)) sort= "postDate";
		if("".equals(order)) order="desc";
		StringBuilder hql=new StringBuilder("select p from Post p where  1=1");
		if(!"".equals(title))
	 	    hql.append(" and p.name like '%").append(title).append("%' ");
		if(!"".equals(userName))
	 	    hql.append(" and p.userName like '%").append(userName).append("%' ");
		if(!StringUtil.isEmpty(starTime))
			hql.append(" and p.postDate >='").append(starTime).append("' ");
		if(!StringUtil.isEmpty(endTime))
			hql.append("and p.postDate <='").append(endTime).append("' ");
		hql.append(" order by ").append(sort).append(" ").append(order);
		list=super.list(hql.toString(),null, currentPage-1, pageSize, null);
		return list;
	}

	public int getPostTotal(String title, String userName, String starTime,String endTime) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Post p where  1=1");
		if(!"".equals(title))
	 	    hql.append(" and p.name like '%").append(title).append("%' ");
		if(!"".equals(userName))
	 	    hql.append(" and p.userName like '%").append(userName).append("%' ");
		if(!StringUtil.isEmpty(starTime))
			hql.append(" and p.postDate >='").append(starTime).append("' ");
		if(!StringUtil.isEmpty(endTime))
			hql.append("and p.postDate <='").append(endTime).append("' ");
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Hotpost> getHotPost(String title, int currentPage,
			int pageSize, String order, String sort) {
		if("".equals(sort)) sort= "h.id";
		if("".equals(order)) order="desc";
		List<Hotpost> list=new ArrayList<Hotpost>();
		StringBuilder hql=new StringBuilder("select new Hotpost(h.id,h.postId,h.type,h.imagePath,h.data,p.name) from Post p ,Hotpost h where  p.id=h.postId");
		hql.append(" and p.name like '%").append(title).append("%' ")
		.append(" order by h.").append(sort).append(" ").append(order);
		list=super.list(hql.toString(),null, currentPage-1, pageSize, null);
		return list;
	}

	public int getHotPostTotal(String title) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Post p ,Hotpost h where  p.id=h.postId");
		hql.append(" and p.name like '%").append(title).append("%' ");
		result=super.count(hql.toString(), null);
		return result;
	}
    

}
