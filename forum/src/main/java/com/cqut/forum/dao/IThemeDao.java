package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Admin;
import com.cqut.forum.entity.Theme;

public interface IThemeDao extends IBaseDao{
	/**
	* @Title: getAdmin
	* @Description: TODO  
	* @return List<Admin>  
	* @author 刘强   
	* @date 2015-5-17 下午4:28:16
	*/ 
	public List<Theme> getTheme(int currentPage, int pageSize, String name, String order, String sort);
	/**
	* @Title: getAdminTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-17 下午4:34:28
	*/ 
	public int getThemeTotal(String name);
	/**
	* @Title: getThemes
	* @Description: TODO  
	* @return List<Theme>  
	* @author 刘强   
	* @date 2015-5-18 上午10:39:30
	*/ 
	public List<Theme> getThemes();
	
	/**
	* @Title: commentNumByThemeId
	* @Description: 统计主题下的帖子评论量  
	* @return int  
	* @author 刘强   
	* @date 2015-6-6 上午2:40:43
	*/ 
	public Long commentNumByThemeId(Long id);
}
