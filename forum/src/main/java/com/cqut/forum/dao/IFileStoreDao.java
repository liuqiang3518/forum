package com.cqut.forum.dao;

import com.cqut.forum.entity.Filestore;

public interface IFileStoreDao extends IBaseDao {

	/**
	* @Title: save
	* @Description: 保存文件并返回MD5值  
	* @return String void 
	* @author 刘强   
	* @date 2015-5-14 上午11:34:38
	*/ 
	public void save(Filestore filestore);
	/**
	* @Title: getFilestoreByMD5
	* @Description: TODO  
	* @return Filestore  
	* @author 刘强   
	* @date 2015-5-14 下午5:12:02
	*/ 
	public Filestore getFilestoreByMD5(String MD5);
}
