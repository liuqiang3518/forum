package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Hotpost;
import com.cqut.forum.entity.Post;

public interface IPostDao extends IBaseDao {



	/**
	* @Title: getHotPostByType
	* @Description: TODO  
	* @return List<Hotpost>  
	* @author 刘强   
	* @date 2015-4-27 上午11:14:27
	*/ 
	public List<Hotpost> getHotPostByType(int type, int size);
	/**
	* @Title: getPostByHotpost
	* @Description: TODO  
	* @return List<Post>  
	* @author 刘强   
	* @date 2015-4-28 下午9:28:14
	*/ 
	public List<Post> getPostByHotpost(int type, int size);
	/**
	* @Title: getPostByPlateId
	* @Description: TODO  
	* @return List<Post>  
	* @author 刘强   
	* @date 2015-4-27 下午6:49:06
	*/ 
	public List<Post> getPostByPlateId(long plateId, int pageIndex, int size);
	/**
	* @Title: getPostTotalByPlateId
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-4-28 下午4:41:52
	*/ 
	public int getPostTotalByPlateId(long plateId);
	/**
	* @Title: getPostById
	* @Description: TODO  
	* @return Post  
	* @author 刘强   
	* @date 2015-4-30 下午4:43:07
	*/ 
	public Post getPostById(Long id);
	/**
	* @Title: getAll
	* @Description: 测试方法 
	* @return List<Post>  
	* @author 刘强   
	* @date 2015-5-8 下午9:21:20
	*/ 
	public List<Post> getAll();
	/**
	* @Title: getPostByUserId
	* @Description: TODO  
	* @return List<Post>  
	* @author 刘强   
	* @date 2015-5-12 下午2:45:24
	*/ 
	public List<Post> getPostByUserId(long userId, int pageIndex, int size);
	/**
	* @Title: getPostTotalByUserId
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-12 下午2:49:57
	*/ 
	public int getPostTotalByUserId(long userId);
	
	/**
	* @Title: getPost
	* @Description: TODO  
	* @return List<Post>  
	* @author 刘强   
	* @date 2015-5-19 上午10:59:35
	*/ 
	public List<Post> getPost(String title, String userName, String starTime, String endTime, int currentPage, int pageSize, String order, String sort);
	/**
	* @Title: getPostTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-19 上午10:59:37
	*/ 
	public int getPostTotal(String title, String userName, String starTime, String endTime);

	/**
	* @Title: getPostForHotPost
	* @Description: type 0,1  
	* @return List<Post>  
	* @author 刘强   
	* @date 2015-5-19 下午3:59:56
	*/ 
	public List<Hotpost> getHotPost(String title, int currentPage, int pageSize, String order, String sort);
	/**
	* @Title: getPostForHotPost
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-19 下午4:00:09
	*/ 
	public int getHotPostTotal(String title);
}
