package com.cqut.forum.dao.impl;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IFileStoreDao;
import com.cqut.forum.entity.Filestore;
@Repository
public class FileStoreDao extends BaseDao implements IFileStoreDao {

	public void save(Filestore filestore) {
		super.save(filestore);
	}

	public Filestore getFilestoreByMD5(String MD5) {
		Filestore filestore=null;
		StringBuilder hql=new StringBuilder("select f from Filestore f where ");
		hql.append(" f.filemd5 ='").append(MD5).append("' ");
		filestore=(Filestore) super.unique(hql.toString(), null);
		return filestore;
	}

}
