package com.cqut.forum.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IAdminDao;
import com.cqut.forum.entity.Admin;
@Repository
public class AdminDao extends BaseDao implements IAdminDao {

	public Admin getAdmin(String name, String password) {
		Admin admin=null;
		StringBuilder hql=new StringBuilder("select a from Admin a where ");
		hql.append(" a.userName ='").append(name).append("' ")
		.append(" and a.password ='").append(password).append("' ");
		admin=(Admin)super.unique(hql.toString(), null);
		return admin;
	}

	public List<Admin> getAdmin(int currentPage, int pageSize, String name,
			String order, String sort) {
		List<Admin> list=new ArrayList<Admin>();
		if("".equals(sort)) sort= "id";
		if("".equals(order)) order="desc";
		StringBuilder hql=new StringBuilder("select a from Admin a where  1=1");
		if(!"".equals(name)){
			hql.append(" and a.userName like '%").append(name+"%' ");
		}
		hql.append(" order by ").append(sort).append(" ").append(order);
		list=super.list(hql.toString(), null, currentPage-1, pageSize, null);
		return list;
	}

	public int getAdminTotal(String name) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Admin a where  1=1");
		if(!"".equals(name)){
			hql.append(" and a.userName like '%").append(name+"%' ");
		}
		result=super.count(hql.toString(), null);
		return result;
	}

	public Admin getAdminByUserName(String userName) {
		Admin admin=null;
		StringBuilder hql=new StringBuilder("select a from Admin a where");
		hql.append(" a.userName ='").append(userName).append("'");
		admin=(Admin)super.unique(hql.toString(), null);
		return admin;
	}


}
