package com.cqut.forum.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IUserDao;
import com.cqut.forum.entity.User;
import com.cqut.forum.entity.Userinfo;
import com.cqut.forum.util.DateUtil;
import com.cqut.forum.util.StringUtil;

/**
 * @author 刘强
 *
 */
@Repository
public class UserDao extends BaseDao implements IUserDao {
	
	public User getUserByEmailAndPassword(String email,String password){
		User user=null;
		StringBuilder hql=new StringBuilder("select u from User u where ");
		hql.append("u.email = '").append(email).append(" '");
		hql.append(" and u.password = '").append(password).append(" '");
		Object obj=super.unique(hql.toString(), null);
		if(null!=obj){
			user=(User)obj;
		}
		return user;
	}

	public User getUserByUserIdAndPassword(String studentId,String password){
		User user=null;
		StringBuilder hql=new StringBuilder("select u from User u where ");
		hql.append("u.studentId = '").append(studentId).append(" '");
		hql.append(" and u.password = '").append(password).append(" '");
		Object obj=super.unique(hql.toString(), null);
		if(null!=obj){
			user=(User)obj;
		}
		return user;		
	}
	public boolean isExeistEmail(String email){
		User user=null;
		StringBuilder hql=new StringBuilder("select u from User u where ");
		hql.append(" u.email = '").append(email).append("' ");
		Object obj=super.unique(hql.toString(), null);
		if(null!=obj){
			user=(User)obj;
		}
		if(null!=user){
			return true;
		}else{
			return false;
		}
	}

	public User getUserByUserId(long userId) {
		User user=null;
		StringBuilder hql=new StringBuilder(" select u from User u where ");
		hql.append(" u.userId =").append(userId);
		Object obj=super.unique(hql.toString(), null);
		if(null!=obj){
			user=(User)obj;
		}
		return user;
	}
	public User getUserByEmail(String email){
		User user=null;
		StringBuilder hql=new StringBuilder(" select u from User u where ");
		hql.append(" u.email = '").append(email).append("'");
		Object obj=super.unique(hql.toString(), null);
		if(null!=obj){
			user=(User)obj;
		}
		return user;		
	}

	public User getUserById(long id) {
		return super.get(User.class, id);
	}

	public List<User> getUser(String name, String academyId, String starTime,
			String endTime, String role,String isfrozen,String email,int currentPage, int pageSize, 
			String order, String sort) {
		if("".equals(sort)) sort= "createDate";
		if("".equals(order)) order="desc";
		StringBuilder hql=new StringBuilder("select u from User u where  1=1");
		if(!StringUtil.isEmpty(name))
			hql.append(" and u.userName like '%").append(name).append("%' ");
		if(!StringUtil.isEmpty(academyId))
			hql.append(" and u.academyId =").append(Long.parseLong(academyId)).append(" ");
		if(!StringUtil.isEmpty(role))
			hql.append(" and u.role =").append(Integer.parseInt(role)).append(" ");
		if(!StringUtil.isEmpty(isfrozen))
			hql.append(" and u.isfrozen =").append(Integer.parseInt(isfrozen)).append(" ");
		if(!StringUtil.isEmpty(email))
			hql.append(" and u.email like '%").append(email).append("%' ");
		if(!StringUtil.isEmpty(starTime))
			hql.append(" and u.createDate >='").append(starTime).append("' ");
		if(!StringUtil.isEmpty(endTime))
			hql.append("and u.createDate <='").append(endTime).append("' ");
		hql.append(" order by ").append(sort).append(" ").append(order);
		List<User> list=new ArrayList<User>();
		list=super.list(hql.toString(),null, currentPage-1, pageSize, null);
		return list;
	}

	public int getUserTotal(String name, String academyId, String starTime,
			String endTime,String role,String isfrozen,String email) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from User u where  1=1");
		if(!StringUtil.isEmpty(name))
			hql.append(" and u.userName like '%").append(name).append("%' ");
		if(!StringUtil.isEmpty(academyId))
			hql.append(" and u.academyId =").append(Long.parseLong(academyId)).append(" ");
		if(!StringUtil.isEmpty(role))
			hql.append(" and u.role =").append(Integer.parseInt(role)).append(" ");
		if(!StringUtil.isEmpty(isfrozen))
			hql.append(" and u.isfrozen =").append(Integer.parseInt(isfrozen)).append(" ");
		if(!StringUtil.isEmpty(email))
			hql.append(" and u.email like '%").append(email).append("%' ");
		if(!StringUtil.isEmpty(starTime))
			hql.append(" and u.createDate >='").append(starTime).append("' ");
		if(!StringUtil.isEmpty(endTime))
			hql.append("and u.createDate <='").append(endTime).append("' ");
		result=super.count(hql.toString(), null);
		return result;
	}

}
