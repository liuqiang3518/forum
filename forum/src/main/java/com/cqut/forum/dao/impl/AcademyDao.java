package com.cqut.forum.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IAcademyDao;
import com.cqut.forum.entity.Academy;
@Repository
public class AcademyDao extends BaseDao implements IAcademyDao{

	public Academy getAcademy(long id) {
		return super.get(Academy.class, id);
	}

	public List<Academy> getAcademys() {
		return super.list(Academy.class);
	}

}
