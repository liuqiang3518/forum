package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Academy;

public interface IAcademyDao extends IBaseDao{
	
	/**
	* @Title: getAcademy
	* @Description: 根据id获取学院实体  
	* @return Academy  
	* @throws
	* @author 刘强   
	* @date 2015-4-24 下午4:01:32
	*/ 
	public Academy getAcademy(long id);
	/**
	* @Title: getAcademys
	* @Description: 获取所有学院实体  
	* @return List<Academy>  
	* @throws
	* @author 刘强   
	* @date 2015-4-24 下午4:02:02
	*/ 
	public List<Academy> getAcademys();

}
