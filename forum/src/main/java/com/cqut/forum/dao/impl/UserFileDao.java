package com.cqut.forum.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.asn1.esf.SPuri;
import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IUserFileDao;
import com.cqut.forum.entity.Filestore;
import com.cqut.forum.entity.Userfile;
@Repository
public class UserFileDao extends BaseDao implements IUserFileDao{

	public List<Userfile> getUserfile(long userId, Long parentId,
			int indexpage, int size) {
		List<Userfile> lists=null;
		StringBuilder hql=new StringBuilder("select u from Userfile u where ");
		hql.append(" u.userId =").append(userId).append(" and u.parentId =").append(parentId)
		.append(" order by u.isFile asc, u.createDate desc");
		lists=super.list(hql.toString(), null, indexpage-1, size, null);
		return lists;
	}

	public int getUserfileTotal(long userId, Long parentId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Userfile u where ");
		hql.append(" u.userId =").append(userId).append(" and u.parentId =").append(parentId);
		result=super.count(hql.toString(),null);
		return result;
	}

	public Userfile getUserfileByUserIdAndFileName(long userId,String fileName) {
	    Userfile userfile=null;
		StringBuilder hql=new StringBuilder("select u from Userfile u where ");
		hql.append(" u.fileName ='").append(fileName).append("' ")
		.append(" and u.userId =").append(userId);
		userfile=(Userfile)super.unique(hql.toString(), null);
		return userfile;
	}

	public int getUserfileChildNum(long parentId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Userfile u where");
		hql.append(" u.parentId =").append(parentId);
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Userfile> getUserfileByParentId(long parentId) {
		List<Userfile> lists=null;
		StringBuilder hql=new StringBuilder("select u from Userfile u where ");
		hql.append(" u.parentId =").append(parentId);
		lists=super.list(hql.toString(),null);
		return lists;
	}

	public List<Userfile> getUserfileByUserIdAndisFile(Long userId, int isFile) {
		List<Userfile> lists=null;
		StringBuilder hql=new StringBuilder("select u from Userfile u where ");
		hql.append(" u.userId =").append(userId)
		.append(" and u.isFile =").append(isFile);
		lists=super.list(hql.toString(),null);
		return lists;
	}

	public void updateUserFile(Userfile userfile) {
		 super.update(userfile);
	}

	public List<Userfile> getUserfileByUserIdAndfileType(long userId,
			int fileType,int indexpage, int size) {
		List<Userfile> lists=null;
		StringBuilder hql=new StringBuilder("select u from Userfile u, Filestore f where ");
		hql.append("u.md5 = f.filemd5").append(" and f.filetype =").append(fileType)
		.append(" and u.userId =").append(userId).append(" and u.isFile =").append(1)
		.append(" order by  u.createDate desc");
		lists=super.list(hql.toString(), null, indexpage-1, size, null);
		return lists;
	}

	public int getUserfileTotalByUserIdAndfileType(long userId, int fileType) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(u) from Userfile u, Filestore f where ");
		hql.append("u.md5 = f.filemd5").append(" and f.filetype =").append(fileType)
		.append(" and u.userId =").append(userId).append(" and u.isFile =").append(1);
		result=super.count(hql.toString(), null);
		return result;
	}
}
