package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Comment;
import com.cqut.forum.entity.Post;

public interface ICommentDao extends IBaseDao {

	/**
	* @Title: getComment
	* @Description:   
	* @return List<Comment>  
	* @author 刘强   
	* @date 2015-4-30 下午3:33:40
	*/ 
	public List<Comment> getComment(Long postId, int pageIndex, int size);
	/**
	* @Title: getCommentTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-4-30 下午4:25:40
	*/ 
	public int getCommentTotal(Long postId);
	/**
	* @Title: getComment
	* @Description: TODO  
	* @return List<Comment>  
	* @author 刘强   
	* @date 2015-4-30 下午3:34:56
	*/ 
	public List<Comment> getComment(Long postId, Long targetId);
	
	/**
	* @Title: get
	* @Description: TODO  
	* @return Comment  
	* @author 刘强   
	* @date 2015-5-6 下午2:25:25
	*/ 
	public Comment get(Long id);
	/**
	* @Title: getPostByCommentId
	* @Description: 获取id 所属comment的所对应的post  
	* @return Post  
	* @author 刘强   
	* @date 2015-5-6 下午2:28:50
	*/ 
	public Post getPostByCommentId(long id);
	/**
	* @Title: getCoomentByTargetId
	* @Description: TODO  
	* @return List<Comment>  
	* @author 刘强   
	* @date 2015-5-6 下午3:54:16
	*/ 
	public List<Comment> getCoomentByTargetId(long targetId);
	/**
	* @Title: getCommentByUserId
	* @Description: TODO  
	* @return List<Comment>  
	* @author 刘强   
	* @date 2015-5-12 下午3:13:53
	*/ 
	public List<Comment> getCommentByUserId(Long userId, int pageIndex, int size);
	/**
	* @Title: getCommentTotalByUserId
	* @Description: TODO  
	* @return List<Comment>  
	* @author 刘强   
	* @date 2015-5-12 下午3:14:15
	*/ 
	public Integer getCommentTotalByUserId(Long userId);
}
