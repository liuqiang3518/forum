package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Userfile;

public interface IUserFileDao extends IBaseDao {

	/**
	* @Title: getUserfile
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-14 下午7:23:06
	*/ 
	public List<Userfile> getUserfile(long userId, Long parentId, int indexpage, int size);
	/**
	* @Title: getUserfileTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-14 下午7:23:35
	*/ 
	public int getUserfileTotal(long userId, Long parentId);
	/**
	* @Title: getUserfileByMd5
	* @Description: TODO  
	* @return Userfile  
	* @author 刘强   
	* @date 2015-5-15 上午11:28:44
	*/ 
	public Userfile getUserfileByUserIdAndFileName(long userId, String fileName);
	/**
	* @Title: getUserfileChildNum
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-15 下午3:15:15
	*/ 
	public int getUserfileChildNum(long parentId);
	/**
	* @Title: getUserfileByParentId
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-15 下午3:20:49
	*/ 
	public List<Userfile> getUserfileByParentId(long parentId);
	/**
	* @Title: getUserfileByUserIdAndisFile
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-15 下午8:06:01
	*/ 
	public List<Userfile> getUserfileByUserIdAndisFile(Long userId, int isFile);
	/**
	* @Title: Update
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-15 下午2:14:31
	*/ 
	public void updateUserFile(Userfile userfile);
	/**
	* @Title: getUserfileByUserIdAndfileType
	* @Description: TODO  
	* @return List<Userfile>  
	* @author 刘强   
	* @date 2015-5-16 上午9:28:35
	*/ 
	public List<Userfile> getUserfileByUserIdAndfileType(long userId, int fileType, int indexpage, int size);
	/**
	* @Title: getUserfileTotalByUserIdAndfileType
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-16 上午9:44:34
	*/ 
	public int getUserfileTotalByUserIdAndfileType(long userId, int fileType);
}
