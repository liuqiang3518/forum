package com.cqut.forum.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.ICommentDao;
import com.cqut.forum.entity.Comment;
import com.cqut.forum.entity.Post;
@Repository
public class CommentDao extends BaseDao implements ICommentDao {

	public List<Comment> getComment(Long postId,int pageIndex,int size) {
		List<Comment> comments=null;
		StringBuilder hql=new StringBuilder("select c from Comment c where ");
		hql.append(" c.postId =").append(postId)
		.append(" and c.targetId=0").append(" order by c.date asc");
		comments=super.list(hql.toString(), null, pageIndex-1, size,null);
		return comments;
	}
	public int getCommentTotal(Long postId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Comment c where ");
		hql.append(" c.postId =").append(postId).append(" and c.targetId=0");
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Comment> getComment(Long postId, Long targetId) {
		List<Comment> comments=null;
		StringBuilder hql=new StringBuilder("select c from Comment c where ");
		hql.append(" c.postId =").append(postId)
		.append(" and c.targetId =").append(targetId)
		.append(" order by c.date asc");
		comments=super.list(hql.toString(), null);
		return comments;
	}
	public Comment get(Long id) {
		return super.get(Comment.class, id);
	}
	public Post getPostByCommentId(long id) {
		Comment comment=super.get(Comment.class, id);
		return super.get(Post.class, comment.getPostId());
	}
	public List<Comment> getCoomentByTargetId(long targetId) {
		List<Comment> comments=null;
		StringBuilder hql=new StringBuilder("select c from Comment c where ");
		hql.append(" c.targetId =").append(targetId)
		.append(" order by c.date asc");
		comments=super.list(hql.toString(), null);
		return comments;
	}

	public List<Comment> getCommentByUserId(Long userId, int pageIndex, int size) {
		List<Comment> comments=null;
		StringBuilder hql=new StringBuilder("select c from Comment c where ");
		hql.append(" c.userId =").append(userId).append(" order by c.date asc");
		comments=super.list(hql.toString(), null, pageIndex-1, size,null);
		return comments;
	}

	public Integer getCommentTotalByUserId(Long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Comment c where ");
		hql.append(" c.userId =").append(userId);
		result=super.count(hql.toString(), null);
		return result;
	}


}
