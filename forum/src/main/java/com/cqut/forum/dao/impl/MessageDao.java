package com.cqut.forum.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IMessageDao;
import com.cqut.forum.entity.Message;
import com.cqut.forum.util.DefaultElements;
@Repository
public class MessageDao extends BaseDao implements IMessageDao {

	public void saveMessage(Message message) {
		super.saveOrUpdate(message);
		
	}

	public List<Message> getPostMessage(long userId, int indexpage, int size) {
		List<Message> lists=new ArrayList<Message>();
		StringBuilder hql=new StringBuilder("select m from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.type =").append(DefaultElements.ZERO)
		.append(" order by m.isRead asc, m.createDate desc");
		lists=super.list(hql.toString(), null, indexpage-1, size, null);
		return lists;
	}

	public int getPostMessageTotal(long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.type =").append(DefaultElements.ZERO);
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Message> getSystemMessage(long userId, int indexpage, int size) {
		List<Message> lists=new ArrayList<Message>();
		StringBuilder hql=new StringBuilder("select m from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.type =").append(DefaultElements.ONE)
		.append(" order by m.isRead asc, m.createDate desc");
		lists=super.list(hql.toString(), null, indexpage-1, size, null);
		return lists;
	}

	public int getSystemMessageTotal(long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.type =").append(DefaultElements.ONE);
		result=super.count(hql.toString(), null);
		return result;
	}

	public int getMessageNum(long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.isRead =").append(DefaultElements.ZERO);
		result=super.count(hql.toString(), null);
		return result;
	}

	public int getPostMessageNum(long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.isRead =").append(DefaultElements.ZERO)
		.append(" and m.type =").append(DefaultElements.ZERO);
		result=super.count(hql.toString(), null);
		return result;
	}

	public int getSystemMessageNum(long userId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Message m where ");
		hql.append(" m.userId =").append(userId)
		.append(" and m.isRead =").append(DefaultElements.ZERO)
		.append(" and m.type =").append(DefaultElements.ONE);
		result=super.count(hql.toString(), null);
		return result;
	}

}
