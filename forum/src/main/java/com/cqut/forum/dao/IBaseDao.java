package com.cqut.forum.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;



/**
 * dao层的共有父类，提供了常用的数据库操作方法。</br>
 * @author 刘强
 *
 */

public interface IBaseDao{

	/**
	 * 根据主键，返回实体
	 * @param id 主键
	 * @return	实体或空（如果没有查到）
	 */
	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> entityClass, Serializable id);
	
	
	/**
	 * 保存实体对象，返回主键
	 * @param model
	 * @return
	 */
	public Serializable save(Object model) ;
	
	/**
	 * 更新实体对象，不返回值
	 * @param entity
	 */
	public void update(Object model);
	
	/**
	 * 执行更新或插入语句，返回受影响的行数
	 * @param sql	纯sql语句
	 * @param param
	 * @return
	 */
	public int update(String sql, Object[] param);
	
	
	/**
	 * 删除指定对象，不返回值
	 * @param model
	 */
	public void delete(Object model);
	
	/**
	 * 删除对象列表，不返回值
	 * @param list
	 */
	public void deleteAll(List list);
	
	/**
	 * 删除对象列表，不返回值
	 * @param objectArray
	 */
	public void deleteAll(Object[] objectArray);
	/**
	 * 根据主键删除
	 * @param id
	 */
	public void delete(Class<?> entityClass, Serializable id);
	
	/**
	 * 保存或更新
	 * @param model 实体类
	 */
    public void saveOrUpdate(Object model);
    
	
    /**
     * 根据查询条件返回唯一一条记录
     * 如果查询出多条，返回第一条
     * @param hql
     * @param param
     * @return 如果查不到东西返回null
     */
    public Object unique(final String hql, final Object[] param) ;
    
    /**
     * 
     * @param hql	普通hql语句
     * @param param	参数
     * @return
     */
	@SuppressWarnings("rawtypes")
	public List list(String hql, Object[] param);
    
    /**
     * 分页查询方法
     * @param entity	任意实体
     * @param start		分页起始数，第1页应为0
     * @param size		每个分页大小
     * @param order		排序语句,本方法会自动加上" order by "关键字
     * @return			封装有所查询实体类的列表,可能返回null
     */
	@SuppressWarnings("unchecked")
	public <T>List<T> list(Class<T> entity, int start, int size, String order);
    
	
	 /**
     * 分页查询
     * @param entity	任意实体
     * @param condition		查询条件
     * @param param		参数
     * @return			封装有所查询实体类的列表,可能返回null
     */
	@SuppressWarnings("unchecked")
	public <T>List<T> list(Class<T> entity, String condition, Object[] param);
	
	
    /**
     * 分页查询
     * @param entity	任意实体
     * @param condition		查询条件
     * @param param		参数
     * @param start		分页起始数，第1页应为0
     * @param size		每个分页大小
     * @param order		排序语句,本方法会自动加上" order by "关键字
     * @return			封装有所查询实体类的列表,可能返回null
     */
	@SuppressWarnings("unchecked")
	public <T>List<T> list(Class<T> entity, String condition, Object[] param, int start, int size, String order);
    
	/**
	 * 按照查询条件，查出统计数
	 * @param entity
	 * @param condition	查询条件，可为空
	 * @param param
	 * @return
	 */
	public int count(Class<?> entity, String condition, Object[] param);
    
    
    
    
    /**
     * 返回个数
     * @param entity	任意实体
     * @return
     */
	public int count(Class<?> entity);
    
    /**
     * 返回个数
     * @param hql   符合HQL语法的count语句
     * @param param	参数
     * @return		查询的结果,如果出错,返回0
     */
    public int count(String hql, Object[] param);
    
    
    
    /**
     * 分页查询方法
     * @param hql	要求为hql语句
     * @param param	参数
     * @param start	分页起始数，第1页应为0
     * @param size	每个分页大小
     * @param order	排序语句,本方法会自动加上" order by "关键字
     * @return		封装有所查询实体类的列表,可能返回null
     */
	@SuppressWarnings("rawtypes")
	public List list(String hql, Object[] param, int start, int size, String order);
    
	
	@SuppressWarnings("rawtypes")
	public List criteriaList(Criteria criteria, int start, int size, String order, List<Integer> schoolIdList);
	
    /**
     * 纯sql的分页
     * @param sql		纯sql语句
     * @param param		参数
     * @param start		分页起始数
     * @param size		每页个数
     * @param order		排序语句,本方法会自动加上" order by "关键字
     * @return			以Map<String,Object>组成的列表,可能返回null
     */
    public List<Map<String,Object>> listByNative(String sql, Object[] param, int start, int size, String order);
    
    /**
     * 纯sql的count方法
     * @param sql		纯sql语句
     * @param param		参数
     * @return			正常的个数,如果出错或为空,返回0
     */
    public int countByNative(String sql, Object[] param);
    
    
    /**
     * 把普通sql语句,处理成分页语句</br>
     * @param sql		纯sql语句
     * @param start		起始页码
     * @param size		每页个数
     * @return
     */
    public String getLimitString(String sql, int start, int size);
    
    
    /**
     * 删除操作
     * @param hql	HQL语句
     * @param param
     */
    public void delete(final String hql, Object[] param);
    
    
    
    /**
     * 查出所有相关实体的信息
     * @param entityClass
     * @return
     */
	@SuppressWarnings("unchecked")
	public <T> List<T> list(Class<T> entityClass);
	
	public int execte(final String hql, final Object... paramlist);
	
	/**
	 * 批量更新多个实体。
	 * @param list
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void batchUpdate(List list);
	
}
