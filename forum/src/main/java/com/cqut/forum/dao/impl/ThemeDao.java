package com.cqut.forum.dao.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IThemeDao;
import com.cqut.forum.entity.Theme;
@Repository
public class ThemeDao extends BaseDao implements IThemeDao{

	public List<Theme> getTheme(int currentPage, int pageSize, String name,
			String order, String sort) {
		if("".equals(sort)) sort= "id";
		if("".equals(order)) order="desc";
		List<Theme> list=new ArrayList<Theme>();
		StringBuilder hql=new StringBuilder("select t from Theme t where  1=1");
		if(!"".equals(name)){
			hql.append(" and t.themeName like '%").append(name+"%' ");
		}
		hql.append(" order by ").append(sort).append(" ").append(order);
		list=super.list(hql.toString(),null, currentPage-1, pageSize, null);
		return list;
	}

	public int getThemeTotal(String name) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Theme t where  1=1");
		if(!"".equals(name)){
			hql.append(" and t.themeName like '%").append(name+"%' ");
		}
		result=super.count(hql.toString(), null);
		return result;
	}

	public List<Theme> getThemes() {
		List<Theme> list=new ArrayList<Theme>();
		String hql=" select t from Theme t";
		list=super.list(hql, null);
		return list;
	}

	public Long commentNumByThemeId(Long id) {
		Long result=(long)0;
		StringBuilder hql=new StringBuilder("select sum(p.replyNumber) from Post p, Plate l where p.plateId=l.id and ");
		hql.append(" l.themeId = ").append(id);
		result=(Long)super.unique(hql.toString(), null);
		return result;
	}



}
