package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Message;

public interface IMessageDao extends IBaseDao {

	/**
	* @Title: saveMessage
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-16 下午8:18:43
	*/ 
	public void saveMessage(Message message);
	/**
	* @Title: getMessage
	* @Description: TODO  
	* @return List<Message>  
	* @author 刘强   
	* @date 2015-5-16 下午8:19:47
	*/ 
	public List<Message> getPostMessage(long userId, int indexpage, int size);
	/**
	* @Title: getMessageTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-16 下午8:27:06
	*/ 
	public int getPostMessageTotal(long userId);
	/**
	* @Title: getSystemMessage
	* @Description: TODO  
	* @return List<Message>  
	* @author 刘强   
	* @date 2015-5-16 下午9:06:24
	*/ 
	public List<Message> getSystemMessage(long userId, int indexpage, int size);
	/**
	* @Title: getSystemMessageTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-16 下午9:06:27
	*/ 
	public int getSystemMessageTotal(long userId);
	/**
	* @Title: getMessageNum
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-16 下午10:13:51
	*/ 
	public int getMessageNum(long userId);
	/**
	* @Title: getPostMessageNum
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-17 上午1:00:39
	*/ 
	public int getPostMessageNum(long userId);
	/**
	* @Title: getSystemMessageNum
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-17 上午1:00:42
	*/ 
	public int getSystemMessageNum(long userId);
}
