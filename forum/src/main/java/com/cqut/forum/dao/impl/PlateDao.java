package com.cqut.forum.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer.ReuseStrategy;
import org.springframework.stereotype.Repository;

import com.cqut.forum.dao.IPlateDao;
import com.cqut.forum.entity.Plate;
import com.cqut.forum.entity.Theme;
@Repository
public class PlateDao extends BaseDao implements IPlateDao {

	public List<Plate> getPlatesByTheme(Long themeId) {
		List<Plate> plates=null;
		StringBuilder hql=new StringBuilder("select p from Plate p where");
		hql.append(" p.themeId = ").append(themeId);
		plates=super.list(hql.toString(), null);
		return plates;
	}

	public Plate getPlateById(Long id) {
		Plate plate=super.get(Plate.class, id);
		return plate;
	}

	public List<Plate> getPlateByPlateId(long plateid){
		Plate palte=getPlateById(plateid);
		return getPlatesByTheme(palte.getThemeId());
	}

	public List<Plate> getPlate(int currentPage, int pageSize, String name,
			long themeId, String order, String sort) {
		if("".equals(sort)) sort= "id";
		if("".equals(order)) order="desc";
		List<Plate> list=new ArrayList<Plate>();
		StringBuilder hql=new StringBuilder("select p from Plate p where  1=1");
		if(!"".equals(name))
	 	    hql.append(" and p.plateName like '%").append(name).append("%' ");
		if(themeId!=0)
			hql.append(" and p.themeId =").append(themeId);
		hql.append(" order by ").append(sort).append(" ").append(order);
		list=super.list(hql.toString(),null, currentPage-1, pageSize, null);
		return list;
	}

	public int getPlateTotal(String name, long themeId) {
		int result=0;
		StringBuilder hql=new StringBuilder("select count(*) from Plate p where  1=1");
		if(!"".equals(name))
	 	    hql.append(" and p.plateName like '%").append(name).append("%' ");
		if(themeId!=0)
			hql.append(" and p.themeId =").append(themeId);
		result=super.count(hql.toString(),null);
		return result;
	}

}
