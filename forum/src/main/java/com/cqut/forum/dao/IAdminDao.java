package com.cqut.forum.dao;

import java.util.List;

import com.cqut.forum.entity.Admin;

public interface IAdminDao extends IBaseDao {
	/**
	* @Title: getAdmin
	* @Description: 根据用户名密码获取admin  
	* @return Admin  
	* @author 刘强   
	* @date 2015-5-17 上午10:35:18
	*/ 
	public Admin getAdmin(String name, String password);
	
	/**
	* @Title: getAdmin
	* @Description: TODO  
	* @return List<Admin>  
	* @author 刘强   
	* @date 2015-5-18 下午11:37:25
	*/ 
	public List<Admin> getAdmin(int currentPage, int pageSize, String name, String order, String sort);
	/**
	* @Title: getAdminTotal
	* @Description: TODO  
	* @return int  
	* @author 刘强   
	* @date 2015-5-18 下午11:37:27
	*/ 
	public int getAdminTotal(String name);
	/**
	* @Title: getAdminByUserName
	* @Description: TODO  
	* @return Admin  
	* @author 刘强   
	* @date 2015-5-19 上午12:43:10
	*/ 
	public Admin getAdminByUserName(String userName);
}
