package com.cqut.forum.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cqut.forum.entity.Academy;
import com.cqut.forum.entity.Message;
import com.cqut.forum.entity.User;
import com.cqut.forum.service.IAcademyService;
import com.cqut.forum.service.IMessageService;
import com.cqut.forum.service.IUserService;
import com.cqut.forum.util.DateUtil;
import com.cqut.forum.util.StringUtil;

@Controller
@RequestMapping(value="/setting")
public class UserController_2 {
	private IUserService userService;
	private IAcademyService academyService;
	private IMessageService messageService;
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public void setAcademyService(IAcademyService academyService) {
		this.academyService = academyService;
	}

	public void setMessageService(IMessageService messageService) {
		this.messageService = messageService;
	}

	/**
	* @Title: userManage
	* @Description: 返回用户管理页面 
	* @return String  
	* @author 刘强   
	* @date 2015-5-18 下午1:59:55
	*/ 
	@RequestMapping(value="/userManage.do")
	public String userManage(HttpServletRequest request,HttpServletResponse response){		
		List<Academy> list=academyService.getAcademys();
		request.setAttribute("academy", list);
		return "setting/userManage";
	}
	/**
	* @Title: userManageJson
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午2:32:48
	*/ 
	@RequestMapping(value="/userManageJson.do", method=RequestMethod.POST)
	public void userManageJson(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		int currentPage = Integer.parseInt(request.getParameter("page"));
		int pageSize    = Integer.parseInt(request.getParameter("rows"));
		String order     = request.getParameter("order")== null?"":request.getParameter("order");		//排序的方式
		String sort     = request.getParameter("sort")== null?"":request.getParameter("sort");
		String name = request.getParameter("name") == null?"":request.getParameter("name");
		String academyId=request.getParameter("academyId");
		String role=request.getParameter("role");
		String email=request.getParameter("email");
		String isfrozen=request.getParameter("isfrozen");
		String starTime=request.getParameter("starTime");
		String endTime=request.getParameter("endTime");
		
        List<User> list=userService.getUser(name, academyId, starTime, endTime,role,isfrozen,email, currentPage, pageSize,  order, sort);
        for(User user:list){
        	Academy academy=academyService.getAcademy(user.getAcademyId());
        	user.setAcademyName(academy.getAcademyName());
        }
        int total=userService.getUserTotal(name, academyId, starTime, endTime,role,isfrozen,email);
        root.put("rows", list);
		root.put("total",total);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

		
	}
	@RequestMapping(value="/delUser.do")
	public void delUser(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		String[] ids=idsStr.split(",");
		for(int i=0;i<ids.length;i++){
			userService.delete(Long.parseLong(ids[i]));
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: frozenUser
	* @Description: 冻结用户 
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午5:08:38
	*/ 
	@RequestMapping(value="/frozenUser.do")
	public void frozenUser(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		userService.updateUserForFrozen(Long.parseLong(idsStr));
		root.put("result", 1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: thawUser
	* @Description: 解冻  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午5:09:08
	*/ 
	@RequestMapping(value="/thawUser.do")
	public void thawUser(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		userService.updateUserForThaw(Long.parseLong(idsStr));
		root.put("result", 1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: sendMessage
	* @Description: 发送系统消息  
	* @return void  
	* @author 刘强   
	* @date 2015-5-20 上午8:50:49
	*/ 
	@RequestMapping(value="/sendMessage.do")
	public void sendMessage(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		String title=request.getParameter("title");
		String content=request.getParameter("content");
		String[] ids=idsStr.split(",");
		for(int i=0;i<ids.length;i++){
			Message message=new Message();
			message.setUserId(Long.parseLong(ids[i]));
			message.setIsRead(0);
			message.setCreateDate(DateUtil.getCurrentTimestamp());
			message.setTitle(title);
			message.setContent(content);
			message.setType(1);
			messageService.saveMessage(message);
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
}
