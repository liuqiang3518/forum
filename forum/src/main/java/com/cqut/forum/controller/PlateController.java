package com.cqut.forum.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cqut.forum.entity.Plate;
import com.cqut.forum.entity.Theme;
import com.cqut.forum.service.IPlateService;
import com.cqut.forum.service.IThemeService;
import com.cqut.forum.util.StringUtil;

@Controller
@RequestMapping(value="/setting")
public class PlateController {
	private IPlateService plateService;
	private IThemeService themeService;

	public void setPlateService(IPlateService plateService) {
		this.plateService = plateService;
	}

	public void setThemeService(IThemeService themeService) {
		this.themeService = themeService;
	}

	/**
	* @Title: plateManage
	* @Description: 返回管理页面 
	* @return String  
	* @author 刘强   
	* @date 2015-5-18 上午10:15:49
	*/ 
	@RequestMapping(value="/plateManage.do")
	public String plateManage(HttpServletRequest request,HttpServletResponse response){
		List<Theme> list =themeService.getThemes();
		request.setAttribute("theme", list);
		return "setting/plateManage";
	}
	/**
	* @Title: plateManegeJson
	* @Description: 获取plate数据  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 上午10:15:46
	*/ 
	@RequestMapping(value="/plateManegeJson.do")
	public void plateManegeJson(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		int currentPage = Integer.parseInt(request.getParameter("page"));
		int pageSize    = Integer.parseInt(request.getParameter("rows"));
		String order     = request.getParameter("order")== null?"":request.getParameter("order");		//排序的方式
		String sort     = request.getParameter("sort")== null?"":request.getParameter("sort");
		String name = request.getParameter("name") == null?"":request.getParameter("name");
		String themeIdStr=request.getParameter("theme");
		long themeId=0;
		if(null!=themeIdStr&&!"".equals(themeIdStr)){
			themeId=Long.parseLong(themeIdStr);
		}
		List<Plate> list=plateService.getPlate(currentPage, pageSize, name, themeId, order, sort);
		for(Plate plate:list){
			Theme theme=themeService.getTheme(plate.getThemeId());
			plate.setThemeName(theme.getThemeName());
		}
		int total=plateService.getPlateTotal(name, themeId);
		root.put("rows", list);
		root.put("total",total);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: delPlate
	* @Description: 删除操作  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 上午10:27:04
	*/ 
	@RequestMapping(value="/delPlate.do")
	public void delPlate(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		String[] ids=idsStr.split(",");
		for(int i=0;i<ids.length;i++){
			plateService.delete(Long.parseLong(ids[i]));
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: getPlateById
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-18 下午12:21:46
	*/ 
	@RequestMapping(value="/getPlateById")
	public void getPlateById(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String id=request.getParameter("id");
		Plate plate=plateService.getPlateById(Long.parseLong(id));
		root.put("name", plate.getPlateName());
		root.put("theme", plate.getThemeId());
		root.put("color", plate.getColor());
		root.put("icon", plate.getIcon());
		root.put("introduction", plate.getIntroduction());
		root.put("id", plate.getId());
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	@RequestMapping(value="/saveOrUpdatePlate.do")
	public void saveOrUpdatePlate(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String themeId=request.getParameter("theme");
		String color=request.getParameter("color");
		String icon=request.getParameter("icon");
		String introduction=request.getParameter("introduction");
		Plate plate=new Plate();
		plate.setPlateName(name);
		plate.setThemeId(Long.parseLong(themeId));
		if(!StringUtil.isEmpty(color))
			plate.setColor(color);
		if(!StringUtil.isEmpty(icon))
			plate.setIcon(icon);
		if(!StringUtil.isEmpty(introduction))
			plate.setIntroduction(introduction);
		if(!StringUtil.isEmpty(id)){
			plate.setId(Long.parseLong(id));
			plateService.update(plate);
		}else{
			plateService.save(plate);
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
}
