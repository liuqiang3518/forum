package com.cqut.forum.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.json.JSONObject;

import com.cqut.forum.entity.Admin;
import com.cqut.forum.entity.User;
import com.cqut.forum.service.IAdminService;
import com.cqut.forum.util.CookieHandler;
import com.cqut.forum.util.DESUtils;
import com.cqut.forum.util.DateUtil;
import com.cqut.forum.util.DefaultElements;
import com.cqut.forum.util.Encode64;
import com.cqut.forum.util.StringUtil;
@Controller
@RequestMapping(value="/setting")
public class AdminController {
	private IAdminService adminService;

	public void setAdminService(IAdminService adminService) {
		this.adminService = adminService;
	}
	
	/**
	* @Title: adminLogin
	* @Description: 返回管理员登录页面  
	* @return String  
	* @author 刘强   
	* @date 2015-5-17 上午10:51:54
	*/ 
	@RequestMapping(value="/adminLogin.do")
	public String adminLogin(HttpServletRequest request,HttpServletResponse response){
		return "setting/adminlogin";
	}
	/**
	* @Title: adminLogin
	* @Description:   管理员登录
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 上午10:49:45
	*/ 
	@RequestMapping(value="/adminLoginJson.do")
	public void adminLoginJson(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		int result=0;
		//获取参数并解码
		String name=new String(Encode64.decode(request.getParameter("name")));
		String password=new String(Encode64.decode(request.getParameter("password")));
		Admin admin=adminService.getAdmin(name, password);
		if(null!=admin){
			CookieHandler.setCookie(response, DefaultElements.ADMINCOOKIE_NAME, DESUtils.encode(admin.getUserName()), DefaultElements.COOKIE_LIFE);
			result=1;
			root.put("result", result);
		}else{
			root.put("result", result);
		}
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: settingIndex
	* @Description: 返回后台主界面 
	* @return String  
	* @author 刘强   
	* @date 2015-5-17 下午12:47:51
	*/ 
	@RequestMapping(value="/settingIndex.do")
	public String settingIndex(HttpServletRequest request,HttpServletResponse response){
		String cookieName=CookieHandler.getCookieValue(request, DefaultElements.ADMINCOOKIE_NAME);	
		String adminName=DESUtils.decode(cookieName);
		Admin admin=adminService.getAdminByUserName(adminName);
		request.setAttribute("level", admin.getLevel());
		return "setting/Default";
	}
	/**
	* @Title: getAdminName
	* @Description: 返回用户名字  
	* @return void  
	* @author 刘强   
	* @date 2015-5-19 上午1:10:05
	*/ 
	@RequestMapping(value="/getAdminName.do")
	public void getAdminName(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String cookieName=CookieHandler.getCookieValue(request, DefaultElements.ADMINCOOKIE_NAME);	
		String adminName=DESUtils.decode(cookieName);
		root.put("result",adminName);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);
	}
	@RequestMapping(value="/logout.do")
	public String logout(HttpServletRequest request,HttpServletResponse response){
		CookieHandler.delCookie(response, DefaultElements.ADMINCOOKIE_NAME);	
		return "setting/adminlogin";
	}
	
////////////////////////管理员用户管理处理
	/**
	* @Title: adminManage
	* @Description: 返回管理员用户管理界面  
	* @return String  
	* @author 刘强   
	* @date 2015-5-18 下午11:34:13
	*/ 
	@RequestMapping(value="/adminManage.do")
	public String adminManage(HttpServletRequest request,HttpServletResponse response){
		return "setting/adminManage";
	}
	@RequestMapping(value="/adminManageJson.do")
	public void adminManageJson(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		int currentPage = Integer.parseInt(request.getParameter("page"));
		int pageSize    = Integer.parseInt(request.getParameter("rows"));
		String name = request.getParameter("name") == null?"":request.getParameter("name");
		String order     = request.getParameter("order")== null?"":request.getParameter("order");		//排序的方式
		String sort     = request.getParameter("sort")== null?"":request.getParameter("sort");	
		List<Admin> list=adminService.getAdmin(currentPage, pageSize, name, order, sort);
		int total=adminService.getAdminTotal(name);
		root.put("rows", list);
		root.put("total",total);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	@RequestMapping(value="/delAdmin.do")
	public void delAdmin(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		String[] ids=idsStr.split(",");
		for(int i=0;i<ids.length;i++){
			adminService.delete(Long.parseLong(ids[i]));
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);
	}
	@RequestMapping(value="/getAdminById.do")
	public void getAdminById(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String id=request.getParameter("id");
		Admin admin=adminService.getAdmin(Long.parseLong(id));
		root.put("id", admin.getId());
		root.put("name", admin.getUserName());
		root.put("password", admin.getPassword());
		root.put("level", admin.getLevel());
		root.put("createDate", admin.getCreateDateStr());
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);
	}
	@RequestMapping(value="/saveOrUpdateAdmin.do")
	public void saveOrUpdateAdmin(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		int result=0;
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		String level=request.getParameter("level");
		String id=request.getParameter("id");
		String createDate=request.getParameter("createDate");
		Admin admin=new Admin();
		admin.setUserName(name);
		admin.setPassword(password);
		admin.setLevel(Integer.parseInt(level));
		if(!StringUtil.isEmpty(id)){
			admin.setId(Long.parseLong(id));
			admin.setCreateDate(DateUtil.strToDate(createDate));
			adminService.update(admin);
			result=1;
		}else{
			Admin admi=adminService.getAdminByUserName(name);
			if(admi!=null){
				result=2;
			}else{
				admin.setCreateDate(DateUtil.getCurrentDate());
				adminService.save(admin);
				result=1;
			}
		}
		root.put("result", result);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
}
