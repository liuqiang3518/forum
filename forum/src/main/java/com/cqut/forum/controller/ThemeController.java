package com.cqut.forum.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cqut.forum.entity.Academy;
import com.cqut.forum.entity.Theme;
import com.cqut.forum.service.IAcademyService;
import com.cqut.forum.service.IThemeService;
import com.cqut.forum.util.StringUtil;
@Controller
@RequestMapping(value="/setting")
public class ThemeController {

	private IThemeService themeService;
	private IAcademyService academyService;

	public void setThemeService(IThemeService themeService) {
		this.themeService = themeService;
	}

	public void setAcademyService(IAcademyService academyService) {
		this.academyService = academyService;
	}

	/**
	* @Title: themeManage
	* @Description: 返回主题管理页面  
	* @return String  
	* @author 刘强   
	* @date 2015-5-17 下午3:32:11
	*/ 
	@RequestMapping(value="/themeManage.do")
	public String themeManage(HttpServletRequest request,HttpServletResponse response){
		List<Academy> list=academyService.getAcademys();
		request.setAttribute("academy", list);
		return "setting/themeManage";
	}
	/**
	* @Title: themeManageJson
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 下午3:33:25
	*/ 
	@RequestMapping(value="/themeManageJson.do")
	public void themeManageJson(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		int currentPage = Integer.parseInt(request.getParameter("page"));
		int pageSize    = Integer.parseInt(request.getParameter("rows"));
		String name = request.getParameter("name") == null?"":request.getParameter("name");
		String order     = request.getParameter("order")== null?"":request.getParameter("order");		//排序的方式
		String sort     = request.getParameter("sort")== null?"":request.getParameter("sort");	
		int total=themeService.getThemeTotal(name);
		List<Theme> list=themeService.getTheme(currentPage, pageSize, name, order, sort);
		root.put("rows", list);
		root.put("total",total);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}/**
	* @Title: delTheme
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 下午7:48:43
	*/ 
	@RequestMapping(value="/delTheme.do")
	public void delTheme(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idsStr=request.getParameter("id");
		String[] ids=idsStr.split(",");
		for(int i=0;i<ids.length;i++){
			themeService.delete(Long.parseLong(ids[i]));
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: saveOrUpdateTheme
	* @Description: TODO  
	* @return void  
	* @author 刘强   
	* @date 2015-5-17 下午10:34:14
	*/ 
	@RequestMapping(value="/saveOrUpdateTheme.do")
	public void saveOrUpdateTheme(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String idStr=request.getParameter("themeId");
		String name=request.getParameter("name");
		String academyIdStr=request.getParameter("academy");
		String color=request.getParameter("color");
		String icon=request.getParameter("icon");
		String introduction=request.getParameter("introduction");
		
		Theme theme=new Theme();
		theme.setThemeName(name);
		if(null!=academyIdStr&&!"".equals(academyIdStr))
			theme.setAcademyId(Long.parseLong(academyIdStr));
		if(null!=color&&!"".equals(color))
			theme.setColor(color);
		if(null!=icon&&!"".equals(icon))
			theme.setIcon(icon);
		if(null!=introduction&&!"".equals(introduction))
			theme.setIntroduuction(introduction);
		if(null!=idStr&&!"".equals(idStr)){
			theme.setId(Long.parseLong(idStr));
			themeService.update(theme);
		}else{
			themeService.save(theme);
		}
		root.put("result",1);
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}

	@RequestMapping(value="/getThemeById.do")
	public void getThemeById(HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> root=new HashMap<String,Object>();
		String id=request.getParameter("id");
		Theme theme=themeService.getTheme(Long.parseLong(id));
		root.put("themeId", theme.getId());
		root.put("name", theme.getThemeName());
		root.put("academy", theme.getAcademyId());
		root.put("color", theme.getColor());
		root.put("icon", theme.getIcon());
		root.put("introduction", theme.getIntroduuction());
		StringUtil.writeToWeb(JSONObject.fromObject(root).toString(), "html", response);

	}
	/**
	* @Title: getThemeById
	* @Description: 返回主题统计页面 
	* @return void  
	* @author 刘强   
	* @date 2015-6-6 上午12:28:28
	*/ 
	@RequestMapping(value="/getThemeStatistics.do")
	public String getThemeStatistics(HttpServletRequest request,HttpServletResponse response){
		List<String> category=new ArrayList<String>();
		List<Long> postNum=new ArrayList<Long>();
		List<Long> repyNum=new ArrayList<Long>();
		List<Theme> themes=themeService.getThemes();
		for(Theme theme:themes){
			category.add("\""+theme.getThemeName()+"\"");
			postNum.add(theme.getPostNumber());
			repyNum.add(themeService.commentNumByThemeId(theme.getId()));
		}
		request.setAttribute("category", category);
		request.setAttribute("postNum", postNum);
		request.setAttribute("repyNum", repyNum);
		return "setting/themeStatistics";	
	}
}
