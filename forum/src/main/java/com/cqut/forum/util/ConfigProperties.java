package com.cqut.forum.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

public class ConfigProperties {
 

	/**
	* @Title: get
	* @Description:   
	* @return String  
	* @throws
	* @author 刘强   
	* @date 2015-4-24 下午5:26:55
	*/ 
	public static String get(String name){
		if(null==name) return null;
		InputStream in = ConfigProperties.class.getClassLoader()
	            .getResourceAsStream("config.properties");		
        Properties prop = new Properties();
        try {
			prop.load(in);
			// 获取所有的名称
			Enumeration<?> allName = prop.propertyNames();
			while (allName.hasMoreElements()) {
				String nam = (String) allName.nextElement();
				if(name.equals(nam)){
					String value = (String) prop.get(name);
					return value;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        return null;
	}
}
