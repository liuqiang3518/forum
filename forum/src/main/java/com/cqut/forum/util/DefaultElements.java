package com.cqut.forum.util;

public class DefaultElements {
	/**
	* @Fields COOKIE_LIFE : cookie的生命周期(天),如：1
	*/ 
	public static int COOKIE_LIFE=1*24*60*60;
	/**
	* @Fields COOKIE_NAME : cookie用户名存储的name
	*/ 
	public static String COOKIE_NAME="forum_user";

	/**
	* @Fields ADMINCOOKIE_NAME : forum_admin
	*/ 
	public static String ADMINCOOKIE_NAME="forum_admin";	
	/**
	* @Fields DES_KEY : DES加密 解密 key
	*/ 
	protected static String DES_KEY="QWERTYUIOP"; 
	
	/**
	* @Fields BLANK : blank 空 
	*/ 
	public static String BLANK="";
	
	public static Integer ONE=1;
	public static Integer TWO=2;
	public static Integer Three=3;
	public static Integer FOUR=4;
	public static Integer FIVE=5;
	public static Integer SIX=6;
	public static Integer SEVEN=7;
	public static Integer EIGHT=8;
	public static Integer NINE=9;
	public static Integer ZERO=0;
	public static Integer TEN=10;
	/**
	* @Fields FUBEN : "副本"
	*/ 
	public static String FUBEN="-副本-";
	public static String UPDATE="&nbsp;已更新！";
	public static String REPLY="&nbsp;有回复！";
	public static String SIGNSUCCESS="恭喜您，注册成功！！";
	
	
}
