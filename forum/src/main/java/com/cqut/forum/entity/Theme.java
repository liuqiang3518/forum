package com.cqut.forum.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Theme entity. @author MyEclipse Persistence Tools
 */

public class Theme implements java.io.Serializable {

	// Fields

	private Long id;
	private String themeName;
	private Long academyId;
	private String introduuction;
	private Long postNumber=(long)0;
	private String color;
	private String icon;
	private List<Plate> plates;
	// Constructors

	private String academyName;
	/** default constructor */
	public Theme() {
	}

	/** minimal constructor */
	public Theme(String themeName, Long postNumber) {
		this.themeName = themeName;
		this.postNumber = postNumber;
	}

	/** full constructor */
	public Theme(String themeName, Long academyId, String introduuction,
			Long postNumber,String color, String icon) {
		this.themeName = themeName;
		this.academyId = academyId;
		this.introduuction = introduuction;
		this.postNumber = postNumber;
		this.color = color;
		this.icon = icon;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getThemeName() {
		return this.themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public Long getAcademyId() {
		return this.academyId;
	}

	public void setAcademyId(Long academyId) {
		this.academyId = academyId;
	}

	public String getIntroduuction() {
		return this.introduuction;
	}

	public void setIntroduuction(String introduuction) {
		this.introduuction = introduuction;
	}

	public Long getPostNumber() {
		return this.postNumber;
	}

	public void setPostNumber(Long postNumber) {
		this.postNumber = postNumber;
	}

	public List<Plate> getPlates() {
		return plates;
	}

	public void setPlates(List<Plate> plates) {
		this.plates = plates;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getAcademyName() {
		return academyName;
	}

	public void setAcademyName(String academyName) {
		this.academyName = academyName;
	}


}