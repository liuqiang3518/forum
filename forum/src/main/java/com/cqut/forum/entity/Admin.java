package com.cqut.forum.entity;

import java.util.Date;

import com.cqut.forum.util.DateUtil;

/**
 * Admin entity. @author MyEclipse Persistence Tools
 */

public class Admin implements java.io.Serializable {

	// Fields

	private Long id;
	private String userName;
	private String password;
	private Integer level;
    private Date createDate;
    
    private String createDateStr;
	// Constructors

	/** default constructor */
	public Admin() {
	}

	/** full constructor */
	public Admin(String userName, String password, Integer level,Date createDate) {
		this.userName = userName;
		this.password = password;
		this.level = level;
		this.createDate=createDate;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
		this.createDateStr=DateUtil.dateToStr(createDate);
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

}