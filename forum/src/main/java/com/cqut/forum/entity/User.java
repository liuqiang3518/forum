package com.cqut.forum.entity;

import java.util.Date;

import com.cqut.forum.util.DateUtil;



/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

	// Fields

	private Long id;
	private Long academyId;
	private String userName;
	private String password;
	private Long userId;
	private String email;
	/**
	 * 角色  ：0 学生；1 老师
	 */
	private Integer role=0;
	private Integer sex;
	private String birthday;
	private String phoneNumber;
	private Integer isfrozen;
	/**
	 * 籍贯
	 */
	private String nativePlace;
	/**
	 * 现居地
	 */
	private String abode;
	/**
	 * 主题id  如果为0表示普通用户，不为0表示该主题的版主
	 */
	private Long plateId;
	private Date createDate;//注册时间
	private String imagePath;
    
/////////////////////////////	
	private String academyName;
	private String createDateStr;
	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(Long academyId, String userName, String password,
			Long userId, String email, Integer role, String imagePath,Integer isfrozen) {
		this.academyId = academyId;
		this.userName = userName;
		this.password = password;
		this.userId = userId;
		this.email = email;
		this.role = role;
		this.imagePath = imagePath;
		this.isfrozen=isfrozen;
	}

	/** full constructor */
	public User(Long academyId, String userName, String password,
			Long userId, String email, Integer role, Integer sex,
			String birthday, String phoneNumber, String nativePlace,
			String abode, Long plateId,Date createDate, String imagePath,Integer isfrozen) {
		this.academyId = academyId;
		this.userName = userName;
		this.password = password;
		this.userId = userId;
		this.email = email;
		this.role = role;
		this.sex = sex;
		this.birthday = birthday;
		this.phoneNumber = phoneNumber;
		this.nativePlace = nativePlace;
		this.abode = abode;
		this.plateId = plateId;
		this.createDate=createDate;
		this.imagePath = imagePath;
		this.isfrozen=isfrozen;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
  
	public Long getAcademyId() {
		return academyId;
	}

	public void setAcademyId(Long academyId) {
		this.academyId = academyId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getRole() {
		return this.role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return this.birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNativePlace() {
		return this.nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getAbode() {
		return this.abode;
	}

	public void setAbode(String abode) {
		this.abode = abode;
	}

	public Long getPlateId() {
		return this.plateId;
	}

	public void setPlateId(Long plateId) {
		this.plateId = plateId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
		this.setCreateDateStr(DateUtil.dateToStr(createDate));
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getAcademyName() {
		return academyName;
	}

	public void setAcademyName(String academyName) {
		this.academyName = academyName;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public Integer getIsfrozen() {
		return isfrozen;
	}

	public void setIsfrozen(Integer isfrozen) {
		this.isfrozen = isfrozen;
	}


}