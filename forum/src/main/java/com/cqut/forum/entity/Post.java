package com.cqut.forum.entity;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.cqut.forum.util.DateUtil;

/**
 * Post entity. @author MyEclipse Persistence Tools
 */

public class Post implements java.io.Serializable {

	// Fields

	private Long id;
	private Long plateId;
	private Long userId;
	private String userName;
	private String name;
	private String content;
	private Timestamp postDate;
	private Timestamp updateDate;
	private Long pageView;
	private Long replyNumber;
	private Integer isfrozen=0;
	
	private String date;
	private String postDateStr;
	private String updateDateStr;

	// Constructors

	/** default constructor */
	public Post() {
	}

	/** minimal constructor */
	public Post(Long plateId, Long userId, String name, String content,int isfrozen) {
		this.plateId = plateId;
		this.userId = userId;
		this.name = name;
		this.content = content;
		this.isfrozen=isfrozen;
	}

	/** full constructor */
	public Post(Long plateId, Long userId, String userName, String name,
			String content, Timestamp postDate, Timestamp updateDate, Long pageView,
			Long replyNumber,int isfrozen) {
		this.plateId = plateId;
		this.userId = userId;
		this.userName = userName;
		this.name = name;
		this.content = content;
		this.postDate = postDate;
		this.updateDate = updateDate;
		this.pageView = pageView;
		this.replyNumber = replyNumber;
		this.isfrozen=isfrozen;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPlateId() {
		return plateId;
	}

	public void setPlateId(Long plateId) {
		this.plateId = plateId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getPostDate() {
		return postDate;
	}

	public void setPostDate(Timestamp postDate) {
		this.postDate = postDate;
		setPostDateStr(DateUtil.dateToStr_2(postDate));
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
		this.setUpdateDateStr(DateUtil.dateToStr_2(updateDate));
	}

	public Long getPageView() {
		return this.pageView;
	}

	public void setPageView(Long pageView) {
		this.pageView = pageView;
	}

	public Long getReplyNumber() {
		return this.replyNumber;
	}

	public void setReplyNumber(Long replyNumber) {
		this.replyNumber = replyNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPostDateStr() {
		return postDateStr;
	}

	public void setPostDateStr(String postDateStr) {
		this.postDateStr = postDateStr;
	}

	public String getUpdateDateStr() {
		return updateDateStr;
	}

	public void setUpdateDateStr(String updateDateStr) {
		this.updateDateStr = updateDateStr;
	}

	public Integer getIsfrozen() {
		return isfrozen;
	}

	public void setIsfrozen(Integer isfrozen) {
		this.isfrozen = isfrozen;
	}


}