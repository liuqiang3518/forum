package com.cqut.forum.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



/**
 * Plate entity. @author MyEclipse Persistence Tools
 */

public class Plate implements java.io.Serializable {

	// Fields

	private Long id;
	private Long themeId;
	private String plateName;
	private Long userId;
	private String introduction;
	private Long postNumber=(long)0;
	private String color;
	private String icon;
	private List<Post> posts ;
	// Constructors

	private String themeName;
	/** default constructor */
	public Plate() {
	}

	/** minimal constructor */
	public Plate(Long themeId, String plateName) {
		this.themeId = themeId;
		this.plateName = plateName;
	}

	/** full constructor */
	public Plate(Long themeId, String plateName, Long userId,
			String introduction, Long postNumber, String color, String icon) {
		this.themeId = themeId;
		this.plateName = plateName;
		this.userId = userId;
		this.introduction = introduction;
		this.postNumber = postNumber;
		this.color = color;
		this.icon = icon;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getThemeId() {
		return themeId;
	}

	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}

	public String getPlateName() {
		return this.plateName;
	}

	public void setPlateName(String plateName) {
		this.plateName = plateName;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getIntroduction() {
		return this.introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Long getPostNumber() {
		return this.postNumber;
	}

	public void setPostNumber(Long postNumber) {
		this.postNumber = postNumber;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}


}