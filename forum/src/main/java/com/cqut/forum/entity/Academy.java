package com.cqut.forum.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Academy entity. @author MyEclipse Persistence Tools
 */

public class Academy implements java.io.Serializable {

	// Fields

	private long id;
	private long academyId;
	/**
	 * 学院名称
	 */
	private String academyName;
	/**
	 * 院长ID
	 */
	private long deanId;
	private String deanName;
	/**
	 * 学院介绍
	 */
	private String introduction;

	// Constructors

	/** default constructor */
	public Academy() {
	}

	/** minimal constructor */
	public Academy(Long academyId, String academyName) {
		this.academyId = academyId;
		this.academyName = academyName;
	}

	/** full constructor */
	public Academy(Long academyId, String academyName, Long deanId,
			String deanName, String introduction) {
		this.academyId = academyId;
		this.academyName = academyName;
		this.deanId = deanId;
		this.deanName = deanName;
		this.introduction = introduction;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAcademyId() {
		return this.academyId;
	}

	public void setAcademyId(Long academyId) {
		this.academyId = academyId;
	}

	public String getAcademyName() {
		return this.academyName;
	}

	public void setAcademyName(String academyName) {
		this.academyName = academyName;
	}

	public Long getDeanId() {
		return this.deanId;
	}

	public void setDeanId(Long deanId) {
		this.deanId = deanId;
	}

	public String getDeanName() {
		return this.deanName;
	}

	public void setDeanName(String deanName) {
		this.deanName = deanName;
	}

	public String getIntroduction() {
		return this.introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

}