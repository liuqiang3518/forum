package com.cqut.forum.entity;

public class Ztree {

	private Long id;
	private Long pId;
	private String name;
	private Long value;
	private Boolean open;
	public Ztree(Long id, Long pId, String name, Long value, Boolean open) {
		super();
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.value = value;
		this.open = open;
	}
	public Ztree() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getpId() {
		return pId;
	}
	public void setpId(Long pId) {
		this.pId = pId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public Boolean getOpen() {
		return open;
	}
	public void setOpen(Boolean open) {
		this.open = open;
	}
	
}
