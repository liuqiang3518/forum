package com.cqut.forum.entity;

import java.sql.Timestamp;

import com.cqut.forum.util.DateUtil;

/**
 * Message entity. @author MyEclipse Persistence Tools
 */

public class Message implements java.io.Serializable {

	// Fields

	private Long id;
	private Long userId;
	private Long targetId;
	private String content;
	private String title;
	private Integer isRead=0;
	private Timestamp createDate;
	private Integer type=0;
	
	private String createDateStr;

	// Constructors

	/** default constructor */
	public Message() {
	}

	/** minimal constructor */
	public Message(long userId, Integer isRead, Timestamp createDate, Integer type) {
		this.userId = userId;
		this.isRead = isRead;
		this.createDate = createDate;
		this.type = type;
	}

	/** full constructor */
	public Message(long userId, Long targetId, String content, Integer isRead,
			Timestamp createDate, Integer type,String title) {
		this.userId = userId;
		this.targetId = targetId;
		this.content = content;
		this.isRead = isRead;
		this.createDate = createDate;
		this.type = type;
		this.title=title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
		this.setCreateDateStr(DateUtil.dateToStr_2(createDate));
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


}