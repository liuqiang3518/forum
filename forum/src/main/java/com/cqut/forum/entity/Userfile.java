package com.cqut.forum.entity;

import java.sql.Timestamp;

import com.cqut.forum.util.DateUtil;

/**
 * Userfile entity. @author MyEclipse Persistence Tools
 */

public class Userfile implements java.io.Serializable {

	// Fields

	private Long id;
	private String fileName;
	private Long userId;
	private Integer isFile;
	private String md5;
	private Integer level;
	private Long parentId;
	private Timestamp createDate;
	
	private String createDateStr;
	private Filestore filestore;
	// Constructors

	/** default constructor */
	public Userfile() {
	}

	/** minimal constructor */
	public Userfile(Long userId,String fileName) {
		this.userId = userId;
		this.fileName=fileName;
	}

	/** full constructor */
	public Userfile(Long userId,String fileName, Integer isFile, String md5, Integer level,
			Long parentId, Timestamp createDate) {
		this.userId = userId;
		this.fileName=fileName;
		this.isFile = isFile;
		this.md5 = md5;
		this.level = level;
		this.parentId = parentId;
		this.createDate = createDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getIsFile() {
		return isFile;
	}

	public void setIsFile(Integer isFile) {
		this.isFile = isFile;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
		this.setCreateDateStr(DateUtil.dateToStr(createDate));
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public Filestore getFilestore() {
		return filestore;
	}

	public void setFilestore(Filestore filestore) {
		this.filestore = filestore;
	}


	// Property accessors


}