package com.cqut.forum.entity;

import java.sql.Timestamp;


/**
 * Filestore entity. @author MyEclipse Persistence Tools
 */

public class Filestore  implements java.io.Serializable {


    // Fields    

     private Long id;
     private Long userId;
     private String filemd5;
     private String fileName;
     private String filepath;
     private Timestamp uploadDate;
     private Integer isfreeze;
     private String filesize;
     private Integer filetype; // 1为文档类型 2为音频文件 3为视频文件 4为图片文件 0 为其他类型 


    // Constructors

    /** default constructor */
    public Filestore() {
    }

	/** minimal constructor */
    public Filestore(long userId, String filemd5, String fileName, String filepath, Timestamp uploadDate) {
        this.userId = userId;
        this.filemd5 = filemd5;
        this.fileName = fileName;
        this.filepath = filepath;
        this.uploadDate = uploadDate;
    }
    
    /** full constructor */
    public Filestore(long userId, String filemd5, String fileName, String filepath, Timestamp uploadDate, Integer isfreeze, String filesize, Integer filetype) {
        this.userId = userId;
        this.filemd5 = filemd5;
        this.fileName = fileName;
        this.filepath = filepath;
        this.uploadDate = uploadDate;
        this.isfreeze = isfreeze;
        this.filesize = filesize;
        this.filetype = filetype;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFilemd5() {
		return filemd5;
	}

	public void setFilemd5(String filemd5) {
		this.filemd5 = filemd5;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Timestamp getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Timestamp uploadDate) {
		this.uploadDate = uploadDate;
	}

	public Integer getIsfreeze() {
		return isfreeze;
	}

	public void setIsfreeze(Integer isfreeze) {
		this.isfreeze = isfreeze;
	}

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}

	public Integer getFiletype() {
		return filetype;
	}

	public void setFiletype(Integer filetype) {
		this.filetype = filetype;
	}

   
    // Property accessors

}