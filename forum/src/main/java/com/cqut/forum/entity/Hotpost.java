package com.cqut.forum.entity;

import java.util.Date;

import com.cqut.forum.util.DateUtil;

/**
 * Hotpost entity. @author MyEclipse Persistence Tools
 */

public class Hotpost implements java.io.Serializable {

	// Fields

	private Long id;
	private Long postId;
	private Integer type;
	private String imagePath;
	private Date data;

	private String title;
	private String dataStr;
	// Constructors

	/** default constructor */
	public Hotpost() {
	}

	/** minimal constructor */
	public Hotpost(Long postId, Integer type, String imagePath) {
		this.postId = postId;
		this.type = type;
		this.imagePath = imagePath;
	}

	/** full constructor */
	public Hotpost(Long postId, Integer type, String imagePath, Date data,String title) {
		this.postId = postId;
		this.type = type;
		this.imagePath = imagePath;
		this.data = data;
		this.title=title;
	}

	public Hotpost(Long id, Long postId, Integer type, String imagePath,
			Date data, String title) {
		super();
		this.id = id;
		this.postId = postId;
		this.type = type;
		this.imagePath = imagePath;
		this.data = data;
		this.title = title;
		setDataStr(DateUtil.dateToStr(data));
	}
	// Property accessors


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDataStr() {
		return dataStr;
	}

	public void setDataStr(String dataStr) {
		this.dataStr = dataStr;
	}



}